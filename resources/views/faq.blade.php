@extends('layouts.header')
<style>
/* Style the buttons that are used to open and close the accordion panel */
.accordion {
  background-color: #eee;
  color: #444;
  cursor: pointer;
  padding: 18px;
  width: 100%;
  text-align: left;
  border: none;
  outline: none;
  transition: 0.4s;
}

/* Add a background color to the button if it is clicked on (add the .active class with JS), and when you move the mouse over it (hover) */
.active, .accordion:hover {
  background-color: #ccc;
}

/* Style the accordion panel. Note: hidden by default */
.panel {
  padding: 0 18px;
  background-color: white;
  display: none;
  overflow: hidden;
}




.accordion:after {
  content: '\02795'; /* Unicode character for "plus" sign (+) */
  font-size: 13px;
  color: #777;
  float: right;
  margin-left: 5px;
}

.active:after {
  content: "\2796"; /* Unicode character for "minus" sign (-) */
}





</style>
@section('content')

        <div id="preloader">
            <div id="status">&nbsp;</div>
        </div>
        <!-- Body content -->

       @include('layouts.navbar2')
        <!-- End of nav bar -->
 
         <div class="page-head"> 
            <div class="container">
                <div class="row">
                    <div class="page-head-content">
                        <h1 class="page-title"><center>FAQs</center> </h1>               
                    </div>
                </div>
            </div>
        </div>
        <!-- End page header -->
        

        <!-- property area -->
        <div class="content-area recent-property" style="background-color: #FCFCFC; padding-bottom: 55px;">
            <div class="container">    

                <div class="row">
                    <div class="col-md-10 col-md-offset-1 col-sm-12 text-center page-title">
                        <!-- /.feature title -->
                        <h2>Frequently ask questions (FAQ) </h2>
                        <br>
                    </div>
                </div>

                <div class="row row-feat"> 
                    <div class="col-md-12">
 
                        <div class="col-sm-6 feat-list">
                            <div class="panel-group">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                         <h4 class="panel-title fqa-title" data-toggle="collapse" data-target="#fqa11" >
                                            Nostrud exercitation ullamco laboris 1 1
                                          
                                    </div>
                                    <div id="fqa11" class="panel-collapse collapse fqa-body">
                                        <div class="panel-body">
                                            <ol>
                                                <button class="accordion">How Do I Apply for a Home?</button> Lorem ipsum dolor sit amet, consectetur adipisicing
                                                <button class="accordion">How Do I Apply for a Home?</button> Lorem ipsum dolor sit amet, consectetur adipisicing
                                                <button class="accordion">How Do I Apply for a Home?</button> Lorem ipsum dolor sit amet, consectetur adipisicing
                                                <button class="accordion">How Do I Apply for a Home?</button> Lorem ipsum dolor sit amet, consectetur adipisicing
                                                <button class="accordion">How Do I Apply for a Home?</button> Lorem ipsum dolor sit amet, consectetur adipisicing                                           
                                            </ol> 
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            


<h3><em>Prospective Renter FAQ’s</em></h3>

<button class="accordion">How Do I Apply for a Home?</button>
<div class="panel">
  <p>	When you have found a home that you would like to apply for, register or login to schedule side visit or contact us for more enqueries
				
</div>



		<button class="accordion">What are the fees?</button>
			<div class="panel">
				There are no up-front fees to give you information or to show homes to
				you. Once you decide you would like to lease one of our properties there is a %10 application
				fee.
			</div>
		

		<button class="accordion">Where are homes available?</button>
		
			<div class="panel">
				<p>
					We have homes throughout adamawa state  with a focus on rental homes in
					jimeta and yola. This gives you plenty of options. 
				</p>
				<p>
				
				</p>
			</div>
		

			

		<button class="accordion">What is the typical length of lease? </button>
			
			<div class="panel">
				The majority of our leases are for one year or longer. If you are
				looking for something shorter than a year, please call us to discuss your options.
			</div>
		

		<button class="accordion">Are your houses pet friendly?</button>
			
			<div class="panel">
				<p>
					Most of our homes are pet friendly. If you have pets, an additional
					deposit is required and you will be required to maintain Renters Insurance.
				</p>
				<p>
					We may not allow certain types of dogs that may have violent tendencies
					if mixed with other dogs. Examples of dogs that may not be allowed are
					Pit Bulls, Rottweilers, Dobermans, German Shepherds, Huskies, Chows or
					mixed breed with any of the above. Owners reserve the right to deny any
					dog, so speak with your leasing agent prior to applying. There may be a
					limit to the number of pets allowed.
				</p>
			</div>
		

		<button class="accordion">How long will it take to process my application?</button>
			
			<div class="panel">
				There are two steps in the application process. The first step is
				schedule visit and then online application that allows us to verify background, rental history and credit. This
				usually takes no more than one business day to see results. The second step is the
				signing of the lease offer. To expedite this process, we use an online eSignature solution!
			</div>
		

		<button class="accordion">What can I do to expedite the application process? </button>
			
			<div class="panel">
				<p>Make sure you have completed theonline application</a>.</p>
				<p>
					Provide the two most recent passport for each adult that will be
					occupying the home.
				</p>
				<p>
					<a href="/our-team">Contact your leasing agent</a> to discuss the details for
					your lease offer, including move in date, length of lease, whether you have pets, and more.
				</p>
			</div>
		

		<button class="accordion">Who is responsible for the utilities? </button>
			
			<div class="panel">
				Generally, you are responsible for the utilities in the home you lease.
				Please see the details in the individual home listing for any utilities or additional
				amenities that may be provided.
			</div>
		



		<button class="accordion">Application Information </button>
			
			<div class="panel">
				<ul>
					<button class="accordion">You will need your credit card when applying online. The application fee is collected when
						you apply.</button>
						
					
					<button class="accordion">How Do I Apply for a Home?</button>Application fees are non-refundable.
					<button class="accordion">How Do I Apply for a Home?</button>All lease offers are presented for approval.
					<button class="accordion">How Do I Apply for a Home?</button>
						Once you have applied online and been approved, your application is valid for any of the
						homes that we lease.
					
				</ul>
			</div>
		
	
		
	</ul>

</section>

<section id="current-renters">
	<h3><em>Current Renter FAQ’s</em></h3>
	<button class="launch-accordion" ></button>
		<button class="accordion">When is my rent due and how is payment accepted? </button>
			
			<div class="panel">
				<p>All rents are due on the first day of the month.</p>
				<p>
					You may <a href="/tenant">pay your rent online</a> for free through your renter
					portal!
				</p>
			</div>
		

		<button class="accordion">	Pay Your Rent Online </button>
		
			<div class="panel">
				<p>
					<a href="/tenant">Click here</a> to pay your rent online.
				</p>
				<p>
					Rent is considered paid only when actually received or credited. We do not accept post
					dated checks. We must be fair and strict with the late fees so please remember to make your
					payment in a timely manner. Thank you.
				</p>
			</div>
		

		<button class="accordion">	When do late fees apply? </button>
		
			<div class="panel">
				In the event rent is not credited by the rent due date, Renter shall pay
				a late charge of 10% of the monthly rental amount as additional rent.
			</div>
		

		<button class="accordion">Can I get a pet after moving in? Can I watch a friends pet for a few days?</button>
			
			<div class="panel">
				Pets are not allowed unless previously approved by Home Rental Services.
				This includes keeping a friend's pet at the property temporarily. If we discover a pet at your
				property you will be given a 14 day "notice to cure". If the pet is not permanently gone from
				the property within the 14 days, the owner may elect to begin eviction proceedings.
			</div>
		

		<button class="accordion">What if I need to move out before my lease ends?</button>
			
			<div class="panel">
				<p>
					Renter must provide written notice of intent to terminate early a minimum of 30 days prior
					to move-out. The move-out date must be provided at this time as well.
				</p>
				<p>
					Notice to do an early termination of lease must be submitted with payment of the re-letting
					fee equal to one month’s rent IF there is one year or less remaining on your lease. If there is
					more than one year remaining on your lease the re-letting fee is equal to 1/12th of your
					monthly rent for every month remaining in your lease.
				</p>
				<ul>
					<button class="accordion">You must return all keys, garage and gate remotes once a new renter is found.</button>
						
					
					<button class="accordion">How Do I Apply for a Home?</button>
						You must continue paying rent each month as agreed, until an approved renter's lease has
						started and they have paid all fees required prior to the start of their lease, including the
						first month of rent and the security deposit.
					
					<button class="accordion">You must continue paying the utility services after vacating, until the new renter’s lease
						term begins.</button>
						
					
					<button class="accordion">You must arrange for lawn service, snow removal etc. after you vacate until a new renter
						moves in.?</button>
						
					
					<button class="accordion">All other terms and conditions of your lease agreement must continue to be met.
				</button>
			</div>
		
	</ul>
</section>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                
            </div>
        </div>

     <!-- Footer area-->
        
        @include('layouts.footer')        

        <script>
        
        var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    /* Toggle between adding and removing the "active" class,
    to highlight the button that controls the panel */
    this.classList.toggle("active");

    /* Toggle between hiding and showing the active panel */
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
        </script>

<style>
.panel {
  padding: 0 18px;
  background-color: white;
  max-height: 0;
  overflow: hidden;
  transition: max-height 0.2s ease-out;
}
</style>

<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  });
}
</script>
@include('layouts.script')
@endsection