@extends('layouts.header')


@section('content')

      @include('layouts.navbar2')
</div>
        <!-- End of nav bar -->

        <div class="page-head"> 
            <div class="container">
                <div class="row">
                    <div class="page-head-content">
                        <h1 class="page-title"> <center> {{ __('New Account Registration') }} </center> </h1>               
                    </div>
                </div>
            </div>
        </div>
        <!-- End page header -->
 

        <!-- register-area -->
        <div class="register-area" style="background-color: rgb(249, 249, 249);">
            <div class="container">

                <div class="col-md-6 col-md-offset-4">
                    <div class="box-for overflow">
                        <div class="col-md-12 col-xs-12  register-blocks">
                            <h2>New account : </h2> 
                            <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group ">
                            <label for="name" ">{{ __('User Name') }}</label>

                            <div >
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                      
<div class="form-group">
                            <label for="fname" >{{ __('First Name') }}</label>

                            <div >
                                <input id="fname" type="text" class="form-control @error('fname') is-invalid @enderror" name="fname" value="{{ old('fname') }}" required autocomplete="fname" autofocus>

                                @error('fname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> <div class="form-group">
                            <label for="mname" >{{ __('Middle Name') }}</label>

                            <div >
                                <input id="mname" type="text" class="form-control @error('mname') is-invalid @enderror" name="mname" value="{{ old('mname') }}" required autocomplete="mname" autofocus>

                                @error('mname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> <div class="form-group">
                            <label for="lname" >{{ __('Last Name') }}</label>

                            <div >
                                <input id="lname" type="text" class="form-control @error('lname') is-invalid @enderror" name="lname" value="{{ old('lname') }}" required autocomplete="lname" autofocus>

                                @error('lname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> <div class="form-group">
                            <label for="gender" >{{ __('Gender') }}</label>

                            <div >
                                <select id="gender" class="form-control @error('gender') is-invalid @enderror" name="gender" value="{{ old('gender') }}" required autocomplete="gender" autofocus>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                                </select>

                                @error('gender')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> <div class="form-group">
                            <label for="dob" >{{ __('Date of Birth') }}</label>

                            <div >
                                <input id="dob" type="date" class="form-control @error('dob') is-invalid @enderror" name="dob" value="{{ old('dob') }}" required autocomplete="dob" autofocus>

                                @error('dob')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> <div class="form-group">
                            <label for="addr" >{{ __('Address') }}</label>

                            <div >
                               <textarea id="addr" col="10"="5" class="form-control @error('addr') is-invalid @enderror" name="addr" value="{{ old('addr') }}" required autocomplete="addr" autofocus>
                               </textarea>

                                @error('addr')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> <div class="form-group">
                            <label for="country" >{{ __('Country') }}</label>

                            <div >
                              <select id="country"  class="form-control @error('country') is-invalid @enderror" name="country"  required  autofocus>
                                <option value="Nigeria">Nigeria</option>
                               
                                </select>
                                @error('country')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            </div> <div class="form-group">
                            <label for="state" >{{ __('State') }}</label>

                            <div >
                                
                                <select id="state"  class="form-control @error('state') is-invalid @enderror" name="state"  required  autofocus>
                                <option value="Adamawa">Adamawa</option>
                               
                                </select>
                                @error('state')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                        </div>

                        <div class="form-group">
                                <label for="local" >{{ __('Locality') }}</label>

                                <div >
                                    
                                    <select id="local"  class="form-control @error('local') is-invalid @enderror" name="local"  required  autofocus>
                                    <option value="Demsa"> Demsa</option>
                                    <option value="Fufure"> Fufure</option>
                                    <option value="Gayuk"> Gayuk</option>
                                    <option value="Gombi"> Gombi</option>
                                    <option value="Gerei"> Gerei</option>
                                    <option value="Hong"> Hong</option>
                                    <option value="Jada"> Jada</option>
                                    <option value="Larmurde"> Larmurde</option>
                                    <option value="Madagali"> Madagali</option>
                                    <option value="Maiha"> Maiha</option>
                                    <option value="Mayo Belwa"> Mayo Belwa</option>
                                    <option value="Michika"> Michika</option>
                                    <option value="Mubi North">Mubi North</option>
                                    <option value="Mubi South"> Mubi South</option>
                                    <option value="Numan"> Numan</option>
                                    <option value="Shelleng"> Shelleng</option>
                                    <option value="Song"> Song</option>
                                    <option value="Toungo"> Toungo</option>
                                    <option value="Yola North"> Yola North</option>
                                    <option value="Yola South"> Yola South</option>
                                    </select>
                                    @error('local')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="tel" >{{ __('Phone No') }}</label>

                                <div >
                                    <input id="tel" type="text" class="form-control @error('tel') is-invalid @enderror" name="tel" value="{{ old('tel') }}" required autocomplete="tel" autofocus>

                                    @error('tel')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            </div>
                       </div>







                            <div class="form-group">
                            <label for="acctype" >{{ __('I am A') }}</label>

                            <div >
                              
                                <select id="acctype"  class="form-control @error('acctype') is-invalid @enderror" name="acctype"  required  autofocus>
                                <option value="tenant">Tenant</option>
                                <option value="landlord">Landlord</option>
                                </select>
                                @error('acctype')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                       
                       </div>

                        <div class="form-group">
                            <label for="email" >{{ __('E-Mail Address') }}</label>

                            <div >
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" >{{ __('Password') }}</label>

                            <div >
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" >{{ __('Confirm Password') }}</label>

                            <div >
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>



                        <div class="form-group">
                            <label for="confirm" >{{ __('I Accept Terms & Condition') }}</label>

                            <div >
                                <input id="confirm" type="checkbox" class=" @error('confirm') is-invalid @enderror" name="confirm" required auto-focus>

                                @error('confirm')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                                
                            </div>
                        </div>
                    </form>
                        </div>
                    </div>
                </div>

               

            </div>
        </div>      

          <!-- Footer area-->
          @include('layouts.footer')
        <!-- end footer -->
@include('layouts.script')
        @endsection