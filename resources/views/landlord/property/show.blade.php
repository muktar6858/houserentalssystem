@extends('landlord.layout.header')
@include('tenant.layout.navbar')
@section('content')
@include('landlord.layout.sidebar')

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Properties</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
        <div class="col-lg-3 col-md-6">
        <div class="container">
<div class="row">
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Property {{ $property->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/landlord/property') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/landlord/property/' . $property->id . '/edit') }}" title="Edit Property"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['landlord/property', $property->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete Property',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $property->id }}</td>
                                    </tr>
                                    <tr><th> user id </th><td> {{ $property->user_id }} </td></tr><tr><th> Status </th><td> {{ $property->status }} </td></tr><tr><th> Type </th><td> {{ $property->type }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('landlord.layout.script')
@endsection
