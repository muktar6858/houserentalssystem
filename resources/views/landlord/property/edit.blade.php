@extends('landlord.layout.header')
@include('tenant.layout.navbar')
@section('content')
@include('landlord.layout.sidebar')

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Properties</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
        <div class="col-lg-3 col-md-6">
        <div class="container">
<div class="row">
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Edit Property #{{ $property->id }}</div>
                    <div class="card-body">
                        <a href="{{ url('/landlord/property') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($property, [
                            'method' => 'PATCH',
                            'url' => ['/landlord/property', $property->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('landlord.property.form', ['formMode' => 'edit'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('landlord.layout.script')
@endsection
