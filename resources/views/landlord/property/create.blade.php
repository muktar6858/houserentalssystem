
@extends('layouts.header')


@section('content')


@include('layouts.navbar')
</div>

@if($bool==true)
<div class="page-head"> 
            <div class="container">
                <div class="row">
                    <div class="page-head-content">
                       
                        <h1 class="page-title"><center>Submit new property </center> </h1>               
                    </div>
                </div>
            </div>
        </div>
        <!-- End page header -->

        <!-- property area -->
        <div class="content-area submit-property" style="background-color: #FCFCFC;">&nbsp;
            <div class="container">
                <div class="clearfix" > 
                    <div class="wizard-container"> 

                        <div class="wizard-card ct-wizard-orange" id="wizardProperty">
                                          
                                <div class="wizard-header">
                                <a href="{{ url('/landlord/allproperties') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                                    <h3>
                                        <b>Submit</b> YOUR PROPERTY <br>
                                        <small></small>
                                    </h3>
                                </div>
                                <form action="/landlord/property" method="POST" enctype="multipart/form-data">         
                            @csrf 
                                <ul>
                                    <li><a href="#step1" data-toggle="tab">Step 1 </a></li>
                                    <li><a href="#step2" data-toggle="tab">Step 2 </a></li>
                                    <li><a href="#step3" data-toggle="tab">Step 3 </a></li>
                                    <li><a href="#step4" data-toggle="tab">Finished </a></li>
                                </ul>

                                <div class="tab-content">

                                    <div class="tab-pane" id="step1">
                                        <div class="row p-b-15  ">
                                            <h4 class="info-text"> Let's start with the basic information</h4>
                                            <div class="col-sm-4 col-sm-offset-1">
                                                <div class="picture-container">
                                                    <div class="picture">
                                                        <img src="assets/img/default-property.jpg" class="picture-src" id="wizardPicturePreview" title=""/>
                                                        <input type="file" name="img"id="wizard-picture" required>
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Property Name <small>(required)</small></label>
                                                    <input name="title" type="text" class="form-control" placeholder="Super villa ..." required>
                                                </div>

                                                <div class="form-group">
                                                    <label>Property Price Per Year <small>(required)</small></label>
                                                    <input name="price" type="text" class="form-control" placeholder="3330000" required>
                                                </div> 
                                                <div class="form-group">
                                                    <label>Type <small></small></label>
                                                    <select name="type" id="" class="form-control" required>
                                                    <option >Apartment</option>
                                                    <option >Flat</option>
                                                    <option >safe content</option>
                                                    <option >house</option>
                                                    </select>
                                                </div>
                                               
                                                <div class="form-group">
                                                    
                                                    <input name="user_id" type="hidden" class="form-control" value="{{Auth::user()->id}}">
                                                </div>
                                                <div class="form-group">
                                                    
                                                    <input name="mode" type="hidden" class="form-control" value="inactive">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--  End step 1 -->

                                    <div class="tab-pane" id="step2">
                                        <h4 class="info-text"> More about your Property </h4>
                                        <div class="row">
                                            <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>Property Description :</label>
                                                        <textarea name="desc" class="form-control" required></textarea>
                                                    </div> 
                                                </div> 
                                            </div>

                                            <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>Property Address :</label>
                                                        <textarea name="addr" class="form-control" required></textarea>
                                                    </div> 
                                                </div> 
                                            </div>

                                            <div class="col-sm-12">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label>Property State :</label>
                                                        <select id="lunchBegins" name="state"class="selectpicker" data-live-search="true" data-live-search-style="begins" title="Select your city" required>
                                                            <option>Adammawa</option>
                                                          
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label>Property City :</label>
                                                        <select id="lunchBegins" class="selectpicker" name="city" data-live-search="true" data-live-search-style="begins" title="Select your city" required>
                                                            <option>Yola North</option>
                                                            <option>Yola South</option>
                                                            
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label>Property Status  :</label>
                                                        <select id="basic" class="selectpicker show-tick form-control" name="status" required>
                                                            <option> -Status- </option>
                                                            <option>Occupied</option>
                                                            <option>Vacant</option>
                
                                                        </select>
                                                    </div>
                                                </div>
                                              
                                            </div>
                                            <div class="col-sm-12 padding-top-15">                                                   
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="property-geo">No of Bedrooms :</label>
                                                        <input type="number" class="form-control" name="no_bedroom"  required><br />
                                                      
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">

                                                    <div class="form-group">
                                                        <label for="price-range">No of bathrooms :</label>
                                                        <input type="number" class="form-control" name="no_bathroom" required><br />
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">

<div class="form-group">
    <label for="price-range">No of Parking Space :</label>
    <input type="number" class="form-control" name="no_parkingsp" required><br />
    
</div>
</div>
                                                <div class="col-sm-4">

                                                    <div class="form-group">
                                                        <label for="property-geo">Property geo (m2) area :</label>
                                                        <input type="text" class="form-control" name="area">
                                                    </div>
                                                </div>   
                                            </div>
                                            <div class="col-sm-12 padding-top-15">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                    <label>Is Furnished?</label>
                                                        <select name="is_furnished" class="form-control" >
                                                           <option value="Yes">Yes</option>
                                                           <option value="No">No</option>
                                                        </select>
                                                    </div>
                                                </div> 
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <div class=" ">
                                                            <label> Is barchelors allowed?</label>

                                                            <select name="is_barchelorsa" class="form-control" >
                                                           <option value="Yes">Yes</option>
                                                           <option value="No">No</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                </div>                                                 
                                                                                              
                                                < 
                                            </div> 
                                            </div></div>
                                    <!-- End step 2 -->

                                    <div class="tab-pane" id="step3">                                        
                                        <h4 class="info-text">Give us somme images and videos ? </h4>
                                        <div class="row">  
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="property-images">Chose Images :</label>
                                                    <input class="form-control" type="file" id="property-images" name="img1">
                                                    <p class="help-block">Select  images for your property .</p>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="property-images">Chose Images :</label>
                                                    <input class="form-control" type="file" id="property-images" name="img2">
                                                    <p class="help-block">Select  images for your property .</p>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="property-images">Chose Images :</label>
                                                    <input class="form-control" type="file" id="property-images" name="img3">
                                                    <p class="help-block">Select  images for your property .</p>
                                                </div>
                                            </div>
                                            <div class="col-sm-6"> 
                                                <div class="form-group">
                                                    <label for="property-video">Property video :</label>
                                                    <input class="form-control" value="" placeholder="http://www.youtube.com, http://vimeo.com" name="video_url" type="text">
                                                </div> 

                                            </div>
                                        </div>
                                    </div>
                                    <!--  End step 3 -->


                                    <div class="tab-pane" id="step4">                                        
                                        <h4 class="info-text"> Finished and submit </h4>
                                        <div class="row">  
                                            <div class="col-sm-12">
                                                <div class="">
                                                    <p>
                                                        <label><strong>Terms and Conditions</strong></label>
                                                        By accessing or using  Am and Rent Enterprise services, such as 
                                                        posting your property advertisement with your personal 
                                                        information on our website you agree to the
                                                        collection, use and disclosure of your personal information 
                                                        in the legal proper manner
                                                    </p>

                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" required="required"> <strong>I Accept terms and conditions</strong>
                                                        </label>
                                                    </div> 

                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <!--  End step 4 -->

                                </div>

                                <div class="wizard-footer">
                                    <div class="pull-right">
                                        <input type='button' class='btn btn-next btn-primary' name='next' value='Next' />
                                        <input type='submit' class='btn btn-finish btn-primary ' name='finish' value='Finish' />
                                    </div>

                                    <div class="pull-left">
                                        <input type='button' class='btn btn-previous btn-default' name='previous' value='Previous' />
                                    </div>
                                    <div class="clearfix"></div>                                            
                                </div>	
                            </form>
                        </div>
                        <!-- End submit form -->
                    </div> 
                </div>
            </div>
        </div>


@else

<br><br><br><br><br><br><br>
<div class="row">
<div class="col-sm-4 col-sm-offset-4">
    <div class="jumbotron">
    <center><b><h2>Oops!!</h2></b></center>
<h3>you currently have no active subscription</h3>
<center><p><a href="{{url('landlord/services')}}">subscribe Now</a></p></center>
<a href="{{url('landlord/myservices')}}">view myservices</a>
</div>
</div>



@endif

    
@endsection

