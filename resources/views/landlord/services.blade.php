@if(Auth::check() && Auth::user()->acc_type=='landlord')
@include('layouts.header')
<link rel="stylesheet" href="{{asset('assets/style2.css')}}">
<div id="page-content">
        <!-- Breadcrumb -->
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active">Pricing</li>
            </ol>
        </div>
        <!-- end Breadcrumb -->

        <div class="container">
            <header><h1>Choose service</h1></header>
            <section id="pricing">
                <div class="row">
                @foreach($service as $services)
                    <div class="col-md-3 col-sm-6">
                        <div class="price-box">
                            <header><h2>{{$services->service_name}}</h2></header>
                            <div class="price">
                                <figure>&#8358;{{$services->service_price}}</figure>
                                <small>/{{$services->service_period}}</small>
                            </div>
                            <ul>
                            @if($services->service_name=='Basic')
                                <li><span>1</span> Property</li>
                                <li class="not-available">Notification</li>
                                <li class="not-available">Cross sale</li>
                                <li class="not-available">Featured Properties</li>
                            @endif
                            @if($services->service_name=='Silver')
                            <li><span>5</span> Properties</li>
                                <li><span>6</span> Notification</li>
                                <li><span>5</span> Cross sale</li>
                                <li>Featured Properties</li>
                            @endif
                            @if($services->service_name=='Gold')
                            <li><span>10</span> Properties</li>
                                <li><span>10</span> Notification</li>
                                <li><span>10</span> Cross sale</li>
                                <li>Featured Properties</li>
                            @endif
                            @if($services->service_name=='Platinum')
                            <li><span>Unlimited</span> Properties</li>
                                <li><span>Unlimited</span>Notification</li>
                                <li><span>Unlimited</span> cross sale</li>
                                <li>Featured Properties</li>
                            @endif
                            </ul>
                            <a href="{{url('landlord/invoice/'.$services->id)}}" class="btn btn-default">Select</a>
                        </div><!-- /.price-box -->
                    </div><!-- /.col-md-3 -->
                    @endforeach
                   
                </div><!-- /.row -->
            </section><!-- /#pricing -->



            @else

<script>
window.location="/login"
</script>

@endif