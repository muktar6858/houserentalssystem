

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="/landlord"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="{{url('landlord/allproperties')}}"><i class="fa fa-building fa-fw"></i>Properties<span class="fa arrow"></span></a>
                            
                            <!-- /.nav-second-level -->
                        </li>
                        
                     
                        <li>
                            <a href="{{url('landlord/payments')}}"><i class="fa fa-money fa-fw"></i>Payments</a>
                        </li>
                        <li>
                            <a href="{{url('landlord/myservice')}}"><i class="fa fa-credit-card fa-fw"></i>Services</a>
                        </li>
                        <li>
                            <a href="{{url('landlord/profile/1')}}"><i class="fa fa-users fa-fw"></i>Profile</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>