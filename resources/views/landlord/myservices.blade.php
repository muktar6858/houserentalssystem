@if(Auth::check() && Auth::user()->acc_type=='landlord')
@include('layouts.header')
<link rel="stylesheet" href="{{asset('assets/style2.css')}}">
   <br>  <br>  <br>  <br>
   <div class="row">
   <div class="col-sm-6 col-sm-offset-3">
   
   @if($bool==true)

  @foreach($sub as $subs)
  

  
  
    <section id="select-package">
                <div class="table-responsive submit-pricing">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Your Package:</th>
                            <th class="title">Amount</th>
                            
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="prices">
                            <td>{{$subs->service_name}}</td>
                            <td>{{'NGN'.$subs->service_price}}</td>
                            
                        </tr>
                        <tr>
                            <td>Property Submit Limit</td>
                            <td>1</td>
                            <td>20</td>
                            <td>Unlimited</td>
                        </tr>
                        <tr>
                            <td>Agent Profiles</td>
                            <td>1</td>
                            <td>10</td>
                            <td>Unlimited</td>
                        </tr>
                        <tr>
                            <td>Agency Profiles</td>
                            <td class="not-available"><i class="fa fa-times"></i></td>
                            <td>5</td>
                            <td>Unlimited</td>
                        </tr>
                        <tr>
                            <td>Featured Properties</td>
                            <td class="not-available"><i class="fa fa-times"></i></td>
                            <td class="available"><i class="fa fa-check"></i></td>
                            <td class="available"><i class="fa fa-check"></i></td>
                        </tr>
                        <tr class="buttons">
                            <td></td>
                            <td class="package-selected" data-package="free"><button type="button" class="btn btn-default small">Select</button></td>
                            <td data-package="silver"><button type="button" class="btn btn-default small">Select</button></td>
                            <td data-package="gold"><button type="button" class="btn btn-default small">Select</button></td>
                        </tr>
                        </tbody>
                    </table>
                </div><!-- /.submit-pricing -->
            </section><!-- /#select-package -->

            @endforeach

<a href="{{url('/landlord')}}">Back</a>

            </div>
   
   </div>

   @else

   <br><br><br><br><br><br><br>
  <div class="row">
  <div class="col-sm-4 col-sm-offset-4">
      <div class="jumbotron">
  <h3>Oops you  have no active subscription</h3>
  <h3>Click <a href="/landlord/services">here</a> to make subscription</h3>

  </div>
  </div>
  
  




   @endif
  
   @include('layouts.dtablescript')
     
      </body>
  </html>
  @else

<script>
window.location="/login"
</script>

@endif