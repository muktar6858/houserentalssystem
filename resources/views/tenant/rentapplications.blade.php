@if(Auth::check() && Auth::user()->acc_type=='tenant')
@include('layouts.dtableheader')

@include('layouts.header')
@extends('tenant.layout.header')

    




@section('content')


@include('tenant.layout.navbar')
@include('tenant.layout.sidebar')

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Welcome  {{Auth::user()->first_name.' '.Auth::user()->last_name}}</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            
            <!-- /.row -->
            <!-- /.col-lg-8 -->
            <!--row begins-->
            <div class="page-head"> 
            <div class="container">
                <div class="row">
                    <div class="page-head-content">
                       
                        <h1 class="page-title"><center>Rental Applications </center> </h1>               
                    </div>
                </div>
            </div>
        </div>

  

            @if($bool==true)
 
 <body>

 <!-- loading animation -->


                 <div class="row">
                     <div class="col-lg-12">
                         <div class="panel panel-default gradient">
                             <div class="panel-heading">
                                 <h4>
                        
                                 </h4>
                             </div>
                             <div class="panel-body noPad clearfix">
                                 <table cellpadding="0" cellspacing="0" border="0" class="tableTools display table table-bordered" width="100%">
                                     <thead>
                                         <tr>
                                             <th>FULL NAME</th>
                                             <th>CURRENT ADDRESS</th>
                                             <th>MOBILE</th>
                                             <th>DATE OF BIRTH</th>
                                             <th>IDENTITY TYPE</th>
                                             <th>ID NO</th>
                                             
                                             <th>NATIONALITY</th>
                                             <th>State of origin</th>
                                             <th>Application status</th>
                                             <th>Action</th>
                                         </tr>
                                     </thead>
                                     <tbody>
                                     @foreach($rentalapp as $rentapp)
                                        <tr><td>{{$rentapp->full_name}}</td>
                                        <td>{{$rentapp->addr}}</td>
                                        <td>{{$rentapp->tel}}</td>
                                     
                                        <td>{{$rentapp->dob}}</td>
                                        <td>{{$rentapp->id_type}}</td>
                                        <td>{{$rentapp->id_no}}</td>
                                       
                                        <td>{{$rentapp->nationatlity}}</td>
                                        <td>{{$rentapp->state_origin}}</td>
                                        <td>{{$rentapp->app_status}}</td>
                                        <td><a class="btn btn-primary" href="{{url('tenant/rentappdetails/'.$rentapp->rentapp_id)}}">view app</a>
                                       @if($rentapp->app_status=='Approved')
                                       @if($lease==1)
                                       <a class="btn btn-primary" href="{{url('tenant/invoices/'.$rentapp->rentapp_id)}}" disabled='disabled'>Proceed to Payment</a>

                                       @else
                                       <a class="btn btn-primary" href="{{url('tenant/invoices/'.$rentapp->rentapp_id)}}">Proceed to Payment</a>

                                       @endif
                                       @endif
                                        </td>
                                     
                                     </tr>
                                     @endforeach
                                     </tbody>
                                     
                                 </table>
                             </div>

                         </div><!-- End .panel -->

                     </div><!-- End .span12 -->

                 </div><!-- End .row -->
            
             <!-- Page end here -->
                 
         </div><!-- End contentwrapper -->
     </div><!-- End #content -->
 
 </div><!-- End #wrapper -->

            </div>
            <!-- /.container-fluid -->
            </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->


    @else
   
<br><br><br><br><br><br><br>
<div class="row">
    <div class="col-sm-4 col-sm-offset-4">
        <div class="jumbotron">
  <h3>Oops you currently have no rental application</h3>
  <center><p><a href="{{url('tenant/rentappform')}}">Start a new application</a></p></center>
</div>
</div>
  
@endif
   

 @include('layouts.dtablescript')
@include('layouts.script')
@endsection
@else

<script>
window.location="/login"
</script>

@endif