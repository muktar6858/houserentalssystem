@if(Auth::check() && Auth::user()->acc_type=='tenant')
@extends('layouts.header')

<link rel="stylesheet" href="{{asset('assets/style2.css')}}">
@section('content')

@include('layouts.navbar2')
<body class="page-sub-page page-invoice" id="page-top">
<!-- Wrapper -->
<div class="wrapper">
   
    <!-- Page Content -->
    <div id="page-content">
        <!-- Breadcrumb -->
      
        <!-- end Breadcrumb -->

        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <a href="javascript:window.print()" class="button-icon btn btn-default"><i class="fa fa-print"></i>Print this application</a>
                    <div class="invoice">
                        <section>
                            <table>
                                <tr>
                                    <td><small>Am and Rent Enterprise</small><h1>Rent Application</h1></td>
                                    <td id="number" class="text-align-right">
                                        <figure>ID:</figure>
                                        <h3>{{ $rentappdetails->user_id}}</h3>
                                    </td>
                                    <td id="date" class="text-align-right">
                                        <figure>Date Created:</figure>
                                        <h3>{{$rentappdetails->created_at}}</h3>
                                      
                                    </td>
                                </tr>
                            </table>
                        </section>
                        <section>
                            <aside>
                                <h2>Basic Information</h2>
                               
                                <div class="contact-info">
                                    <dl>
                                        <dt>FULL NAME:</dt>
                                        <dd>{{$rentappdetails->full_name}}</dd>
                                        <dt>CURRENT ADDRESS:</dt>
                                        <dd>{{$rentappdetails->addr}}</dd>
                                        <dt>MOBILE:</dt>
                                        <dd>{{$rentappdetails->tel}}</dd>
                                        <dt>IDENTITY TYPE:</dt>
                                        <dd>{{$rentappdetails->id_type}}</dd>
                                        <dt>ID NO:</dt>
                                        <dd>{{$rentappdetails->id_no}}</dd>
                                        <dt>SEX:</dt>
                                        <dd>{{$rentappdetails->sex}}</dd>
                                        <dt>MARITAL STATUS:</dt>
                                        <dd>{{$rentappdetails->marital_status}}</dd>
                                        
                                        <dt>NO. OF CHILDREN:</dt>
                                        <dd>{{$rentappdetails->no_children}}</dd>
                                        <dt>NO. OF CARS:</dt>
                                        <dd>{{$rentappdetails->no_cars}}</dd>
                                        <dt>TYPE OF PET(S):</dt>
                                        <dd>{{$rentappdetails->type_pet}}</dd>
                                        <dt>NO. OF PETS:</dt>
                                        <dd>{{$rentappdetails->no_pet}}</dd>
                                        <dt>NATIONALITY:</dt>
                                        <dd>{{$rentappdetails->nationatlity}}</dd>
                                        <dt>STATE OF ORIGIN:</dt>
                                        <dd>{{$rentappdetails->state_origin}}</dd>
                                       
                                    </dl>
                                </div>
                            </aside>
                            <aside>
                               
                                <address>
                                    <div class="title"><img src="{{$rentappdetails->img}}" alt="pasport"></div>
                                    <div class="address">
                                    <br>  <br> 
                                    <dt>IF MARRIED, NAME OF SPOUSE(S):</dt>
                                        <dd>{{$rentappdetails->name_spouse}}</dd>
                                        <dt>HOWLONG WILL YOU BE RENTING?:</dt>
                                        <dd>{{$rentappdetails->how_longr}}</dd>
                                        
                                        <dt>OTHER DEPENDANTS:</dt>
                                        <dd>{{$rentappdetails->no_dependant}}</dd>
                                        <dt>HOW MANY WILL BE RENTING?:</dt>
                                        <dd>{{$rentappdetails->how_manyr}}</dd>
                                        </div>
                                </address>
                                <div class="contact-info">
                                    <dl>
                                  
                                      
                                        
                                    </dl>
                                </div>
                            </aside>
                        </section>
                        <section>
                    
                            <aside>
                                <h2>NEXT OF KIN:</h2>
                               
                                <div class="contact-info">
                                    <dl>
                                        <dt>FULL NAME:</dt>
                                        <dd>{{$rentappdetails->full_name}}</dd>
                                        <dt>CURRENT ADDRESS:</dt>
                                        <dd>{{$rentappdetails->addr}}</dd>
                                        <dt>RELATIONSHIP::</dt>
                                        <dd>{{$rentappdetails->tel}}</dd>
                                        <dt>EMAIL ::</dt>
                                        <dd>{{$rentappdetails->id_type}}</dd>
                                        <dt>MOBILE ::</dt>
                                        <dd>{{$rentappdetails->id_no}}</dd>
                                        
                                    </dl>
                                </div>
                            </aside>
                   
                            <address>
                            <h2>REFEREES:</h2>
                               
                               <div class="contact-info">
                                   <dl>
                                       <dt>FULL NAME:</dt>
                                       <dd>{{$rentappdetails->full_name}}</dd>
                                       <dt>RELATIONSHIP:</dt>
                                       <dd>{{$rentappdetails->addr}}</dd>
                                       
                                   
                                       <dt>MOBILE ::</dt>
                                       <dd>{{$rentappdetails->id_no}}</dd>
                                       
                                   </dl>
                               </div>
                                </address>
                        </section>
                        <section class="no-border" id="description">
                            <h2>CURRENT TENANCY DETAILS:</h2>
                            
                            <address>
                                   
                                    <div class="address">
                    
                                    <dt>ADDRESS::</dt>
                                        <dd>{{$rentappdetails->name_spouse}}</dd>
                                        <dt>REASON FOR LEAVING:</dt>
                                        <dd>{{$rentappdetails->how_longr}}</dd>
                                        
                                        <dt>NAME OF LANDLORD/AGENT::</dt>
                                        <dd>{{$rentappdetails->no_dependant}}</dd>
                                        <dt>PHONE NO:</dt>
                                        <dd>{{$rentappdetails->how_manyr}}</dd>
                                        <dt>LENGTH OF TIME AT ABOVE ADDRESS:</dt>
                                        <dd>{{$rentappdetails->how_manyr}}</dd>
                                        <dt>HAVE YOU GIVEN NOTICE TO YOUR CURRENT LANDLORD/AGENT?::</dt>
                                        <dd>{{$rentappdetails->how_manyr}}</dd>
                                     
                                        </div>
                                </address>
                                <h2>PREVIOUS RENTAL HISTORY:</h2>
                                <address>
                                   
                                   <div class="address">
                   
                                   <dt>NAME OF LANDLORD/AGENT:</dt>
                                       <dd>{{$rentappdetails->name_spouse}}</dd>
                                       <dt>PHONE NO:</dt>
                                       <dd>{{$rentappdetails->how_longr}}</dd>
                                       
                                       <dt>WAS YOUR SECURITY DEPOSIT RETURNEED IN FULL?::</dt>
                                       <dd>{{$rentappdetails->no_dependant}}</dd>
                                       <dt>IF NO? PLEASE SPECIFY REASON WHY?:</dt>
                                       <dd>{{$rentappdetails->how_manyr}}</dd>
                                     
                                    
                                       </div>
                               </address>
                        </section>
                        <hr>


                        <section class="no-border" id="description">
                            <h2>CURRENT EMPLOYEMENT DETAILS:</h2>
                            
                            <address>
                                   
                                    <div class="contact-info">
                    
                                    <dt>EMPLOYER/ADDRESS:</dt>
                                        <dd>{{$rentappdetails->name_spouse}}</dd>
                                        <dt>OCCUPATION:</dt>
                                        <dd>{{$rentappdetails->how_longr}}</dd>
                                        
                                        <dt>NAME OF LANDLORD/AGENT::</dt>
                                        <dd>{{$rentappdetails->no_dependant}}</dd>
                                        <dt>LENGTH OF EMPLOYMENT:</dt>
                                        <dd>{{$rentappdetails->how_manyr}}</dd>
                                        <dt>CONTACT PERSON/TEL NO:</dt>
                                        <dd>{{$rentappdetails->how_manyr}}</dd>
                                     
                                     
                                        </div>
                                </address>
                                <h2>SELF EMPLOYEMENT DETAILS:</h2>
                                <address>
                                   
                                   <div class="address">
                   
                                   <dt>BUSINESS NAME/NATURE:</dt>
                                       <dd>{{$rentappdetails->name_spouse}}</dd>
                                       <dt>ADDRESS:</dt>
                                       <dd>{{$rentappdetails->how_longr}}</dd>
                                       
                                       <dt>DATE OF INCORPORATION:</dt>
                                       <dd>{{$rentappdetails->no_dependant}}</dd>
                                       <dt>POSITION HELD:</dt>
                                       <dd>{{$rentappdetails->how_manyr}}</dd>
                                       <dt>NAME(S) OF OTHER PARTNERS IN THE BUSINESS (IF ANY):</dt>
                                       <dd>{{$rentappdetails->how_manyr}}</dd>
                                       <dt>IF UNEMPLOYED, STATE MEANS OF LIVELIHOOD:</dt>
                                       <dd>{{$rentappdetails->how_manyr}}</dd>
                                       <dt>NAME OF BANKERS::</dt>
                                       <dd>{{$rentappdetails->how_manyr}}</dd>
                                       <dt>ADDRESS:</dt>
                                       <dd>{{$rentappdetails->how_manyr}}</dd>
                                    
                                       </div>
                               </address>
                        </section>


                        <section id="description-table" class="no-border">
                            <table>
                                <thead>
                                <tr>
                                 <th><center>Terms and Conditions</center></th>   
                                </tr>
                                </thead>
                                <tbody>
                                                               
                                </tbody>
                            </table>
                            <div>
                             The Tenant agrees that each of the Rental Agreement and of Landlord’s Rules and Regulations constitute a condition on Tenant’s right to possession of the premises. Any failure to comply with terms of the Rental Agreement shall  constitute a default hereunder and the Landlord may terminate tenant’s right to possession and forfeit this tenancy in any manner provided by law. Landlord may 
                            enter the premises for purpose of making repair, alteration, addition therein but without obligation to do so. Tenant shall not add or change any lock, locking device, bolt or latch on the premises without the consent to do so. Tenant shall keep the premises in a clean and sanitary manner, dispose of all rubbish, garbage and waste in a clean and sanitary manner.
                            </div>
                        </section>
                        
<div class="bg-primary"> <input color="red"type="checkbox" checked="true" disabled/>I HAVE READ, AGREED TO AND UNDERSTAND ALL THE ABOVE TERMS AND CONDITIONS THAT ARE RELEVANT TO ME
</div>
                        <section id="underline" class="no-border">
                       @if($lease==1)

@else
                        <a cladd="btn btn-primary"href="{{url('tenant/rentappeditform/'.$rentappdetails->rentapp_id)}}">Edit Application</a>
                      
                      @endif
                        </section>
                    </div>
                </div>
              
            </div><!-- /.row -->
          
        </div><!-- /.container -->
    </div>


@endsection
@else

<script>
window.location="/login"
</script>

@endif