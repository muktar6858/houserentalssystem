@if(Auth::check() && Auth::user()->acc_type=='tenant')
@extends('layouts.header')


@section('content')
<link rel="stylesheet" href="{{asset('assets/style2.css')}}">
@include('layouts.navbar2')
<body class="page-sub-page page-invoice" id="page-top">
<!-- Wrapper -->
<div class="wrapper">
   
    <!-- Page Content -->
    <div id="page-content">
        <!-- Breadcrumb -->
      
        <!-- end Breadcrumb -->
        <form method="POST" action="{{ route('pay') }}" accept-charset="UTF-8" class="form-horizontal" role="form">
    

        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <a href="javascript:window.print()" class="button-icon btn btn-default"><i class="fa fa-print"></i>Print this invoice</a>
                    <div class="invoice">
                        <section>
                            <table>
                                <tr>
                                    <td><h1>Invoice</h1></td>
                                    <td id="number" class="text-align-right">
                                        <figure>Number:</figure>
                                        <h3>{{$invoiceID}}</h3>
                                    </td>
                                    <td id="date" class="text-align-right">
                                        <figure>Date:</figure>
                                        <h3>{{today()}}</h3>
                                    </td>
                                </tr>
                            </table>
                        </section>
                        <section>
                            <aside>
                     

                           
                                <h2>Invoice For</h2>
                                <address>
                                    <div class="title">{{$rentapp->title}} &nbsp; package</div>
                                    <div class="address-line">Period: 1year</div>
                                  
                                </address>
                                <div class="contact-info">
                                    <dl>
                                        <dt>Phone:</dt>
                                        <dd>080-740-1421</dd>
                                        <dt>Email:</dt>
                                        <dd>support@hrs.com</dd>
                                        <dt>Website:</dt>
                                        <dd>www.hrs.com</dd>
                                    </dl>
                                </div>
                            </aside>
                           
                        </section>
                        <section>
                            <h4>Note:</h4>
                            <p>Rent Payment for {{$rentapp->type}} at  {{$rentapp->addr}} . payment can be made online using your credit card.
                            </p>
                        </section>
                       
                        <section id="description-table" class="no-border">
                            <table>
                                <thead>
                                <tr>
                                    <th>Apartment Type</th>
                                    <th>Period</th>
                                    <th>Unit price</th>
                                    <th>VAT</th>
                                    <th>Total Amount</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{$rentapp->type}}</td>
                                    <td>1year</td>
                                    <td>&#8358;{{$rentapp->price}}</td>
                                    <td>10%</td>
                                    <td>&#8358;{{$percent}}</td>
                                </tr>
                                
                                </tbody>
                            </table>
                        </section>
                        <section class="text-align-right" id="subtotal">
                            <aside>
                                <figure><strong>Subtotal:</strong></figure>
                                <figure><strong>Discount:</strong></figure>
                            </aside>
                            <aside>
                                <figure>&#8358;{{$rentapp->price}}</figure>
                                <figure>&#8358;0</figure>
                            </aside>
                        </section>
                        <section id="summary">
                            <h2 class="no-bottom-margin">Total Due:</h2>
                            <figure>&#8358;{{$totaldue}}</figure>
                        </section>

                        <input type="hidden" name="email" value="{{Auth::user()->email}}"> {{-- required --}}
            <input type="hidden" name="orderID" value="{{$invoiceID}}">
               
            <input type="hidden" name="amount" value="{{$totaldue.'00'}}"> {{-- required in kobo --}}
            <input type="hidden" name="quantity" value="1">
            <input type="hidden" name="metadata" value="{{ json_encode($array = ['user_id' => Auth::user()->id,'property_id'=>$rentapp->property_id,'type_ofservice'=>'RentPayment','invoice_no'=>$invoiceID ]) }}" > {{-- For other necessary things you want to add to your payload. it is optional though --}}
            <input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}"> {{-- required --}}
            <input type="hidden" name="key" value="{{ config('paystack.secretKey') }}"> {{-- required --}}
            {{ csrf_field() }} {{-- works only when using laravel 5.1, 5.2 --}}

<input type="hidden" name="_token" value="{{ csrf_token() }}"> {{-- employ this in place of csrf_field only in laravel 5.0 --}}

                        <section id="underline" class="no-border">
                            <button type="submit" class="button-icon btn btn-default">Pay online</a>
                        </section>
                    </div>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div>





 
  
   
            


</form>


@endsection

@else

<script>
window.location="/login"
</script>

@endif