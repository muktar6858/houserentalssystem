@if(Auth::check() && Auth::user()->acc_type=='tenant')
@extends('tenant.layout.header')



@section('content')


@include('tenant.layout.navbar')
@include('tenant.layout.sidebar')

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Welcome  {{$user->first_name.' '.$user->last_name}}</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            
            <!-- /.row -->
            <!-- /.col-lg-8 -->
            <!--row begins-->
            <div class="row">
                 <div class="col-lg-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bell fa-fw"></i><strong>Current lease</strong>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">

                        @if($bool==true)
                        <h3>Your Current Lease Expires On:</h3>
                        <h3>{{$lease->end_date}}</h3>

                        @else
                         @if($testbooking==true)

                        @if($tenantcheck==1)
                        <h3>Your Application is Under Review  </h3><br>
                        

                        @else
                        <h3>You can Now  </h3><br>
                        <h3><a href="/tenant/rentappform/{{$booking->property_id}}">Proceed</a> to send your Application</h3>

                        @endif
                      
                        @else
                        @if($boolschedule==true)
                        <h3>Your have Schedule site Visit ON </h3><br>
                        <h3>$checkschedul->visit_date</h3>
                        <h3>time </h3><br>
                        <h3>$checkschedul->visit_time</h3>
                        @else
                        <h3>Your have no active lease </h3><br>
                        <h3>Search for Property<a href="/allproperties">Now</a></h3>
                        @endif
@endif
                        @endif
                            <div class="list-group">
    </div>
                            <!-- /.list-group -->
                          
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
            </div><!--/row ends-->
            <!--row begins-->

            <div class="row">
                 <div class="col-lg-4">
                 @if($tenantapp>=1)
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <i class="fa fa-times fa-fw"></i> <strong>Rejected Application!!</strong>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="list-group">
                                Alert Your Application has been Rejected
                            </div>
                            <!-- /.list-group -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
            </div><!--/row ends-->
@endif
            </div>
            <!-- /.container-fluid -->
            </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
@include('layouts.script')
@endsection
@else

<script>
window.location="/login"
</script>

@endif