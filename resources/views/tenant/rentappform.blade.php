@if(Auth::check() && Auth::user()->acc_type=='tenant')
@extends('layouts.header')


@section('content')


@include('layouts.navbar')
</div>
<div class="page-head"> 
            <div class="container">
                <div class="row">
                    <div class="page-head-content">
                       
                        <h1 class="page-title"><center>Submit new rent application </center> </h1>               
                    </div>
                </div>
            </div>
        </div>
        <!-- End page header -->

        <!-- property area -->
        <div class="content-area submit-property" style="background-color: #FCFCFC;">&nbsp;
            <div class="container">
                <div class="clearfix" > 
                    <div class="wizard-container"> 

                        <div class="wizard-card ct-wizard-orange" id="wizardProperty">
                                          
                                <div class="wizard-header">
                                <a href="{{ url('tenant/rentapplications') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                                    <h3>
                                        <b>Submit</b> YOUR rental application <br>
                                        <small></small>
                                    </h3>
                                </div>
                                <form action="{{url('tenant-app')}}" method="POST" enctype="multipart/form-data">         
                            @csrf 
                                <ul>
                                    <li><a href="#step1" data-toggle="tab">Step 1 </a></li>
                                    <li><a href="#step2" data-toggle="tab">Step 2 </a></li>
                                    <li><a href="#step3" data-toggle="tab">Step 3 </a></li>
                                    <li><a href="#step4" data-toggle="tab">Step 4 </a></li>
                                    <li><a href="#step5" data-toggle="tab">Finished </a></li>
                                </ul>

                                <div class="tab-content">

                                    <div class="tab-pane" id="step1">
                                        <div class="row p-b-15  ">
                                            <h4 class="info-text"> Let's start with the basic information</h4>
                                            <div class="col-sm-4 col-sm-offset-1">
                                                <div class="picture-container">
                                                    <div class="picture">
                                                        <img src="assets/img/default-property.jpg" class="picture-src" id="wizardPicturePreview" title=""/>
                                                        <input type="file" name="img"id="wizard-picture" >
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>FULL  LEGAL NAME OF PROSPECTIVE TENANT  </label>
                                                    <input name="full_name" type="text" class="form-control" placeholder="john doe ..." >
                                                </div>

                                                <div class="form-group">
                                                    <label>CURRENT ADDRESS </label>
                                                    <input name="addr" type="text" class="form-control" placeholder="no 50 angere avenue" >
                                                </div> 

                                               
                                                <div class="form-group">
                                                    <label>HOW MANY YEARS DO YOU INTEND TO STAY?</label>
                                                    <select name="how_longr" id=""class="form-control" >
                                                    <option value="">1 Years</option>
                                                    <option value="">2 Years</option>
                                                    <option value="">3 Years</option>
                                                    </select>
                                                   
                                                </div> 


                                               
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>MOBILE</label>
                                                        <input type="text" name="tel" class="form-control" >
                                                    </div> 
                                                </div> 
                                          

                                           
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>DATE OF BIRTH</label>
                                                        <input type="date" name="dob" class="form-control" >
                                                    </div> 
                                                </div> 
                                          
                                               
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>IDENTITY TYPE </label>
                                                    <select name="id_type" type="text" class="form-control" >
                                                    <option >National ID</option>
                                                    <option >Voters Card</option>
                                                    <option >Drivers License</option>
                                                    </select>
                                                </div>  </div> 

                                                <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>ID NO </label>
                                                    <input name="id_no" type="text" class="form-control" placeholder="3330000" >
                                                </div> </div>  

                                                <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>SEX OF APPLICANT </label>
                                                    <select name="sex" type="text" class="form-control"  >
                                                
                                                        <option >Male</option>
                                                        <option >Female</option>
                                                        
                                                        </select>
                                                </div>   </div> 

                                                <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>MARITAL STATUS </label>
                                                    <select name="marital_status" type="text" class="form-control"  >
                                                    <option >Single</option>
                                                        <option >Married</option>
                                                      
                                                        </select>
                                                </div>   </div> 

                                                <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>IF MARRIED, NAME OF SPOUSE(S)  </label>
                                                    <input name="name_spouse" type="text" class="form-control" placeholder="" >
                                                </div>   </div> 

                                                <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>NO. OF CHILDREN </label>
                                                    <input name="no_children" type="number" class="form-control" placeholder="3330000" >
                                                </div>   </div> 
                                                

                                                <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>ARE THERE ANY OTHER DEPENDANTS?  </label>
                                                    <select name="no_dependant" type="text" class="form-control" >
                                                    <option >Yes</option>
                                                    <option>No</option>
                                                    </select>
                                                </div> </div> 

                                                <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>HOW MANY WILL BE RENTING?  </label>
                                                    <input name="how_manyr'" type="number" class="form-control" placeholder="3330000" >
                                                </div>  </div> 












                                               

                                                <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>NO. OF CARS </label>
                                                    <input name="no_cars" type="number" class="form-control" placeholder="3330000" >
                                                </div>  </div> 

                                                
                                                <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>TYPE OF PET(S) </label>
                                                    <input name="type_pet" type="text" class="form-control" placeholder="3330000" >
                                                </div> </div> 

                                                <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>NO. OF PETS </label>
                                                    <input name="no_pet" type="number" class="form-control" placeholder="3330000" >
                                                </div> </div> 
                                             






























                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>NATIONALITY OF APPLICANT <small></small></label>
                                                    <select name="nationality" id="" class="form-control" >
                                                    <option >Nigerian</option>
                                                    <option >United state</option>
                                                    <option >Uae</option>
                                                    <option >UK</option>
                                                    </select>
                                                </div> </div>



                                                <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>IF NIGERIAN, STATE OF ORIGIN <small></small></label>
                                                    <select name="state_origin" id="" class="form-control" >
                                                    <option >Adamawa</option>
                                                    <option >Taraba</option>
                                                    <option >Gombe</option>
                                                    <option >Maiduguri</option>
                                                    </select>
                                                </div> </div>
                                               
                                                <div class="form-group">
                                                    
                                                    <input name="user_id" type="hidden" class="form-control" value='{{Auth::user()->id}}'>
                                                </div>
                                                
                                                <div class="form-group">
                                                    
                                                <input type="hidden" name="app_status" value="inprogress" >

                                                </div>
                                                <div class="form-group">
                                                    
                                                <input type="hidden" name="property_id" value="{{$id}}" >

                                                </div>
                                                
                                               
                                            </div>
                                        </div>
                                    </div>
                                    <!--  End step 1 -->

                                    <div class="tab-pane" id="step2">
                                        <h4 class="info-text"> NEXT OF KIN: </h4>
                                        <div class="row">
                                        <div class="col-sm-12"> 
                                                <div class="col-sm-6"> 
                                                    <div class="form-group">
                                                        <label>FULL NAMES :</label>
                                                        <input type="text" name="nfull_name" class="form-control" >
                                                    </div> 
                                                </div> 
                                            

                                           
                                                <div class="col-sm-6"> 
                                                    <div class="form-group">
                                                        <label>RELATIONSHIP :</label>
                                                        <select type="text" name="nrel" class="form-control" >
                                                        <option >Father</option>
                                                        <option >Mother</option>
                                                        <option >Brother</option>
                                                        <option >Sister</option>
                                                        <option >Cousine</option>
                                                        </select>
                                                    </div> 
                                                </div> 
                                               
                                          
                                                

                                                
                                                <div class="col-sm-4"> 
                                                    <div class="form-group">
                                                        <label>ADDRESS :</label>
                                                        <input type="text" name="naddr" class="form-control" >
                                                    </div> 
                                                </div>
                                            
                                            
                                                <div class="col-sm-4"> 
                                                    <div class="form-group">
                                                        <label>EMAIL :</label>
                                                        <input type="text" name="nmail" class="form-control" >
                                                    </div> 
                                                </div> 
                                              
                                                <div class="col-sm-4"> 
                                                    <div class="form-group">
                                                        <label>MOBILE :</label>
                                                        <input type="number" name="ntel" class="form-control" >
                                                
                                                </div> </div> </div>

                                                <center><h4>REFEREES</h4></center>
                                                <div class="col-sm-12"> 
                                                <div class="col-sm-6"> 
                                                    <div class="form-group">
                                                        <label>FULL NAMES</label>
                                                        <input type="text" name="rfull_name" class="form-control" >
                                                    </div> 
                                                </div> 
                                               
                                                <div class="col-sm-6"> 
                                                    <div class="form-group">
                                                        <label>RELATIONSHIP</label>
                                                        <select type="text" name="rrel" class="form-control" >
                                                        <option >Father</option>
                                                        <option >Mother</option>
                                                        <option >Brother</option>
                                                        <option >Sister</option>
                                                        <option >Cousine</option>
                                                        </select>
                                                    </div> 
                                                </div>  </div> 
                                                <div class="col-sm-12"> 
                                                <div class="col-sm-4"> 
                                                    <div class="form-group">
                                                        <label>MOBILE</label>
                                                        <input type="text" name="rtel" class="form-control" >
                                                    </div> 
                                                </div> 
                                            </div> 
                                     </div>
                                </div>
                                    <!-- End step 2 -->

                                    <div class="tab-pane" id="step3">                                        
                                        <h4 class="info-text">CURRENT TENANCY DETAILS: </h4>
                                        <div class="row">  
                                        <div class="col-sm-12"> 
                                                <div class="col-sm-6"> 
                                                    <div class="form-group">
                                                        <label>LENGTH OF TIME AT CURRENT ADDRESS</label>
                                                        <input type="text" name="ctime_atcurrent" class="form-control" required>
                                                    </div> 
                                                </div> 
                                         

                                            
                                                <div class="col-sm-6"> 
                                                    <div class="form-group">
                                                        <label>REASON FOR LEAVING</label>
                                                        <input type="text" name="creason" class="form-control" required>
                                                    </div> 
                                                </div> 
                                            </div>

                                            <div class="col-sm-12"> 
                                                <div class="col-sm-6"> 
                                                    <div class="form-group">
                                                        <label>NAME OF LANDLORD/AGENT</label>
                                                        <input type="text" name="cname_agent" class="form-control" required>
                                                    </div> 
                                                </div>  
                                                
                                                <div class="col-sm-6"> 
                                                    <div class="form-group">
                                                        <label>PHONE NO</label>
                                                        <input type="text" name="ctel" class="form-control" required>
                                                    </div> 
                                                </div>   
                                            </div>

                                                <div class="col-sm-12"> 
                                                <div class="col-sm-6"> 
                                                    <div class="form-group">
                                                        <label>HAVE YOU GIVEN NOTICE TO YOUR CURRENT LANDLORD/AGENT?.</label>
                                                        <select type="text" name="cnotice" class="form-control" required>
                                                        <option >Yes</option>
                                                        <option >No</option>
                                                      
                                                        </select>
                                                    </div> 
                                                </div> 

                                                
                                                <div class="col-sm-6"> 
                                                    <div class="form-group">
                                                        <label>PREVIOUS ADDRESS</label>
                                                        <input type="text" name="cprev_addr" class="form-control" required>
                                                    </div> 
                                                </div> 
                                           

                                           
                                                <div class="col-sm-6"> 
                                                    <div class="form-group">
                                                        <label>LENGTH OF TIME AT ABOVE ADDRESS</label>
                                                        <input type="text" name="ctime_atprevaddr" class="form-control" required>
                                                    </div> 
                                                </div> 
                                            </div>
                                          <center><h4>PREVIOUS RENTAL HISTORY</h4></center>  
                                            <div class="col-sm-12"> 
                                                <div class="col-sm-6"> 
                                                    <div class="form-group">
                                                        <label>NAME OF LANDLORD/AGENT</label>
                                                        <input type="text" name="pname_agent" class="form-control" required>
                                                    </div> 
                                                </div>  
                                                
                                                <div class="col-sm-6"> 
                                                    <div class="form-group">
                                                        <label>PHONE NO</label>
                                                        <input type="text" name="ptel" class="form-control" required>
                                                    </div> 
                                                </div>   </div>
                                                <div class="col-sm-12"> 
                                                <div class="col-sm-6"> 
                                                    <div class="form-group">
                                                        <label>WAS YOUR SECURITY DEPOSIT RETURNEED IN FULL?</label>
                                                        <select type="text" name="psec_deposit" class="form-control" required>
                                                        <option >Yes</option>
                                                        <option >No</option>
                                                     
                                                        </select>
                                                    </div> 
                                                </div>   
                                               
                                                <div class="col-sm-6"> 
                                                    <div class="form-group">
                                                        <label>IF NO? PLEASE SPECIFY REASON WHY?</label>
                                                        <input type="text" name="preason" class="form-control" required>
                                                    </div> 
                                                </div>   </div>   




                                           
                                        </div>
                                    </div>
                                    <!--  End step 3 -->



                                    <div class="tab-pane" id="step4">
                                        <h4 class="info-text"> CURRENT EMPLOYEMENT DETAILS: </h4>
                                        <div class="row">
                                            <div class="col-sm-12"> 
                                                <div class="col-sm-6"> 
                                                    <div class="form-group">
                                                        <label>EMPLOYER/ADDRESS</label>
                                                        <input type="text" name="empaddr" class="form-control" required>
                                                    </div> 
                                                </div> 
                                         

                                            
                                                <div class="col-sm-6"> 
                                                    <div class="form-group">
                                                        <label>OCCUPATION</label>
                                                        <input type="text" name="empoccup" class="form-control" required>
                                                    </div> 
                                                </div> 
                                            </div>

                                            <div class="col-sm-12"> 
                                                <div class="col-sm-6"> 
                                                    <div class="form-group">
                                                        <label>LENGTH OF EMPLOYMENT</label>
                                                        <input type="text" name="emplenth" class="form-control" required>
                                                    </div> 
                                                </div>
                                                 
                                                <div class="col-sm-6"> 
                                                    <div class="form-group">
                                                        <label>CONTACT PERSON/TEL NO</label>
                                                        <input type="text" name="emptel" class="form-control" required>
                                                    </div> 
                                                </div>
                                             </div>

                                               <center><h2> SELF EMPLOYEMENT DETAILS</h2></center>
                                                <div class="col-sm-12"> 
                                                <div class="col-sm-6"> 
                                                    <div class="form-group">
                                                        <label>BUSINESS NAME/NATURE</label>
                                                        <input type="text" name="empbname" class="form-control" required>
                                                    </div> 
                                                </div> 
                                              
                                                <div class="col-sm-6"> 
                                                    <div class="form-group">
                                                        <label>ADDRESS</label>
                                                        <input type="text" name="empbaddr" class="form-control" required>
                                                    </div> 
                                                </div> </div>


                                                <div class="col-sm-12"> 
                                                <div class="col-sm-6"> 
                                                    <div class="form-group">
                                                        <label>DATE OF INCORPORATION</label>
                                                        <input type="text" name="empdate" class="form-control" required>
                                                    </div> 
                                                </div>
                                                <div class="col-sm-6"> 
                                                
                                                    <div class="form-group">
                                                        <label>POSITION HELD</label>
                                                        <input type="text" name="emppos" class="form-control" required>
                                                    </div> 
                                                </div></div>
                                                <div class="col-sm-12"> 
                                                <div class="col-sm-6"> 
                                                    <div class="form-group">
                                                        <label>NAME(S) OF OTHER PARTNERS IN THE BUSINESS (IF ANY)</label>
                                                        <input type="text" name="empnamepartner" class="form-control" required>
                                                    </div> 
                                                </div>
                                                
                                                <div class="col-sm-6"> 
                                                    <div class="form-group">
                                                        <label>IF UNEMPLOYED, STATE MEANS OF LIVELIHOOD</label>
                                                        <input type="text" name="empmeans" class="form-control" required>
                                                    </div> 
                                                </div></div>

                                                <div class="col-sm-12"> 
                                                <div class="col-sm-6"> 
                                                    <div class="form-group">
                                                        <label>NAME  OF BANKERS:</label>
                                                        <input type="text" name="empbankername" class="form-control" required>
                                                    </div> 
                                                </div>
                                              
                                                <div class="col-sm-6"> 
                                                    <div class="form-group">
                                                        <label> ADDRESS</label>
                                                        <input type="text" name="empbankeraddr" class="form-control" required>
                                                    </div> 
                                                </div>

                                                </div> 
                                                </div></div>

                                    <!-- end of step 4          --->


                                    <div class="tab-pane" id="step5">                                        
                                        <h4 class="info-text"> Finished and submit </h4>
                                        <div class="row">  
                                            <div class="col-sm-12">
                                                <div class="">
                                                    <p>
                                                        <label><strong>Terms and Conditions</strong></label>
                                                        The Tenant agrees that each of the Rental Agreement and of Landlord’s Rules and Regulations constitute a condition
on Tenant’s right to possession of the premises. Any failure to comply with terms of the Rental Agreement shall
constitute a default hereunder and the Landlord may terminate tenant’s right to possession and forfeit this tenancy in
any manner provided by law.
Landlord may enter the premises for purpose of making repair, alteration, addition therein but without obligation to
do so. Tenant shall not add or change any lock, locking device, bolt or latch on the premises without the consent to
do so.
Tenant shall keep the premises in a clean and sanitary manner, dispose of all rubbish, garbage and waste in a clean
and sanitary manner.
                                                    </p>

                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" > <strong>I HAVE READ, AGREED TO AND 
                                                            UNDERSTAND ALL THE ABOVE TERMS AND CONDITIONS THAT ARE RELEVANT TO
ME</strong>
                                                        </label>
                                                    </div> 

                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <!--  End step 4 -->

                                </div>

                                <div class="wizard-footer">
                                    <div class="pull-right">
                                        <input type='button' class='btn btn-next btn-primary' name='next' value='Next' />
                                        <input type='submit' class='btn btn-finish btn-primary ' name='finish' value='Finish' />
                                    </div>

                                    <div class="pull-left">
                                        <input type='button' class='btn btn-previous btn-default' name='previous' value='Previous' />
                                    </div>
                                    <div class="clearfix"></div>                                            
                                </div>	
                            </form>
                        </div>
                        <!-- End submit form -->
                    </div> 
                </div>
            </div>
        </div>

    
@endsection


@else

<script>
window.location="/login"
</script>

@endif