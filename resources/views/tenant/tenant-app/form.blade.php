<div class="form-group{{ $errors->has('user_id') ? 'has-error' : ''}}">
    {!! Form::label('user_id', 'User Id', ['class' => 'control-label']) !!}
    {!! Form::number('user_id', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('full_name') ? 'has-error' : ''}}">
    {!! Form::label('full_name', 'Full Name', ['class' => 'control-label']) !!}
    {!! Form::text('full_name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('full_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('addr') ? 'has-error' : ''}}">
    {!! Form::label('addr', 'Addr', ['class' => 'control-label']) !!}
    {!! Form::text('addr', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('addr', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('tel') ? 'has-error' : ''}}">
    {!! Form::label('tel', 'Tel', ['class' => 'control-label']) !!}
    {!! Form::text('tel', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('tel', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('sex') ? 'has-error' : ''}}">
    {!! Form::label('sex', 'Sex', ['class' => 'control-label']) !!}
    {!! Form::text('sex', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('sex', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('marital_status') ? 'has-error' : ''}}">
    {!! Form::label('marital_status', 'Marital Status', ['class' => 'control-label']) !!}
    {!! Form::text('marital_status', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('marital_status', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('name_spouse') ? 'has-error' : ''}}">
    {!! Form::label('name_spouse', 'Name Spouse', ['class' => 'control-label']) !!}
    {!! Form::text('name_spouse', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('name_spouse', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('no_children') ? 'has-error' : ''}}">
    {!! Form::label('no_children', 'No Children', ['class' => 'control-label']) !!}
    {!! Form::text('no_children', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('no_children', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('no_dependant') ? 'has-error' : ''}}">
    {!! Form::label('no_dependant', 'No Dependant', ['class' => 'control-label']) !!}
    {!! Form::text('no_dependant', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('no_dependant', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('how_longr') ? 'has-error' : ''}}">
    {!! Form::label('how_longr', 'How Longr', ['class' => 'control-label']) !!}
    {!! Form::text('how_longr', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('how_longr', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('no_cars') ? 'has-error' : ''}}">
    {!! Form::label('no_cars', 'No Cars', ['class' => 'control-label']) !!}
    {!! Form::text('no_cars', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('no_cars', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('type_pet') ? 'has-error' : ''}}">
    {!! Form::label('type_pet', 'Type Pet', ['class' => 'control-label']) !!}
    {!! Form::text('type_pet', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('type_pet', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('nationatlity') ? 'has-error' : ''}}">
    {!! Form::label('nationatlity', 'Nationatlity', ['class' => 'control-label']) !!}
    {!! Form::text('nationatlity', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('nationatlity', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('state_origin') ? 'has-error' : ''}}">
    {!! Form::label('state_origin', 'State Origin', ['class' => 'control-label']) !!}
    {!! Form::text('state_origin', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('state_origin', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('img') ? 'has-error' : ''}}">
    {!! Form::label('img', 'Img', ['class' => 'control-label']) !!}
    {!! Form::text('img', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('img', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('house_id') ? 'has-error' : ''}}">
    {!! Form::label('house_id', 'House Id', ['class' => 'control-label']) !!}
    {!! Form::number('house_id', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('house_id', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
