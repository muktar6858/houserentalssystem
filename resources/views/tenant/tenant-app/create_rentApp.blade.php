@extends('layouts.header')


@section('content')


@include('layouts.navbar')
</div>
<div class="page-head"> 
            <div class="container">
                <div class="row">
                    <div class="page-head-content">
                       
                        <h1 class="page-title"><center>Submit new Rental Application </center> </h1>               
                    </div>
                </div>
            </div>
        </div>
        <!-- End page header -->

        <!-- property area -->
        <div class="content-area submit-property" style="background-color: #FCFCFC;">&nbsp;
            <div class="container">
                <div class="clearfix" > 
                    <div class="wizard-container"> 

                        <div class="wizard-card ct-wizard-orange" id="wizardProperty">
                                          
                                <div class="wizard-header">
                                <a href="{{ url('/landlord/property') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                                    <h3>
                                        <b>Submit</b> YOUR Rental App <br>
                                        <small></small>
                                    </h3>
                                </div>
                                <form action="/landlord/property" method="POST" enctype="multipart/form-data">         
                            @csrf 
                                <ul>
                                    <li><a href="#step1" data-toggle="tab">Step 1 </a></li>
                                    <li><a href="#step2" data-toggle="tab">Step 2 </a></li>
                                    <li><a href="#step3" data-toggle="tab">Step 3 </a></li>
                                    <li><a href="#step4" data-toggle="tab">Step 4 </a></li>
                                   
                                    <li><a href="#step5" data-toggle="tab">Finished </a></li>
                                </ul>

                                <div class="tab-content">

                                    <div class="tab-pane" id="step1">
                                        <div class="row p-b-15  ">
                                            <h4 class="info-text"> Let's start with PERSONAL DETAILS:</h4>
                                            <div class="col-sm-4 col-sm-offset-1">
                                                <div class="picture-container">
                                                    <div class="picture">
                                                        <img src="assets/img/default-property.jpg" class="picture-src" id="wizardPicturePreview" title=""/>
                                                        <input type="file" name="img"id="wizard-picture" required>
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>FULL  LEGAL NAME OF PROSPECTIVE TENANT  </label>
                                                    <input name="title" type="text" class="form-control" placeholder="Super villa ..." required>
                                                </div>

                                                <div class="form-group">
                                                    <label>CURRENT ADDRESS </label>
                                                    <input name="price" type="text" class="form-control" placeholder="3330000" required>
                                                </div> 


                                                <div class="form-group">
                                                    <label>MOBILE </label>
                                                    <input name="price" type="text" class="form-control" placeholder="3330000" required>
                                                </div> 
                                                <div class="form-group">
                                                    <label>DATE OF BIRTH </label>
                                                    <input name="price" type="text" class="form-control" placeholder="3330000" required>
                                                </div> 
                                               
                                                <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>IDENTITY TYPE </label>
                                                    <input name="price" type="text" class="form-control" placeholder="3330000" required>
                                                </div>  </div> 

                                                <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>ID NO </label>
                                                    <input name="price" type="text" class="form-control" placeholder="3330000" required>
                                                </div> </div>  

                                                <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>SEX OF APPLICANT </label>
                                                    <input name="price" type="text" class="form-control" placeholder="3330000" required>
                                                </div>   </div> 

                                                <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>MARITAL STATUS </label>
                                                    <input name="price" type="text" class="form-control" placeholder="3330000" required>
                                                </div>   </div> 

                                                <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>IF MARRIED, NAME OF SPOUSE(S)  </label>
                                                    <input name="price" type="text" class="form-control" placeholder="3330000" required>
                                                </div>   </div> 

                                                <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>NO. OF CHILDREN </label>
                                                    <input name="price" type="text" class="form-control" placeholder="3330000" required>
                                                </div>   </div> 
                                                

                                                <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>ARE THERE ANY OTHER DEPENDANTS?  </label>
                                                    <input name="price" type="text" class="form-control" placeholder="3330000" required>
                                                </div> </div> 

                                                <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>HOW MANY WILL BE RENTING?  </label>
                                                    <input name="price" type="text" class="form-control" placeholder="3330000" required>
                                                </div>  </div> 

                                                <div class="col-sm-7">
                                                <div class="form-group">
                                                    <label>HOW LONG WILL YOU BE RENTING? </label>
                                                    <input name="price" type="text" class="form-control" placeholder="3330000" required>
                                                </div>  </div> 

                                                <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>NO. OF CARS </label>
                                                    <input name="price" type="text" class="form-control" placeholder="3330000" required>
                                                </div>  </div> 

                                                
                                                <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>TYPE OF PET(S) </label>
                                                    <input name="price" type="text" class="form-control" placeholder="3330000" required>
                                                </div> </div> 

                                                <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>NO. OF PETS </label>
                                                    <input name="price" type="text" class="form-control" placeholder="3330000" required>
                                                </div> </div> 
                                             
                                                <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>NATIONALITY OF APPLICANT <small></small></label>
                                                    <select name="type" id="" class="form-control" required>
                                                    <option >Nigerian</option>
                                                    <option >United state</option>
                                                    <option >Uae</option>
                                                    <option >UK</option>
                                                    </select>
                                                </div> </div>



                                                <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>IF NIGERIAN, STATE OF ORIGIN <small></small></label>
                                                    <select name="type" id="" class="form-control" required>
                                                    <option >Adamawa</option>
                                                    <option >Taraba</option>
                                                    <option >Gombe</option>
                                                    <option >Maiduguri</option>
                                                    </select>
                                                </div> </div>
                                               
                                                <div class="form-group">
                                                    
                                                    <input name="user_id" type="hidden" class="form-control" value="1">
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>

                                
                                    <!--  End step 1 -->




                                    <div class="tab-pane" id="step2">
                                        <h4 class="info-text"> NEXT OF KIN: </h4>
                                        <div class="row">
                                            <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>FULL NAMES :</label>
                                                        <input type="text" name="desc" class="form-control" required>
                                                    </div> 
                                                </div> 
                                            </div>

                                            <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>RELATIONSHIP :</label>
                                                        <input type="text" name="addr" class="form-control" required>
                                                    </div> 
                                                </div> 
                                            </div>

                                            <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>ADDRESS :</label>
                                                        <input type="text" name="desc" class="form-control" required>
                                                    </div> 
                                                </div> 
                                                <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>EMAIL :</label>
                                                        <input type="text" name="desc" class="form-control" required>
                                                    </div> 
                                                </div> 
                                                <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>MOBILE :</label>
                                                        <input type="text" name="desc" class="form-control" required>
                                                    </div> 
                                                </div> 

                                                REFEREES
                                                <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>FULL NAMES</label>
                                                        <input type="text" name="desc" class="form-control" required>
                                                    </div> 
                                                </div> 
                                                <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>RELATIONSHIP</label>
                                                        <input type="text" name="desc" class="form-control" required>
                                                    </div> 
                                                </div> 
                                                <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>MOBILE</label>
                                                        <input type="text" name="desc" class="form-control" required>
                                                    </div> 
                                                </div> 
                                    <!-- End step 2 -->

                                    <div class="tab-pane" id="step3">
                                        <h4 class="info-text"> CURRENT TENANCY DETAILS: </h4>
                                        <div class="row">
                                            <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>LENGTH OF TIME AT CURRENT ADDRESS</label>
                                                        <input type="text" name="desc" class="form-control" required>
                                                    </div> 
                                                </div> 
                                            </div>

                                            <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>REASON FOR LEAVING</label>
                                                        <input type="text" name="addr" class="form-control" required>
                                                    </div> 
                                                </div> 
                                            </div>

                                            <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>NAME OF LANDLORD/AGENT</label>
                                                        <input type="text" name="desc" class="form-control" required>
                                                    </div> 
                                                </div> 
                                                <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>PHONE NO</label>
                                                        <input type="text" name="desc" class="form-control" required>
                                                    </div> 
                                                </div> 
                                                <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>HAVE YOU GIVEN NOTICE TO YOUR CURRENT LANDLORD/AGENT?.</label>
                                                        <input type="text" name="desc" class="form-control" required>
                                                    </div> 
                                                </div> 

                                                <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>PREVIOUS ADDRESS</label>
                                                        <input type="text" name="desc" class="form-control" required>
                                                    </div> 
                                                </div> 
                                            </div>

                                            <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>LENGTH OF TIME AT ABOVE ADDRESS</label>
                                                        <input type="text" name="addr" class="form-control" required>
                                                    </div> 
                                                </div> 
                                            </div>
                                            PREVIOUS RENTAL HISTORY
                                            <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>NAME OF LANDLORD/AGENT</label>
                                                        <input type="text" name="desc" class="form-control" required>
                                                    </div> 
                                                </div> 
                                                <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>PHONE NO</label>
                                                        <input type="text" name="desc" class="form-control" required>
                                                    </div> 
                                                </div> 
                                                <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>WAS YOUR SECURITY DEPOSIT RETURNEED IN FULL?</label>
                                                        <input type="text" name="desc" class="form-control" required>
                                                    </div> 
                                                </div> 
                                                <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>IF NO? PLEASE SPECIFY REASON WHY?</label>
                                                        <input type="text" name="desc" class="form-control" required>
                                                    </div> 
                                                </div> 
                                            
                                   
                                    <!--  End step 3 -->



                                    <div class="tab-pane" id="step4">
                                        <h4 class="info-text"> CURRENT EMPLOYEMENT DETAILS: </h4>
                                        <div class="row">
                                            <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>EMPLOYER/ADDRESS</label>
                                                        <input type="text" name="desc" class="form-control" required>
                                                    </div> 
                                                </div> 
                                            </div>

                                            <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>OCCUPATION</label>
                                                        <input type="text" name="addr" class="form-control" required>
                                                    </div> 
                                                </div> 
                                            </div>

                                            <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>LENGTH OF EMPLOYMENT</label>
                                                        <input type="text" name="desc" class="form-control" required>
                                                    </div> 
                                                </div> 
                                                <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>CONTACT PERSON/TEL NO</label>
                                                        <input type="text" name="desc" class="form-control" required>
                                                    </div> 
                                                </div> 

                                                SELF EMPLOYEMENT DETAILS
                                                <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>BUSINESS NAME/NATURE</label>
                                                        <input type="text" name="desc" class="form-control" required>
                                                    </div> 
                                                </div> 
                                                <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>ADDRESS</label>
                                                        <input type="text" name="desc" class="form-control" required>
                                                    </div> 
                                                </div> 


                                                <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>DATE OF INCORPORATION</label>
                                                        <input type="text" name="desc" class="form-control" required>
                                                    </div> 
                                                </div>
                                                <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>POSITION HELD</label>
                                                        <input type="text" name="desc" class="form-control" required>
                                                    </div> 
                                                </div>
                                                <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>NAME(S) OF OTHER PARTNERS IN THE BUSINESS (IF ANY)</label>
                                                        <input type="text" name="desc" class="form-control" required>
                                                    </div> 
                                                </div>
                                                <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>IF UNEMPLOYED, STATE MEANS OF LIVELIHOOD</label>
                                                        <input type="text" name="desc" class="form-control" required>
                                                    </div> 
                                                </div>

                                                <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>NAME  OF BANKERS:</label>
                                                        <input type="text" name="desc" class="form-control" required>
                                                    </div> 
                                                </div>
                                                <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label> ADDRESS</label>
                                                        <input type="text" name="desc" class="form-control" required>
                                                    </div> 
                                                </div>

                                            
                                            
                                   
                                    <!--  End step 4 -->







                                    <div class="tab-pane" id="step5">                                        
                                        <h4 class="info-text"> Finished and submit </h4>
                                        <div class="row">  
                                            <div class="col-sm-12">
                                                <div class="">
                                                    <p>
                                                        <label><strong>Terms and Conditions</strong></label>
                                                        I HAVE READ, AGREED TO AND UNDERSTAND ALL THE TERMS AND CONDITIONS THAT ARE RELEVANT TO
ME
                                                    </p>

                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" required> <strong>Accept termes and conditions.</strong>
                                                        </label>
                                                    </div> 

                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <!--  End step 4 -->

                                </div>

                                <div class="wizard-footer">
                                    <div class="pull-right">
                                        <input type='button' class='btn btn-next btn-primary' name='next' value='Next' />
                                        <input type='submit' class='btn btn-finish btn-primary ' name='finish' value='Finish' />
                                    </div>

                                    <div class="pull-left">
                                        <input type='button' class='btn btn-previous btn-default' name='previous' value='Previous' />
                                    </div>
                                    <div class="clearfix"></div>                                            
                                </div>	
                            </form>
                        </div>
                        <!-- End submit form -->
                    </div> 
                </div>
            </div>
        </div>

    
@endsection