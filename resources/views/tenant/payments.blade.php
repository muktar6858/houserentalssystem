@if(Auth::check() && Auth::user()->acc_type=='tenant')
@include('layouts.dtableheader')


@extends('tenant.layout.header')

@include('layouts.header')

@section('content')


@include('tenant.layout.navbar')
@include('tenant.layout.sidebar')

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Welcome  {{Auth::user()->first_name.' '.Auth::user()->last_name}}</h1>

                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            
            <!-- /.row -->
            <!-- /.col-lg-8 -->
            <!--row begins-->

            <div class="page-head"> 
            <div class="container">
                <div class="row">
                    <div class="page-head-content">
                       
                        <h1 class="page-title"><center>Payment History </center> </h1>               
                    </div>
                </div>
            </div>
        </div>
 
            @if($bool=true)
  
    
  </head>
    
  <body>

  <!-- loading animation -->


                  <div class="row">
                      <div class="col-lg-12">
                          <div class="panel panel-default gradient">
                              <div class="panel-heading">
                                  <h4>
                                  </h4>
                              </div>
                              <div class="panel-body noPad clearfix">
                                  <table cellpadding="0" cellspacing="0" border="0" class="tableTools display table table-bordered" width="100%">
                                      <thead>
                                          <tr>
                                              <th>Payment for</th>
                                              <th>Status</th>
                                              <th>Invoice number</th>
                                              <th>Amount Paid</th>
                                              <th>Payment date</th>
                                          
                                              
                                          </tr>
                                      </thead>
                                      <tbody>
                                      @foreach($payment as $payments)
                                         <tr><td>Rent Payment</td>
                                         <td>{{$payments->status}}</td>
                                         <th>{{$payments->invoice_no}}</th>
                                         <td>{{$payments->amount}}</td>
                                         <td>{{$payments->created_at->diffForHumans()}}</td>
                                         
                                      </tr>
                                      @endforeach
                                      </tbody>
                                      
                                  </table>
                              </div>

                          </div><!-- End .panel -->

                      </div><!-- End .span12 -->

                  </div><!-- End .row -->
             
              <!-- Page end here -->
                  
          </div><!-- End contentwrapper -->
      </div><!-- End #content -->
  
  </div><!-- End #wrapper -->


  @else

<br><br><br><br><br><br><br>
<div class="row">
<div class="col-sm-4 col-sm-offset-4">
  <div class="jumbotron">
<h3>Oops you  have no payment history</h3>

</div>
</div>



@endif


@include('layouts.dtablescript')
 



 
            </div>
            <!-- /.container-fluid -->
            </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
@include('layouts.script')
@endsection
@else

<script>
window.location="/login"
</script>

@endif