
</div>
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="#"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="{{url('tenant/lease')}}"><i class="fa fa-building fa-fw"></i>Lease</a>
                            
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="{{url('tenant/rentapplications')}}"><i class="fa fa-calendar fa-fw"></i>Application</a>
                        </li>
                        <li>
                            <a href="{{url('tenant/payment')}}"><i class="fa fa-credit-card fa-fw"></i>Payments</a>
                        </li>
                        
                        <li>
                            <a href="{{ url('tenant/profile/'.Auth::user()->id) }}"><i class="fa fa-users fa-fw"></i>Profile</a>
                        </li>
                      
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>