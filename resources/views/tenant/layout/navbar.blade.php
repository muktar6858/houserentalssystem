
    <div id="wrapper">

<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#"><span class="icon-bar"><img src="{{ asset('images/logo.png')}}"></span> &nbsp; House Rental System</a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
        <!-- /.dropdown -->
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
            </a>
             <ul class="dropdown-menu">
      <li><a href="/profile"><i class="fa fa-user"></i> <strong>My Profile Details</strong> </a></li>
      <!--login/logout area starts-->
      <li>
       
       <a href="/profile" class="btn btn-danger navbar-btn btn-sm hidden-xs"><i class="fa fa-cog"></i> <strong> Profile </strong></a>
 
       <p class="navbar-text navbar-right">&nbsp;</p>
       <a href=" #" class="btn btn-success navbar-btn btn-sm navbar-right"><strong> {{Auth::user()->name}} </strong></a>
       <p class="navbar-text navbar-right">
      
      </p>
      
      <ul class="nav navbar-nav navbar-right hidden-xs" style="min-width: 330px;">
      </ul>
      <ul class="nav navbar-nav visible-xs">
      </ul>
       
       
    </li>
    <!--login/logout area ends-->
    <li class="divider"></li>
    <li><a class="btn navbar-btn btn-warning" href="{{url('/logout')}}"><i class="fa fa-power-off"></i> <strong style="color:black"> Logout </strong> </a></li>
  </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->
