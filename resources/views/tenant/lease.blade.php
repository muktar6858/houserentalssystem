@if(Auth::check() && Auth::user()->acc_type=='tenant')
@include('layouts.dtableheader')

@include('layouts.header')

@extends('tenant.layout.header')


 
@section('content')


@include('tenant.layout.navbar')
@include('tenant.layout.sidebar')

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Welcome  {{Auth::user()->first_name.' '.Auth::user()->last_name}}</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            
            <!-- /.row -->
            <!-- /.col-lg-8 -->
            <!--row begins-->
 

            <div class="page-head"> 
            <div class="container">
                <div class="row">
                    <div class="page-head-content">
                       
                        <h1 class="page-title"><center>Lease </center> </h1>               
                    </div>
                </div>
            </div>
        </div>
   
@if($count==1)
     
                                <br><br><br><br><br><br><br>
                        <div class="row">
                            <div class="col-sm-4 col-sm-offset-4">
                       
                                <div class="jumbotron">
                             
                       <center> <h2>Current Lease</h2></center>
                        <small>Start Date:&nbsp;</small>
                     <strong>{{ $lease->start_date}}</strong>
<hr>

                     <small>End Date:&nbsp;</small>
                     <strong>{{ $lease->end_date }}</strong>
                     <br>  
                     <small>Status:&nbsp;</small>
                     <strong>{{ $lease->rent_status }}</strong>
                     <br>   <br>
                     <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Request Lease Termination</button>

                        </div>
                        </div>



   <!-- Trigger the modal with a button -->

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Lease Termination Request</h4>
      </div>
      <div class="modal-body">
        <form action="{{url('landlord/requestermination')}}" method="POST">
        @csrf 

          <label for="reasons">Enter Your Reasons</label>
        <textarea  class="form-control "name="reasons"></textarea>
        <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
        <input type="hidden" name="lease_id" value="{{$lease->property->id}}">
        <input type="hidden" name="status" value="inview">

        <input type="submit"  value="submit">
  
       
      </div>
      <div class="modal-footer">
      
      </form>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div

















@else
    <br><br><br><br><br><br><br>
<div class="row">
    <div class="col-sm-4 col-sm-offset-4">
        <div class="jumbotron">
  <h3>Oops you currently have no active Lease</h3>
  <center><p><a href="#">find a dwelling now</a></p></center>
</div>
</div>
  @endif


 




 @include('layouts.dtablescript')


 
            </div>
            <!-- /.container-fluid -->
            </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
@include('layouts.script')
@endsection
@else

<script>
window.location="/login"
</script>

@endif