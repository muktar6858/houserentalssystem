   <!-- Le javascript
    ================================================== -->
    <!-- Important plugins put in all pages -->
    <script  type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="{{asset('plugins/js/bootstrap/bootstrap.js')}}"></script>  
    <script type="text/javascript" src="{{asset('plugins/js/jquery.mousewheel.js')}}"></script>
    <script type="text/javascript" src="{{asset('plugins/js/libs/jRespond.min.js')}}"></script>

    <!-- Charts plugins -->
    <script type="text/javascript" src="{{asset('plugins/charts/sparkline/jquery.sparkline.min.js')}}"></script><!-- Sparkline plugin -->
   
    <!-- Misc plugins -->
    <script type="text/javascript" src="{{asset('plugins/misc/nicescroll/jquery.nicescroll.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('plugins/misc/qtip/jquery.qtip.min.js')}}"></script><!-- Custom tooltip plugin -->
    <script type="text/javascript" src="{{asset('plugins/misc/totop/jquery.ui.totop.min.js')}}"></script> 

    <!-- Search plugin -->
    <script type="text/javascript" src="{{asset('plugins/misc/search/tipuesearch_set.js')}}"></script>
    <script type="text/javascript" src="{{asset('plugins/misc/search/tipuesearch_data.js')}}"></script><!-- JSON for searched results -->
    <script type="text/javascript" src="{{asset('plugins/misc/search/tipuesearch.js')}}"></script>

    <!-- Form plugins -->
    <script type="text/javascript" src="{{asset('plugins/forms/uniform/jquery.uniform.min.js')}}"></script>

    <!-- Table plugins -->
    <script type="text/javascript" src="{{asset('plugins/tables/dataTables/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('plugins/tables/dataTables/TableTools.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('plugins/tables/dataTables/ZeroClipboard.js')}}"></script>
    <script type="text/javascript" src="{{asset('plugins/tables/responsive-tables/responsive-tables.js')}}"></script><!-- Make tables responsive -->

    <!-- Init plugins -->
    <script type="text/javascript" src="{{asset('plugins/js/main.js')}}"></script><!-- Core js functions -->
    <script type="text/javascript" src="{{asset('plugins/js/datatable.js')}}"></script><!-- Init plugins only for page -->