

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="#"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-building fa-fw"></i>Houses<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="http://localhost/rentalmanagement/houses_view.php?SortField=&SortDirection=&FilterAnd%5B1%5D=and&FilterField%5B1%5D=5&FilterOperator%5B1%5D=equal-to&FilterValue%5B1%5D=vaccant">Vaccant</a>
                                </li>
                                <li>
                                    <a href="http://localhost/rentalmanagement/houses_view.php?SortField=&SortDirection=&FilterAnd%5B1%5D=and&FilterField%5B1%5D=5&FilterOperator%5B1%5D=equal-to&FilterValue%5B1%5D=occupied">Occupied</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="tenants_view.php"><i class="fa fa-users fa-fw"></i>Tenants</a>
                        </li>
                        <li>
                            <a href="invoices_view.php"><i class="fa fa-credit-card fa-fw"></i>Invoices</a>
                        </li>
                        <li>
                            <a href="payments_view.php"><i class="fa fa-money fa-fw"></i>Payments</a>
                        </li>
                        <li>
                            <a href="http://localhost/rentalmanagement/payments_view.php?SortField=&SortDirection=&FilterAnd%5B1%5D=and&FilterField%5B1%5D=8&FilterOperator%5B1%5D=greater-than&FilterValue%5B1%5D=0"><i class="fa fa-list-alt fa-fw"></i>Outstanding Balances</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>