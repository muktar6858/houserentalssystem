<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'HRS') }}</title>

    <!-- Styles -->
   


<link  href="{{asset('assets/css/normalize.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/fontello.css')}}">
<link href="{{asset('assets/fonts/icon-7-stroke/css/pe-icon-7-stroke.css')}}" rel="stylesheet">
<link href="{{asset('assets/fonts/icon-7-stroke/css/helper.css')}}" rel="stylesheet">
<link href="{{asset('assets/css/animate.css')}}" rel="stylesheet" media="screen">
<link rel="stylesheet" href="{{asset('assets/css/bootstrap-select.min.css')}}"> 
 <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/icheck.min_all.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/price-range.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/owl.carousel.css')}}">  
<link rel="stylesheet" href="{{asset('assets/css/owl.theme.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/owl.transitions.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/wizard.css')}}"> 
<link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/lightslider.min.css')}}">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work  you view the page via file:// -->
<!--[ lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![end]-->
</head>
<body>
   

        <main class="py-4">
            @yield('content')
        </main>
    </div>

  
      <!-- Scripts -->
      @include('layouts.script')
</body>
</html>
