

        <nav class="navbar navbar-default ">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigation">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                  
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse yamm" id="navigation">
                <a class="navbar-brand" href="/"><img  class="img img-responsive"src="{{ asset('images/logo.png')}}" alt="">HRS</a>
                    <div class="button navbar-right">

                    @if(!Auth::check())
                   
                        <button class="navbar-btn nav-button wow bounceInRight login" onclick=" window.location.href='login'" data-wow-delay="0.45s">Login</button>
                        <button class="navbar-btn nav-button wow fadeInRight" onclick=" window.location.href='register'" data-wow-delay="0.48s">Register</button>
                   @else

                  

                    @endif
                    </div>
                    <ul class="main-nav nav navbar-nav navbar-right">
                        <li class="dropdown ymm-sw " data-wow-delay="0.1s">
                            <a href="/"  data-delay="200">Home <b ></b></a>
                            
                        </li>

                        @if(Auth::check() && Auth::user()->acc_type=='tenant')
                   <button class="navbar-btn nav-button wow bounceInRight login" onclick=" window.location.href='/tenant'" data-wow-delay="0.45s">Dashboard</button>
                   
                    @endif
                    @if(Auth::check() && Auth::user()->acc_type=='landlord')
                   <button class="navbar-btn nav-button wow bounceInRight login" onclick=" window.location.href='/landlord'" data-wow-delay="0.45s">Dashbord</button>
                   
                    @endif

                    @if(Auth::check() && (Auth::user()->acc_type=='manager' || Auth::user()->acc_type=='staff'))
                   <button class="navbar-btn nav-button wow bounceInRight login" onclick=" window.location.href='/admin'" data-wow-delay="0.45s">Dashbord</button>
                   
                    @endif
                        <li class="wow fadeInDown" data-wow-delay="0.2s"><a class="" href="/properties">Properties</a></li>
                        <li class="wow fadeInDown" data-wow-delay="0.3s"><a class="" href="/faq">Faq</a></li>
                        <li class="dropdown yamm-fw" data-wow-delay="0.4s">
                            <a href="/about" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">About us <b></b></a>
                            <ul class="dropdown-menu">
                                <li>
                                   

                       
                    </ul>
                    <li class="wow fadeInDown" data-wow-delay="0.5s"><a href="/contactus">Contact US</a></li>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <!-- End of nav bar -->