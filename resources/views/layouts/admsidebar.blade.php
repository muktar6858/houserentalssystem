

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="/admin"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="{{url('admin/properties')}}"><i class="fa fa-building fa-fw"></i>Properties<span class="fa arrow"></span></a>
                            
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="{{url('admin/lease')}}"><i class="fa fa-list-alt fa-fw"></i>Lease</a>
                        </li>
@if(Auth::user()->acc_type=='manager')
                        <li>
                            <a href="{{url('admin/users')}}"><i class="fa fa-users fa-fw"></i>Users</a>
                        </li>

                        <li>
                            <a href="{{url('admin/services')}}"><i class="fa fa-users fa-fw"></i>Services</a>
                        </li>

                        <li>
                            <a href="{{url('admin/leasetermination')}}"><i class="fa fa-credit-card fa-fw"></i>Lease Termination Request</a>
                        </li>
@endif
                        <li>
                            <a href="{{url('admin/payments')}}"><i class="fa fa-money fa-fw"></i>Payments</a>
                        </li>
                        <li>
                            <a href="{{url('admin/subscription')}}"><i class="fa fa-credit-card fa-fw"></i>Subscriptions</a>
                        </li>
                        <li>
                            <a href="{{url('admin/rentapplications')}}"><i class="fa fa-credit-card fa-fw"></i>Rental Applications</a>
                        </li>
                        <li>
                            <a href="{{url('admin/booking')}}"><i class="fa fa-credit-card fa-fw"></i>Bookings</a>
                        </li>
                        <li>
                            <a href="{{url('admin/contactus')}}"><i class="fa fa-credit-card fa-fw"></i>Contacts/Complains</a>
                        </li>

                      

                        <li>
                            <a href="{{url('admin/profile/'.Auth::user()->id)}}"><i class="fa fa-users fa-fw"></i>Profile</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>