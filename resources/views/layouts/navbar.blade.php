
    <div id="wrapper">

<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#"><span class="icon-bar"><img src="{{ asset('images/logo.png')}}"></span> &nbsp; House Rental System</a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
        <!-- /.dropdown -->
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
            </a>
             <ul class="dropdown-menu">
      <li><a href=" PREPEND_PATH; membership_profile.php"><i class="fa fa-user"></i> <strong>My Profile Details</strong> </a></li>
      <!--login/logout area starts-->
      <li>
       
       <a href=" PREPEND_PATH; admin/pageHome.php" class="btn btn-danger navbar-btn btn-sm hidden-xs"><i class="fa fa-cog"></i> <strong> ['admin area']; </strong></a>
       <a href=" PREPEND_PATH; admin/pageHome.php" class="btn btn-danger navbar-btn btn-sm visible-xs btn-sm"><i class="fa fa-cog"></i> <strong> ['admin area']; </strong></a>
        
     ff
       <p class="navbar-text navbar-right">&nbsp;</p>
       <a href=" PREPEND_PATH; index.php?signIn=1" class="btn btn-success navbar-btn btn-sm navbar-right"><strong> mm </strong></a>
       <p class="navbar-text navbar-right">
      
      </p>
      
      <ul class="nav navbar-nav navbar-right hidden-xs" style="min-width: 330px;">
      </ul>
      <ul class="nav navbar-nav visible-xs">
      </ul>
       
       
    </li>
    <!--login/logout area ends-->
    <li class="divider"></li>
    <li><a class="btn navbar-btn btn-warning" href=" PREPEND_PATH; index.php?signOut=1"><i class="fa fa-power-off"></i> <strong style="color:black"> ['sign out']; </strong> </a></li>
  </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    