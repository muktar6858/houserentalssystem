   <!-- Footer area-->
   <div class="footer-area">

<div class=" footer">
    <div class="container">
        <div class="row">

            <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                <div class="single-footer">
                    <h4>About us </h4>
                    <div class="footer-title-line"></div>

                    <img src="images/favicon.png" alt="" class="wow pulse" data-wow-delay="1s">HRS
                    <p>Am and Rent Enterprise Yola, Adamawa, State</p>
                    <ul class="footer-adress">
                        <li><i class="pe-7s-map-marker strong"> </i>No 164, Mohammed Mustapha Way, Jimeta, Yola, Adamawa, Nigeria</li>
                        <li><i class="pe-7s-mail strong"> </i> support@hrs.com</li>
                        <li><i class="pe-7s-call strong"> </i> +234 8038781709</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                <div class="single-footer">
                    <h4>Quick links </h4>
                    <div class="footer-title-line"></div>
                    <ul class="footer-menu">
                        <li><a href="/properties">Properties</a>  </li> 
                        <li><a href="#">Services</a>  </li> 
                        <li><a href="#">Submit property </a></li> 
                        <li><a href="/contactus">Contact us</a></li> 
                        <li><a href="/faq">fqa</a>  </li> 
                        <li><a href="#">Terms </a>  </li> 
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                <div class="single-footer">
                    <h4>Last News</h4>
                    <div class="footer-title-line"></div>
                    <ul class="footer-blog">
                        <li>
                            <div class="col-md-3 col-sm-4 col-xs-4 blg-thumb p0">
                                <a href="single.html">
                                    <img src="assets/img/demo/small-proerty-2.jpg">
                                </a>
                                <span class="blg-date">11-28-2019</span>

                            </div>
                            <div class="col-md-8  col-sm-8 col-xs-8  blg-entry">
                                <h6> <a href="#">Add news functions </a></h6> 
                                <p style="line-height: 17px; padding: 8px 2px;">Site have been updated</p>
                            </div>
                        </li> 

                        <li>
                            <div class="col-md-3 col-sm-4 col-xs-4 blg-thumb p0">
                                <a href="#">
                                    <img src="assets/img/demo/small-proerty-2.jpg">
                                </a>
                                <span class="blg-date">11-28-2019</span>

                            </div>
                            <div class="col-md-8  col-sm-8 col-xs-8  blg-entry">
                                <h6> <a href="#">Add news functions </a></h6> 
                                <p style="line-height: 17px; padding: 8px 2px;">Site have been updated</p>
                            </div>
                        </li> 

                        <li>
                            <div class="col-md-3 col-sm-4 col-xs-4 blg-thumb p0">
                                <a href="#">
                                    <img src="assets/img/demo/small-proerty-2.jpg">
                                </a>
                                <span class="blg-date">11-28-2019</span>

                            </div>
                            <div class="col-md-8  col-sm-8 col-xs-8  blg-entry">
                                <h6> <a href="#">Add news functions </a></h6> 
                                <p style="line-height: 17px; padding: 8px 2px;">new features added voila ...</p>
                            </div>
                        </li> 


                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                <div class="single-footer news-letter">
                    <h4>Stay in touch</h4>
                    <div class="footer-title-line"></div>
                    <p>subscribe to our news letters</p>

                    <form>
                        <div class="input-group">
                            <input class="form-control" type="text" placeholder="E-mail ... ">
                            <span class="input-group-btn">
                                <button class="btn btn-primary subscribe" type="button"><i class="pe-7s-paper-plane pe-2x"></i></button>
                            </span>
                        </div>
                        <!-- /input-group -->
                    </form> 

                    <div class="social pull-right"> 
                        <ul>
                            <li><a class="wow fadeInUp animated" href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a class="wow fadeInUp animated" href="#" data-wow-delay="0.2s"><i class="fa fa-facebook"></i></a></li>
                            <li><a class="wow fadeInUp animated" href="#" data-wow-delay="0.3s"><i class="fa fa-google-plus"></i></a></li>
                            <li><a class="wow fadeInUp animated" href="#" data-wow-delay="0.4s"><i class="fa fa-instagram"></i></a></li>
                            <li><a class="wow fadeInUp animated" href="#" data-wow-delay="0.6s"><i class="fa fa-dribbble"></i></a></li>
                        </ul> 
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="footer-copy text-center">
    <div class="container">
        <div class="row">
            <div class="pull-left">
                <span> Copyright(C) Am and Rent Enterprise , All rights reserved 2019  </span> 
            </div> 
            <div class="bottom-menu pull-right"> 
                <ul> 
                    <li><a class="wow fadeInUp animated" href="/" data-wow-delay="0.2s">Home</a></li>
                    <li><a class="wow fadeInUp animated" href="/properties" data-wow-delay="0.3s">Property</a></li>
                    <li><a class="wow fadeInUp animated" href="/faq" data-wow-delay="0.4s">Faq</a></li>
                    <li><a class="wow fadeInUp animated" href="contactus" data-wow-delay="0.6s">Contact</a></li>
                </ul> 
            </div>
        </div>
    </div>
</div>

</div>
