@extends('layouts.header')


@section('content')

      @include('layouts.navbar2')
</div>
        <!-- End of nav bar -->

        <div class="page-head"> 
            <div class="container">
                <div class="row">
                    <div class="page-head-content">
                        <h1 class="page-title"> <center> {{ __('Update Profile') }} </center> </h1>               
                    </div>
                </div>
            </div>
        </div>
        <!-- End page header -->
 

        <!-- register-area -->
        <div class="register-area" style="background-color: rgb(249, 249, 249);">
            <div class="container">

                <div class="col-md-6 col-md-offset-4">

               
                                            







                    <div class="box-for overflow">
                        <div class="col-md-12 col-xs-12  register-blocks">
                            <h2>Update account : </h2> 
                            <form method="POST" action="{{url('/admin/updateuser/'.$user->id)}}" enctype='multipart/form-data'>
                        @csrf

                                              
<div class="form-group">
                            <label for="first_name" >{{ __('First Name') }}</label>

                            <div >
                                <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ $user->first_name }}"  autocomplete="first_name" autofocus>

                                @error('first_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> <div class="form-group">
                            <label for="middle_name" >{{ __('Middle Name') }}</label>

                            <div >
                                <input id="middle_name" type="text" class="form-control @error('middle_name') is-invalid @enderror" name="middle_name" value="{{ $user->middle_name }}"  autocomplete="middle_name" autofocus>

                                @error('middle_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> <div class="form-group">
                            <label for="last_name" >{{ __('Last Name') }}</label>

                            <div >
                                <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ $user->last_name }}"  autocomplete="last_name" autofocus>

                                @error('last_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> <div class="form-group">
                            <label for="gender" >{{ __('Gender') }}</label>

                            <div >
                                <select id="gender" class="form-control @error('gender') is-invalid @enderror" name="gender" value="{{ $user->gender }}"  autocomplete="gender" autofocus>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                                </select>

                                @error('gender')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> <div class="form-group">
                            <label for="dob" >{{ __('Date of Birth') }}</label>

                            <div >
                                <input id="dob" type="date" class="form-control @error('dob') is-invalid @enderror" name="dob" required  value="{{ $user->dob }}" autocomplete="dob" autofocus>

                                @error('dob')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> <div class="form-group">
                            <label for="addr" >{{ __('Address') }}</label>

                            <div >
                               <input type="text" row="4"id="addr" col="10"="5" class="form-control @error('addr') is-invalid @enderror" name="addr" value="{{ $user->addr }}"  required autocomplete="addr" autofocus>
                               

                                @error('addr')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> <div class="form-group">
                            <label for="country" >{{ __('Country') }}</label>

                            <div >
                              <select id="country"  class="form-control @error('country') is-invalid @enderror" name="country"  value="{{ $user->country }}" required  autofocus>
                                <option value="Nigeria">Nigeria</option>
                               
                                </select>
                                @error('country')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            </div> <div class="form-group">
                            <label for="state" >{{ __('State') }}</label>

                            <div >
                                
                                <select id="state"  class="form-control @error('state') is-invalid @enderror" name="state"  value="{{ $user->state }}" required  autofocus>
                                <option value="Adamawa">Adamawa</option>
                               
                                </select>
                                @error('state')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                        </div>

                        <div class="form-group">
                                <label for="locality" >{{ __('localityity') }}</label>

                                <div >
                                    
                                    <select id="locality"  class="form-control @error('locality') is-invalid @enderror" name="locality" value="{{ $user->locality }}"  required  autofocus>
                                    <option value="Demsa"> Demsa</option>
                                    <option value="Fufure"> Fufure</option>
                                    <option value="Gayuk"> Gayuk</option>
                                    <option value="Gombi"> Gombi</option>
                                    <option value="Gerei"> Gerei</option>
                                    <option value="Hong"> Hong</option>
                                    <option value="Jada"> Jada</option>
                                    <option value="Larmurde"> Larmurde</option>
                                    <option value="Madagali"> Madagali</option>
                                    <option value="Maiha"> Maiha</option>
                                    <option value="Mayo Belwa"> Mayo Belwa</option>
                                    <option value="Michika"> Michika</option>
                                    <option value="Mubi North">Mubi North</option>
                                    <option value="Mubi South"> Mubi South</option>
                                    <option value="Numan"> Numan</option>
                                    <option value="Shelleng"> Shelleng</option>
                                    <option value="Song"> Song</option>
                                    <option value="Toungo"> Toungo</option>
                                    <option value="Yola North"> Yola North</option>
                                    <option value="Yola South"> Yola South</option>
                                    </select>
                                    @error('locality')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="phone" >{{ __('Phone No') }}</label>

                                <div >
                                    <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ $user->phone }}"  autocomplete="phone" autofocus>

                                    @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            </div>
                       </div>              

                        <div class="form-group">
                                <label for="email" >{{ __('Email') }}</label>

                                <div >
                                    <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $user->email }}"  autocomplete="email" autofocus>

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            </div>
                       </div>                      

                        <div class="form-group">
                            <label for="password" >{{ __('Password') }}</label>

                            <div >
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" >{{ __('Confirm Password') }}</label>

                            <div >
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation"  required  autocomplete="new-password">
                            </div>
                        </div>


                        <div class="form-group">
                                                    <label for="property-images">Chose Images :</label>
                                                    <input name="img"class="form-control" type="file" id="property-images">
                                                    <p class="help-block">Select  image for your profile .</p>
                                                </div>
                        


                        <div class="form-group mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Update') }}
                                </button>
                                
                            </div>
                        </div>
                    </form>
                        </div>
                    </div>
                </div>
              
               

            </div>
        </div>      

          <!-- Footer area-->
          @include('layouts.footer')
        <!-- end footer -->
@include('layouts.script')
        @endsection