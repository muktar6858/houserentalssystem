
@include('layouts.dtableheader')

@include('layouts.header')



                




 @extends('tenant.layout.header')



@section('content')


@include('tenant.layout.navbar')
</div>
@include('layouts.admsidebar')



        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Welcome  Back {{Auth::user()->name}}</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
              
                <div class="page-head"> 
            <div class="container">
                <div class="row">
                    <div class="page-head-content">
                       
                        <h1 class="page-title"><center>All Properties </center> </h1>               
                    </div>
                </div>
            </div>
        </div>


        @if ($message = Session::get('flash_message'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif

        <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default gradient">
                                <div class="panel-heading">
                                    <h4>
                                    </h4>
                                </div>
                                <div class="panel-body noPad clearfix">
                                    <table cellpadding="0" cellspacing="0" border="0" class="tableTools display table table-bordered" width="100%">
                                        <thead>
                                        
                                            <tr>
                                                <th>Apart Type</th>
                                                <th>Address</th>
                                                <th>State</th>
                                                <th>City</th>
                                                <th>Status</th>
                                                <th>Price</th>
                                                <th>Mode</th>
                                                <th>Action</th>
                                            </tr>
                                            
                                        </thead>
                                        <tbody>
                                  
                                    @foreach($property as $prop)
                                    
                                    <tr>
                                     
                                           <td>{{$prop->title}}</td>
                                           <td>{{$prop->addr}}</td>
                                           <td>{{$prop->state}}</td>
                                           <td>{{$prop->city}}</td>
                                           <td>{{$prop->status}}</td>
                                          
                                           <td>{{'NGN'.$prop->price}}</td>
                                           <td>{{$prop->mode}}</td>
                                           <td><a class="btn btn-success" href='/admin/showproperties/{{$prop->id}}'> Show</a>
                                         
                                            </td>
                                         
                                           </tr>
                                    

                                       @endforeach
                                        </tbody>
                                        
                                    </table>
                                </div>

                            </div><!-- End .panel -->

                        </div><!-- End .span12 -->

                    </div><!-- End .row -->
               
    			<!-- Page end here -->
    				
            </div><!-- End contentwrapper -->
        </div><!-- End #content -->
    
    </div><!-- End #wrapper -->

 @include('layouts.dtablescript')










            </div><!--/row ends-->

            </div>
            <!-- /.container-fluid -->
            </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <script src="vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="vendor/metisMenu/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
@endsection

