
@extends('layouts.header')


@section('content')


@include('layouts.navbar')

<div class="page-head"> 
            <div class="container">
                <div class="row">
                    <div class="page-head-content">
                       
                        <h1 class="page-title"><center>Update Property Record </center> </h1>               
                    </div>
                </div>
            </div>
        </div>
        <!-- End page header -->

        <!-- property area -->
        <div class="content-area submit-property" style="background-color: #FCFCFC;">&nbsp;
            <div class="container">
                <div class="clearfix" > 
                    <div class="wizard-container"> 

                        <div class="wizard-card ct-wizard-orange" id="wizardProperty">
                                          
                                <div class="wizard-header">
                                <a href="/admin/property" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                                    <h3>
                                        <b>Update</b> YOUR PROPERTY <br>
                                        <small></small>
                                    </h3>
                                </div>
                                <form action="{{url('/admin/properties/update/'.$property->id) }}" method="POST" enctype="multipart/form-data">         
                            @csrf 
                                <ul>
                                    <li><a href="#step1" data-toggle="tab">Step 1 </a></li>
                                    <li><a href="#step2" data-toggle="tab">Step 2 </a></li>
                                    <li><a href="#step3" data-toggle="tab">Step 3 </a></li>
                                    <li><a href="#step4" data-toggle="tab">Finished </a></li>
                                </ul>

                                <div class="tab-content">

                                    <div class="tab-pane" id="step1">
                                        <div class="row p-b-15  ">
                                            <h4 class="info-text"> Let's start with the basic information</h4>
                                            <div class="col-sm-4 col-sm-offset-1">
                                                <div class="picture-container">
                                                    <div class="picture">
                                                        <img src="{{asset($property->img)}}" value="{{asset($property->img)}}"class="picture-src" id="wizardPicturePreview" title=""/>
                                                        <input type="file" name="img"id="wizard-picture" >
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Property name <small>(required)</small></label>
                                                    <input name="title" type="text" class="form-control"  value="{{$property->title}}" required>
                                                </div>

                                                <div class="form-group">
                                                    <label>Property price <small>(required)</small></label>
                                                    <input name="price" type="text" class="form-control" value="{{$property->price}}"  required>
                                                </div> 
                                                <div class="form-group">
                                                    <label>Type <small></small></label>
                                                    <select name="type" id="" class="form-control" required>
                                                    <option >Apartment</option>
                                                    <option >Flat</option>
                                                    <option >safe content</option>
                                                    <option >house</option>
                                                    </select>
                                                </div>
                                               
                                                <div class="form-group">
                                                    
                                                    <input name="user_id" type="hidden" class="form-control" value="{{$property->user_id}}">
                                                </div>
                                                <div class="form-group">
                                                    
                                                    <input name="mode" type="hidden" value="{{$property->mode}}"class="form-control" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--  End step 1 -->

                                    <div class="tab-pane" id="step2">
                                        <h4 class="info-text"> More about your Property </h4>
                                        <div class="row">
                                            <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>Property Description :</label>
                                                        <textarea name="desc" class="form-control"  required>{{$property->desc}}</textarea>
                                                    </div> 
                                                </div> 
                                            </div>

                                            <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>Property Address :</label>
                                                        <textarea name="addr" class="form-control"  required>{{$property->addr}}</textarea>
                                                    </div> 
                                                </div> 
                                            </div>

                                            <div class="col-sm-12">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label>Property State :</label>
                                                        <select id="lunchBegins" name="state"class="selectpicker" data-live-search="true" data-live-search-style="begins" title="Select your city" required>
                                                            <option>Adammawa</option>
                                                          
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label>Property City :</label>
                                                        <select id="lunchBegins" class="selectpicker" name="city" data-live-search="true" data-live-search-style="begins" title="Select your city" required>
                                                            <option>Yola North</option>
                                                            <option>Yola South</option>
                                                            
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label>Property Status  :</label>
                                                        <select id="basic" class="selectpicker show-tick form-control" name="status" required>
                                                            <option> -Status- </option>
                                                            <option>Occupied</option>
                                                            <option>Vacant</option>
                
                                                        </select>
                                                    </div>
                                                </div>
                                              
                                            </div>
                                            <div class="col-sm-12 padding-top-15">                                                   
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="property-geo">No of Bedrooms :</label>
                                                        <input type="number" class="form-control" name="no_bedroom" value="{{$property->no_bedroom}}" required><br />
                                                      
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">

                                                    <div class="form-group">
                                                        <label for="price-range">No of bathrooms :</label>
                                                        <input type="number" class="form-control" name="no_bathroom" value="{{$property->no_bathroom}}" required><br />
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">

<div class="form-group">
    <label for="price-range">No of Parking Space :</label>
    <input type="number" class="form-control" name="no_parkingsp"value="{{$property->no_parkingsp}}" required><br />
    
</div>
</div>
                                                <div class="col-sm-4">

                                                    <div class="form-group">
                                                        <label for="property-geo">Property geo (m2) area :</label>
                                                        <input type="text" class="form-control" name="area" value="{{$property->area}}">
                                                           
                                                    </div>
                                                </div>   
                                            </div>
                                            <div class="col-sm-12 padding-top-15">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="is_furnished" >Is Furnished?
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div> 
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="is_barchelorsa" >Is barchelors allowed?
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>                                                 
                                         
                                            </div> 
                                            </div></div>
                                    <!-- End step 2 -->

                                    <div class="tab-pane" id="step3">                                        
                                        <h4 class="info-text">Give us somme images and videos ? </h4>
                                        <div class="row">  
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="property-images">Chose Images :</label>
                                                    <input class="form-control" type="file" id="property-images" name="img1">
                                                    <p class="help-block">Select  images for your property .</p>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="property-images">Chose Images :</label>
                                                    <input class="form-control" type="file" id="property-images" name="img2">
                                                    <p class="help-block">Select  images for your property .</p>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="property-images">Chose Images :</label>
                                                    <input class="form-control" type="file" id="property-images" name="img3">
                                                    <p class="help-block">Select  images for your property .</p>
                                                </div>
                                            </div>
                                            <div class="col-sm-6"> 
                                                <div class="form-group">
                                                    <label for="property-video">Property video :</label>
                                                    <input class="form-control"  value="{{$property->video_url}}"  name="video_url" type="text">
                                                </div> 

                                            </div>
                                        </div>
                                    </div>
                                    <!--  End step 3 -->


                                    <div class="tab-pane" id="step4">                                        
                                        <h4 class="info-text"> Finished and submit </h4>
                                        <div class="row">  
                                            <div class="col-sm-12">
                                                <div class="">
                                                    <p>
                                                        <label><strong>Terms and Conditions</strong></label>
                                                        By accessing or using  Am and Rent Enterprise services, such as 
                                                        posting your property advertisement with your personal 
                                                        information on our website you agree to the
                                                        collection, use and disclosure of your personal information 
                                                        in the legal proper manner
                                                    </p>

                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" required> <strong>Accept termes and conditions.</strong>
                                                        </label>
                                                    </div> 

                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <!--  End step 4 -->

                                </div>

                                <div class="wizard-footer">
                                    <div class="pull-right">
                                        <input type='button' class='btn btn-next btn-primary' name='next' value='Next' />
                                        <input type='submit' class='btn btn-finish btn-primary ' name='finish' value='Finish' />
                                    </div>

                                    <div class="pull-left">
                                        <input type='button' class='btn btn-previous btn-default' name='previous' value='Previous' />
                                    </div>
                                    <div class="clearfix"></div>                                            
                                </div>	
                            </form>
                        </div>
                        <!-- End submit form -->
                    </div> 
                </div>
            </div>
        </div>

    
@endsection

