<div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
    {!! Form::label('name', 'Username: ', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>




<div class="form-group{{ $errors->has('first_name') ? ' has-error' : ''}}">
    {!! Form::label('first_name', 'First Name: ', ['class' => 'control-label']) !!}
    {!! Form::text('first_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('middle_name') ? ' has-error' : ''}}">
    {!! Form::label('middle_name', 'Middle Name: ', ['class' => 'control-label']) !!}
    {!! Form::text('middle_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('middle_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('last_name') ? ' has-error' : ''}}">
    {!! Form::label('last_name', 'Last Name: ', ['class' => 'control-label']) !!}
    {!! Form::text('last_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('gender') ? ' has-error' : ''}}">
    {!! Form::label('gender', 'Gender: ', ['class' => 'control-label']) !!}
    {!! Form::select('gender', [1=>'Male',2=>'Female'], null, ['class' => 'form-control', 'multiple' => false]) !!}
    {!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group{{ $errors->has('acc_type') ? ' has-error' : ''}}">
    {!! Form::hidden('acc_type','admin') !!}
    {!! $errors->first('acc_type', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('dob') ? ' has-error' : ''}}">
    {!! Form::label('dob', 'Date of Birth: ', ['class' => 'control-label']) !!}
    {!! Form::date('dob', null, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('dob', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('addr') ? ' has-error' : ''}}">
    {!! Form::label('addr', 'Address: ', ['class' => 'control-label']) !!}
    {!! Form::text('addr', null, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('addr', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('country') ? ' has-error' : ''}}">
    {!! Form::label('country', 'Country: ', ['class' => 'control-label']) !!}
    {!! Form::select('country', ['Nigeria'=>'Nigeria'], null, ['class' => 'form-control', 'multiple' => false]) !!}
    {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('state') ? ' has-error' : ''}}">
    {!! Form::label('state', 'State: ', ['class' => 'control-label']) !!}
    {!! Form::select('state', ['Adamawa'=>'Adamawa'], null, ['class' => 'form-control', 'multiple' => false]) !!}
    {!! $errors->first('state', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group{{ $errors->has('locality') ? ' has-error' : ''}}">
    {!! Form::label('locality', 'Locality: ', ['class' => 'control-label']) !!}
    {!! Form::select('locality', ['Fufure'=>'Fufure','Gayuk'=>'Gayuk','Gombi'=>'Gombi','Gerei'=>'Gerei','Hong'=>'Hong','Jada'=>'Jada','Larmurde'=>'Larmurde',
   'Madagali'=>'Madagali','Maiha'=>'Maiha','Mayo Belwa'=>'Mayo Belwa','Michika'=>'Michika','Mubi North'=>'Mubi North','Mubi South'=>'Mubi South','Numan'=>'Numan',
    'Shelleng'=>'Shelleng','Song'=>'Song','Toungo'=>'Toungo', 'Yola North'=>'Yola North','Yola South'=>'Yola South'], null, ['class' => 'form-control', 'multiple' => false]) !!}
    {!! $errors->first('locality', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('phone') ? ' has-error' : ''}}">
    {!! Form::label('phone', 'Phone: ', ['class' => 'control-label']) !!}
    {!! Form::text('phone', null, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('img') ? ' has-error' : ''}}">
    {!! Form::label('img', 'Image: ', ['class' => 'control-label']) !!}
    {!! Form::file('img',['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('img', '<p class="help-block">:message</p>') !!}
</div>



<div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
    {!! Form::label('email', 'Email: ', ['class' => 'control-label']) !!}
    {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('password') ? ' has-error' : ''}}">
    {!! Form::label('password', 'Password: ', ['class' => 'control-label']) !!}
    @php
        $passwordOptions = ['class' => 'form-control'];
        if ($formMode === 'create') {
            $passwordOptions = array_merge($passwordOptions, ['required' => 'required']);
        }
    @endphp
    {!! Form::password('password', $passwordOptions) !!}
    {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('roles') ? ' has-error' : ''}}">
    {!! Form::label('role', 'Role: ', ['class' => 'control-label']) !!}
    {!! Form::select('roles[]', $roles, isset($user_roles) ? $user_roles : [], ['class' => 'form-control', 'multiple' => true]) !!}
</div>
<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
