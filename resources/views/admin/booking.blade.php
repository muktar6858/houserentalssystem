
@include('layouts.dtableheader')
    





  @extends('tenant.layout.header')
@include('layouts.header')


@section('content')


@include('tenant.layout.navbar')
</div>
@include('layouts.admsidebar')



        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Welcome  Back {{Auth::user()->name}}</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
              
                <div class="page-head"> 
            <div class="container">
                <div class="row">
                    <div class="page-head-content">
                       
                        <h1 class="page-title"><center>Bookings </center> </h1>               
                    </div>
                </div>
            </div>
        </div>







  
    
        </head>
        
        <body>
    
        <!-- loading animation -->
      
    
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default gradient">
                                    <div class="panel-heading">
                                        <h4>
                                     
                                        </h4>
                                    </div>
                                    <div class="panel-body noPad clearfix">
                                        <table cellpadding="0" cellspacing="0" border="0" class="tableTools display table table-bordered" width="100%">
                                            <thead>
                                                <tr>
                                                <th>Tenant name</th>
                                                    <th>property title</th>
                                                    
                                                    <th>visit date</th>
                                                    <th>time</th>
                                                    <th>message</th>
                                                    <th>status</th>
                                                    <th>Action</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($booking as $bookings)
                                               <tr><td>{{$bookings->user_id}}</td>
                                               <td>{{$bookings->property_id}}</td>
                                               <th>{{$bookings->visit_date}}</th>
                                               <td>{{$bookings->visit_time}}</td>
                                               <td>{{$bookings->message}}</td>
                                               <td>{{$bookings->status}}</td>
                                           
                                               <td> <a class="btn btn-default" href='/admin/showproperties/{{$bookings->property_id}}'>View Property</a> 
                                               <a class="btn btn-default" href="/admin/bookins/manage/{{$bookings->id}}?mode=Visited"">Mark as Visited</a>
                                               </td>
                                            </tr>
                                            @endforeach
                                            </tbody>
                                            
                                        </table>
                                    </div>
    
                                </div><!-- End .panel -->
    
                            </div><!-- End .span12 -->
    
                        </div><!-- End .row -->
                   
                    <!-- Page end here -->
                        
                </div><!-- End contentwrapper -->
            </div><!-- End #content -->
        
        </div><!-- End #wrapper -->
    
    
  
    
     @include('layouts.dtablescript')
       
        </body>
    </html>
  
  
  







            </div><!--/row ends-->

            </div>
            <!-- /.container-fluid -->
            </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <script src="vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="vendor/metisMenu/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
@endsection

