
@include('layouts.dtableheader')

@include('layouts.header')



                




 @extends('tenant.layout.header')



@section('content')


@include('tenant.layout.navbar')
</div>
@include('layouts.admsidebar')



        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Welcome  Back {{Auth::user()->name}}</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
              
                <div class="page-head"> 
            <div class="container">
                <div class="row">
                    <div class="page-head-content">
                       
                        <h1 class="page-title"><center>All services </center> </h1>               
                    </div>
                </div>
            </div>
        </div>



        <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default gradient">
                                <div class="panel-heading">
                                    <h4>
                                    </h4>
                                </div>
                                <div class="panel-body noPad clearfix">
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Add New Service</button>









<!-- Trigger the modal with a button -->

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">New Service</h4>
</div>
<div class="modal-body">
<form action="{{url('admin/services')}}" method="POST">
@csrf 

<label for="service">Service name</label>
<input type="text"class="form-control " name="service_name" >
<label for="service">Service description</label>

<textarea  class="form-control "name="dsc"></textarea>
<label for="service">Service price</label>

<input type="text"class="form-control " name="service_price" >
<label for="service">Service period</label>

<input type="text" class="form-control "name="service_period" >
<label for="service">no of houses allowed</label>

<input type="number" class="form-control "name="no_houses" >

<input type="submit"  value="submit">


</div>
<div class="modal-footer">

</form>
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>

</div>
</div>



















                                    <table cellpadding="0" cellspacing="0" border="0" class="tableTools display table table-bordered" width="100%">
                                        <thead>
                                        
                                            <tr>
                                                <th>Service Name</th>
                                                <th>Price</th>
                                                <th>Period</th>
                                                <th>Action</th>
                                            </tr>
                                            
                                        </thead>
                                        <tbody>
                                  
                                    @foreach($services as $serv)
                                    
                                    <tr>
                                     
                                           <td>{{$serv->service_name}}</td>
                                           <td>{{$serv->service_price}}</td>
                                           <td>{{$serv->service_period}}</td>
                                          
                                           <td>  <button type="button" class="btn btn-default" data-toggle="modal" data-target="#updatemodal">Update Service</button>

                                         
                                            </td>
                                         
                                           </tr>
                                    

                                       @endforeach






<!-- Trigger the modal with a button -->

<!-- Modal -->
<div id="updatemodal" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">New Service</h4>
</div>
<div class="modal-body">
<form action="{{url('admin/services')}}" method="POST">
@csrf 

<label for="service">Service name</label>
<input type="text"class="form-control " name="service_name" >
<label for="service">Service description</label>

<textarea  class="form-control "name="dsc"></textarea>
<label for="service">Service price</label>

<input type="text"class="form-control " name="service_price" >
<label for="service">Service period</label>

<input type="text" class="form-control "name="service_period" >
<label for="service">no of houses allowed</label>

<input type="number" class="form-control "name="no_houses" >

<input type="submit"  value="submit">


</div>
<div class="modal-footer">

</form>
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>

</div>
</div>







                                        </tbody>
                                        
                                    </table>
                                </div>

                            </div><!-- End .panel -->

                        </div><!-- End .span12 -->

                    </div><!-- End .row -->
               
    			<!-- Page end here -->
    				
            </div><!-- End contentwrapper -->
        </div><!-- End #content -->
    
    </div><!-- End #wrapper -->





 @include('layouts.dtablescript')










            </div><!--/row ends-->

            </div>
            <!-- /.container-fluid -->
            </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <script src="vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="vendor/metisMenu/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
@endsection

