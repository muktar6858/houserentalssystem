<div class="form-group{{ $errors->has('service_name') ? 'has-error' : ''}}">
    {!! Form::label('service_name', 'Service Name', ['class' => 'control-label']) !!}
    {!! Form::text('service_name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('service_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('service_price') ? 'has-error' : ''}}">
    {!! Form::label('service_price', 'Service Price', ['class' => 'control-label']) !!}
    {!! Form::text('service_price', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('service_price', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('service_period') ? 'has-error' : ''}}">
    {!! Form::label('service_period', 'Service Period', ['class' => 'control-label']) !!}
    {!! Form::text('service_period', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('service_period', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
