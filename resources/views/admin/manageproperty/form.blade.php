<div class="form-group{{ $errors->has('user_id') ? 'has-error' : ''}}">
    {!! Form::label('user_id', 'user_id', ['class' => 'control-label']) !!}
    {!! Form::number('user_id', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('title') ? 'has-error' : ''}}">
    {!! Form::label('title', 'Title', ['class' => 'control-label']) !!}
    {!! Form::text('title', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('addr') ? 'has-error' : ''}}">
    {!! Form::label('addr', 'Addr', ['class' => 'control-label']) !!}
    {!! Form::text('addr', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('addr', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status', 'Status', ['class' => 'control-label']) !!}
    {!! Form::Select('status',['Available'=>'Available','Reserved'=>'Reserved'], null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('type') ? 'has-error' : ''}}">
    {!! Form::label('type', 'Type', ['class' => 'control-label']) !!}
    {!! Form::select('type',['Flat'=>'Flat','Apartment'=>'Apartment','SafeContent'=>'Safe Content'], null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('no_bedroom') ? 'has-error' : ''}}">
    {!! Form::label('no_bedroom', 'No Bedroom', ['class' => 'control-label']) !!}
    {!! Form::number('no_bedroom', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('no_bedroom', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('no_bathroom') ? 'has-error' : ''}}">
    {!! Form::label('no_bathroom', 'No Bathroom', ['class' => 'control-label']) !!}
    {!! Form::number('no_bathroom', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('no_bathroom', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('no_parkingsp') ? 'has-error' : ''}}">
    {!! Form::label('no_parkingsp', 'No Parkingsp', ['class' => 'control-label']) !!}
    {!! Form::number('no_parkingsp', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('no_parkingsp', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('price') ? 'has-error' : ''}}">
    {!! Form::label('price', 'Price', ['class' => 'control-label']) !!}
    {!! Form::number('price', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('desc') ? 'has-error' : ''}}">
    {!! Form::label('desc', 'Desc', ['class' => 'control-label']) !!}
    {!! Form::text('desc', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('area') ? 'has-error' : ''}}">
    {!! Form::label('area', 'Area', ['class' => 'control-label']) !!}
    {!! Form::text('area', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('area', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('is_furnished') ? 'has-error' : ''}}">
    {!! Form::label('is_furnished', 'Is Furnished', ['class' => 'control-label']) !!}
    {!! Form::select('is_furnished',['Yes'=>'Yes','No'=>'No'], null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('is_furnished', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group{{ $errors->has('is_barchelorsa') ? 'has-error' : ''}}">
    {!! Form::label('is_barchelorsa', 'Is Barchelorsa', ['class' => 'control-label']) !!}
    {!! Form::select('is_barchelorsa',['Yes'=>'Yes','No'=>'No'], null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('is_barchelorsa', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group{{ $errors->has('img') ? 'has-error' : ''}}">
    {!! Form::label('img', 'Img', ['class' => 'control-label']) !!}
    {!! Form::file('img', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('img', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('img1') ? 'has-error' : ''}}">
    {!! Form::label('img1', 'Img1', ['class' => 'control-label']) !!}
    {!! Form::file('img1', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('img1', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('img2') ? 'has-error' : ''}}">
    {!! Form::label('img2', 'Img2', ['class' => 'control-label']) !!}
    {!! Form::file('img2', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('img2', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('img3') ? 'has-error' : ''}}">
    {!! Form::label('img3', 'Img3', ['class' => 'control-label']) !!}
    {!! Form::file('img3', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('img3', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('video_url') ? 'has-error' : ''}}">
    {!! Form::label('video_url', 'Video Url', ['class' => 'control-label']) !!}
    {!! Form::text('video_url', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('video_url', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
