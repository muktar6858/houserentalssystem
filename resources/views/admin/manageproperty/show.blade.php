@extends('layouts.backend')

@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Property</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/managepropert') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/managepropert/' . $property->id . '/edit') }}" title="Edit User"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method' => 'DELETE',
                            'url' => ['/admin/managepropert', $property->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete Property',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table" >
                                <thead>
                                    <tr>
                                        <th>title</th><th>Address</th><th>status</th><th>state</th><th>description</th><th>price</th><th>bathroom</th><th>bedroom</th><th>mode</th><th>is_furnished</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{ $property->title }}</td> <td> {{ $property->addr}} </td><td> {{ $property->status }} </td><td> {{ $property->state }} </td>
                                        <td> {{ $property->desc }} </td><td> {{ $property->price }} </td><td> {{ $property->no_bathroom }} </td><td> {{ $property->no_bedroom }} </td><td> {{ $property->mode }} </td><td> {{ $property->is_furnished }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
