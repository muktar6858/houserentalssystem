@extends('layouts.header')

<link rel="stylesheet" href="{{asset('assets/style2.css')}}">
@section('content')

@include('layouts.navbar2')
<body class="page-sub-page page-invoice" id="page-top">
<!-- Wrapper -->
<div class="wrapper">
   
    <!-- Page Content -->
    <div id="page-content">
        <!-- Breadcrumb -->
      
        <!-- end Breadcrumb -->

        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="invoice">
                        <section>
                            <table>
                                <tr>

                                    <td><h1>User Profile</h1></td>
                                   
                                </tr>
                            </table>
                        </section>
                        <section>
                            <aside>
                                <h2>Basic Information</h2>
                               
                           <div class="contact-info">
                                    <dl>
                                        <dt>Username:</dt>
                                        <dd>{{$user->name}}</dd>
                                        <dt>First Name:</dt>
                                        <dd>{{$user->first_name}}</dd>
                                        <dt>Midlle Name:</dt>
                                        <dd>{{$user->middle_name}}</dd>
                                        <dt>Last Name:</dt>
                                        <dd>{{$user->last_name}}</dd>
                                        <dt>Gender:</dt>
                                        <dd>{{$user->gender}}</dd>
                                        <dt>Date of Birth:</dt>
                                        <dd>{{$user->dob}}</dd>
                                       
                                        <dt>Phone:</dt>
                                        <dd>{{$user->phone}}</dd>
                                        <dt>Email:</dt>
                                        <dd>{{$user->email}}</dd>
                                        <dt>Account Type:</dt>
                                        <dd>{{$user->acc_type}}</dd>
                                        <dt>Date Joined:</dt>
                                        <dd>{{$user->created_at}}</dd>
                                        <br>  <br> 
                                    </dl>
                                </div>
                            </aside>
                            <aside>
                               
                                <address>
                                    <div class="title"><img src="{{$user->img}}" alt="pasport"></div>
                                    <div class="address">
                                 <br>
                                    <dl>
                                    <dt>Address:</dt>
                                        <dd>{{$user->addr}}</dd>
                                        
                                        <dt>Country:</dt>
                                        <dd>{{$user->country}}</dd>
                                        <dt>State:</dt>
                                        <dd>{{$user->state}}</dd>
                                        <dt>Locality:</dt>
                                        <dd>{{$user->locality}}</dd>
                                        </dl>
                                        </div>
                                </address>
                                <div class="contact-info">
                                    <dl>
                                  
                                      
                                    <td ><a href="{{url('admin/edituser/'.$user->id)}}" class="btn btn-primary">Edit Profile</a>   </td>
                                    </dl>
                                </div>
                            </aside>
                        </section>
                     
                    </div>
                </div>
              
            </div><!-- /.row -->
          
        </div><!-- /.container -->
    </div>


@endsection
