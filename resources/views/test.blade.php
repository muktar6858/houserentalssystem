
        @extends('layouts.header')
       @section('content')
       
     
        <!-- Body content -->


       @include('layouts.navbar2')
        <!-- End of nav bar -->

        <div class="page-head"> 
            <div class="container">
                <div class="row">
                    <div class="page-head-content">
                        <h1 class="page-title"><center> Contact page</center></h1>               
                    </div>
                </div>
            </div>
        </div>
        <!-- End page header -->

        <!-- property area -->
        <div class="content-area recent-property padding-top-40" style="background-color: #FFF;">
            <div class="container">  
                <div class="row">
                    <div class="col-md-8 col-md-offset-2"> 
                        <div class="" id="contact1">                        
                            
                            <!-- /.row -->
                         
        
                            <h2>Contact form</h2>
                            <form action="{{url('/contact_us')}}" method="POST">
                                <div class="row">
                                  

                                    @if(Auth::check())

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                       
                                            <input type="hidden" name="email" class="form-control" id="email">
                                            <input type="hidden" name="user_id" value="{{Auth::user()->id}}" class="form-control" id="email">
                                            <input type="hidden" name="name" value="{{Auth::user()->name}}" class="form-control" id="email">

                                        </div>
                                    </div>



                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="subject">Subject</label>
                                            <input type="text" name="subject" class="form-control" id="subject">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="message">Message</label>
                                            <textarea id="message" name="message" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 text-center">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send message</button>
                                    </div>
                                </div>


                                    @else
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="firstname">Full Name</label>
                                            <input type="text" name="name"class="form-control" id="firstname">
                                        </div>
                                    </div>
                            
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="text" name="email" class="form-control" id="email">
                                        </div>
                                    </div>



                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="subject">Subject</label>
                                            <input type="text" name="subject" class="form-control" id="subject">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="message">Message</label>
                                            <textarea id="message" name="message"class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 text-center">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send message</button>
                                    </div>
                                </div>



                                    @endif
                                   
                                <!-- /.row -->
                            </form>
                        </div>

                        </div>    
                </div>
                        <div class="row">
                                <div class="col-sm-4">
                                    <h3><i class="fa fa-map-marker"></i> Address</h3>
                                    <p>No 164,
                                        <br>Mohammed Mustapha Way
                                        <br>Jimeta, Yola, 
                                        <br>
                                        <strong>Adamawa, Nigeria </strong>
                                    </p>
                                </div>
                                <!-- /.col-sm-4 -->
                                <div class="col-sm-4">
                                    <h3><i class="fa fa-phone"></i> Call center</h3>
                                    <p class="text-muted">This number is toll free if calling from
                                        withing adamawa state.</p>
                                    <p><strong>+234 8038781709</strong></p>
                                </div>
                                <!-- /.col-sm-4 -->
                                <div class="col-sm-4">
                                    <h3><i class="fa fa-envelope"></i> Electronic support</h3>
                                    <p class="text-muted">Please feel free to write an email to us or to use our electronic ticketing system.</p>
                                    <ul>
                                        <li><strong><a href="mailto:"> support@amandrent.com</a></strong>   </li>
                                        <li><strong><a href="#">Ticketio</a></strong> - our ticketing support platform</li>
                                    </ul>
                                </div>
                                <!-- /.col-sm-4 -->
                            </div>
                    </div>    
                </div>
            </div>
        </div>

  <!-- Footer area-->
      @include('layouts.footer')
      @endsection