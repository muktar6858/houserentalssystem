@extends('layouts.header')

@section('content')
        <!-- Body content -->

        @include('layouts.navbar2')

        <div id="preloader">
            <div id="status">&nbsp;</div>
        </div>
        <!-- Body content -->
    
        <!--End top header -->

       
        <!-- End of nav bar -->

        <div class="page-head"> 
            <div class="container">
                <div class="row">
                    <div class="page-head-content">
                        <h1 class="page-title"><center> Properties </center> </h1>               
                    </div>
                </div>
            </div>
        </div>
        <!-- End page header -->

        <!-- property area -->
        <div class="properties-area recent-property" style="background-color: #FFF;">
            <div class="container">  
                <div class="row">
                     
                <div class="col-md-3 p0 padding-top-40">
                    <div class="blog-asside-right pr0">
                        <div class="panel panel-default sidebar-menu wow fadeInRight animated" >
                            <div class="panel-heading">
                                <h3 class="panel-title">Smart search</h3>
                            </div>
                            <div class="panel-body search-widget">
                                <form action="{{url('/homeproperties')}}" class=" form-inline" method="POST"> 
                                    @csrf
                                    <fieldset>
                                        <div class="row">
                                             <div class="col-xs-12">
                                                <input type="text" name="title"class="form-control" placeholder="Key word">
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <div class="row">
                                             <div class="col-xs-12">
                                             <label for="city">Select Location</label>
                                                <select name="city" class="form-control">
                                                <option value="Yola North">Yola North</option>
                                                <option value="Yola North">Yola South</option>
                                                </select>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset class="padding-5">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="checkbox">
                                                    <label> <input type="checkbox" name="bachelor" > For bachelor </label>
                                                </div> 
                                            </div>
                                      
                                        </div>
                                    </fieldset>

                                   


                                  

                                    <fieldset >
                                        <div class="row">
                                            <div class="col-xs-12">  
                                                <input class="button btn largesearch-btn" value="Search" type="submit">
                                            </div>  
                                        </div>
                                    </fieldset>                                     
                                </form>
                            </div>
                        </div>

                        
                            
                    </div>
                </div>

                <div class="col-md-9  pr0 padding-top-40 properties-page">
                    <div class="col-md-12 clear"> 
                        <div class="col-xs-10 page-subheader sorting pl0">
                            <ul class="sort-by-list">
                                <li class="active">
                                    <a href="javascript:void(0);" class="order_by_date" data-orderby="property_date" data-order="ASC">
                                        Property Date <i class="fa fa-sort-amount-asc"></i>					
                                    </a>
                                </li>
                                <li class="">
                                    <a href="javascript:void(0);" class="order_by_price" data-orderby="property_price" data-order="DESC">
                                        Property Price <i class="fa fa-sort-numeric-desc"></i>						
                                    </a>
                                </li>
                            </ul><!--/ .sort-by-list-->

                            <div class="items-per-page">
                                <label for="items_per_page"><b>Property per page :</b></label>
                                <div class="sel">
                                    <select id="items_per_page" name="per_page">
                                        <option value="3">3</option>
                                        <option value="6">6</option>
                                        <option value="9">9</option>
                                        <option selected="selected" value="12">12</option>
                                        <option value="15">15</option>
                                        <option value="30">30</option>
                                        <option value="45">45</option>
                                        <option value="60">60</option>
                                    </select>
                                </div><!--/ .sel-->
                            </div><!--/ .items-per-page-->
                        </div>

                        <div class="col-xs-2 layout-switcher">
                            <a class="layout-list" href="javascript:void(0);"> <i class="fa fa-th-list"></i>  </a>
                            <a class="layout-grid active" href="javascript:void(0);"> <i class="fa fa-th"></i> </a>                          
                        </div><!--/ .layout-switcher-->
                    </div>
                    <div class="section clear"> 
                    <div id="list-type" class="proerty-th">
                    @foreach($property as $properties)
                    
                            <div class="col-sm-6 col-md-4 p0">
                                    <div class="box-two proerty-item">
                                        <div class="item-thumb">
                                            <a href="{{ url('showhomeproperty/' . $properties->id) }}" ><img src='{{asset("$properties->img")}}'></a>
                                        </div>

                                        <div class="item-entry overflow">
                                            <h5><a href="{{ url('showhomeproperty/' . $properties->id) }}"> {{$properties->title}} </a></h5>
                                            <div class="dot-hr"></div>
                                            <span class="pull-left"><b> Area :</b> {{$properties->area}}m </span>
                                            <span class="proerty-price pull-right"><span>&#8358;</span>{{number_format($properties->price,2)}}</span>
                                            <p style="display: none;"> {{$properties->desc}}</p>
                                            <div class="property-icon">
                                                <img src="assets/img/icon/bed.png">{{$properties->no_bedroom}}|
                                                <img src="assets/img/icon/shawer.png">{{$properties->no_bathroom}}|
                                                <img src="assets/img/icon/cars.png">{{$properties->no_parkingsp}}  
                                            </div>
                                        </div>


                                    </div>
                                </div> 
                            
                        @endforeach
                       
                   
                    
                    <div class="col-md-12"> 
                        <div class="pull-right">
                            <div class="pagination">
                                <ul>
                                    <li><a href="#">Prev</a></li>
                                    <li><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">Next</a></li>
                                </ul>
                            </div>
                        </div>                
                    </div>
                </div>  
                </div>              
            </div>
        </div>

          <!-- Footer area-->
      
          </div>  </div> 
     @include('layouts.footer')

@include('layouts.script')
@endsection