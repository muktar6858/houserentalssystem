
@extends('layouts.header')

@section('content')
        <!-- Body content -->

        @include('layouts.navbar2')
     
        <!-- Body content -->

     
        <div class="page-head"> 
            <div class="container">
                <div class="row">
                    <div class="page-head-content">
                        <h1 class="page-title"><center>Property Page</center></h1>               
                    </div>
                </div>
            </div>
        </div>
        <!-- End page header -->

        <!-- property area -->
        <div class="content-area single-property" style="background-color: #FCFCFC;">&nbsp;
            <div class="container">   

                <div class="clearfix padding-top-40" >

                    <div class="col-md-8 single-property-content prp-style-2">
                        <div class="">
                            <div class="row">
                                <div class="light-slide-item">            
                                    <div class="clearfix">
                                        <div class="favorite-and-print">
                                            <a class="add-to-fav" href="#login-modal" data-toggle="modal">
                                                <i class="fa fa-star-o"></i>
                                            </a>
                                            <a class="printer-icon " href="javascript:window.print()">
                                                <i class="fa fa-print"></i> 
                                            </a>
                                        </div> 

                                        <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
                                        <li data-thumb="{{asset($property->img)}}"> 
                                                <img src="{{asset($property->img)}}" />
                                            </li>
                                            <li data-thumb="{{asset($property->img1)}}"> 
                                                <img src="{{asset($property->img1)}}" />
                                            </li>
                                            <li data-thumb="{{asset($property->img2)}}"> 
                                                <img src="{{asset($property->img2)}}" />
                                            </li>                                        
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="single-property-wrapper">

                                <div class="section">
                                    <h4 class="s-property-title">Description</h4>
                                    <div class="s-property-content">
                                        <p>{{$property->desc}}</p>
                                    </div>
                                </div>
                                <!-- End description area  -->

                                <div class="section additional-details">

                                    <h4 class="s-property-title">Additional Details</h4>

                                    <ul class="additional-details-list clearfix">
                                    <li>
                                        <span class="col-xs-6 col-sm-4 col-md-4 add-d-title">Address</span>
                                        <span class="col-xs-6 col-sm-8 col-md-8 add-d-entry">{{$property->addr}}</span>
                                    </li>

                                    <li>
                                        <span class="col-xs-6 col-sm-4 col-md-4 add-d-title">State</span>
                                        <span class="col-xs-6 col-sm-8 col-md-8 add-d-entry">{{$property->state}}</span>
                                    </li>
                                    <li>
                                        <span class="col-xs-6 col-sm-4 col-md-4 add-d-title">City</span>
                                        <span class="col-xs-6 col-sm-8 col-md-8 add-d-entry">{{$property->city}}</span>
                                    </li>

                                    <li>
                                        <span class="col-xs-6 col-sm-4 col-md-4 add-d-title">Barchelors allwoed</span>
                                        <span class="col-xs-6 col-sm-8 col-md-8 add-d-entry">{{$property->is_barchelorsa}}</span>
                                    </li>

                                    <li>
                                        <span class="col-xs-6 col-sm-4 col-md-4 add-d-title">Furnish</span>
                                        <span class="col-xs-6 col-sm-8 col-md-8 add-d-entry">{{$property->is_furnished}}</span>
                                    </li>
                                    </ul>
                                </div>  
                                <!-- End additional-details area  -->

                                
                                <!-- End features area  -->

                                <div class="section property-video"> 
                                    <h4 class="s-property-title">Property Video</h4> 
                                    <div class="video-thumb">
                                        <a class="video-popup" href="yout" title="Virtual Tour">
                                            <img src="assets/img/property-video.jpg" class="img-responsive wp-post-image" alt="Exterior">            
                                        </a>
                                    </div>
                                </div>
                                <!-- End video area  -->

                                <div class="section property-share"> 
                                    <h4 class="s-property-title">Share width your friends </h4> 
                                    <div class="roperty-social">
                                        <ul> 
                                            <li><a title="Share this on dribbble " href="#"><img src="{{asset('assets/img/social_big/dribbble_grey.png')}}"></a></li>                                         
                                            <li><a title="Share this on facebok " href="#"><img src="{{asset('assets/img/social_big/facebook_grey.png')}}"></a></li> 
                                            <li><a title="Share this on delicious " href="#"><img src="{{asset('assets/img/social_big/delicious_grey.png')}}"></a></li> 
                                            <li><a title="Share this on tumblr " href="#"><img src="{{asset('assets/img/social_big/tumblr_grey.png')}}"></a></li> 
                                            <li><a title="Share this on digg " href="#"><img src="{{asset('assets/img/social_big/digg_grey.png')}}"></a></li> 
                                            <li><a title="Share this on twitter " href="#"><img src="{{asset('assets/img/social_big/twitter_grey.png')}}"></a></li> 
                                            <li><a title="Share this on linkedin " href="#"><img src="{{asset('assets/img/social_big/linkedin_grey.png')}}"></a></li>                                        
                                        </ul>
                                    </div>
                                </div>
                                <!-- End video area  -->
                            </div>
                        </div>
                        @foreach($property2 as $properties)
                        
                        <div class="similar-post-section padding-top-40"> 
                            <div id="prop-smlr-slide_0"> 
                                <div class="box-two proerty-item">
                                    <div class="item-thumb">
                                        <a href="{{ url('showhomeproperty/'. $properties->id) }}" ><img src='{{asset("$properties->img")}}'></a>
                                    </div>
                                    <div class="item-entry overflow">
                                        <h5><a href="{{ url('showhomeproperty/'. $properties->id) }}"> {{$properties->title}} </a></h5>
                                        <div class="dot-hr"></div>
                                        <span class="pull-left"><b> Area :</b> {{$properties->area}} </span>
                                        <span class="proerty-price pull-right"> <span>&#8358;</span>{{number_format($properties->price,2)}}</span>
                                    </div>
                                </div> 
                                
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <div class="col-md-4 p0">
                        <aside class="sidebar sidebar-property blog-asside-right property-style2">
                            <div class="dealer-widget">
                                <div class="dealer-content">
                                    <div class="inner-wrapper">
                                        <div class="single-property-header">                                          
                                            <h1 class="property-title">{{$property->title}}</h1>
                                            <span class="property-price"><span>&#8358;</span>{{number_format($property->price, 2)}}</span>
                                        </div>

                                        <div class="property-meta entry-meta clearfix ">   

                                            <div class="col-xs-4 col-sm-4 col-md-4 p-b-15">
                                                <span class="property-info-icon icon-tag">                                                                                      
                                                    <img src="{{asset('assets/img/icon/rent-orange.png')}}">
                                                </span>
                                                <span class="property-info-entry">
                                                    <span class="property-info-label">Status</span>
                                                    <span class="property-info-value">{{$property->status}}</span>
                                                </span>
                                            </div>

                                            <div class="col-xs-4 col-sm-4 col-md-4 p-b-15">
                                                <span class="property-info icon-area">
                                                    <img src="{{asset('assets/img/icon/room-orange.png')}}">
                                                </span>
                                                <span class="property-info-entry">
                                                    <span class="property-info-label">Area</span>
                                                    <span class="property-info-value">{{$property->area}}<b class="property-info-unit">Sq Ft</b></span>
                                                </span>
                                            </div>

                                            <div class="col-xs-4 col-sm-4 col-md-4 p-b-15">
                                                <span class="property-info-icon icon-bed">
                                                    <img src="{{asset('assets/img/icon/bed-orange.png')}}">
                                                </span>
                                                <span class="property-info-entry">
                                                    <span class="property-info-label">Bedrooms</span>
                                                    <span class="property-info-value">{{$property->no_bedroom}}</span>
                                                </span>
                                            </div>

                                            <div class="col-xs-4 col-sm-4 col-md-4 p-b-15">
                                                <span class="property-info-icon icon-bath">
                                                    <img src="{{asset('assets/img/icon/cars-orange.png')}}">
                                                </span>
                                                <span class="property-info-entry">
                                                    <span class="property-info-label">Bathrooms</span>
                                                    <span class="property-info-value">{{$property->no_bathroom}}</span>
                                                </span>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 col-md-4 p-b-15">
                                                <span class="property-info-icon icon-bath">
                                                    <img src="{{asset('assets/img/icon/shawer-orange.png')}}">
                                                </span>
                                                <span class="property-info-entry">
                                                    <span class="property-info-label">Bathrooms</span>
                                                    <span class="property-info-value">3.5</span>
                                                </span>
                                            </div>

                                            <div class="col-xs-4 col-sm-4 col-md-4 p-b-15">
                                                <span class="property-info-icon icon-garage">
                                                    <img src="{{asset('assets/img/icon/room-orange.png')}}">
                                                </span>
                                                <span class="property-info-entry">
                                                    <span class="property-info-label">Parking Space</span>
                                                    <span class="property-info-value">{{$property->no_parkingsp}}</span>
                                                </span>
                                            </div>

                                            <div class="col-xs-4 col-sm-4 col-md-4 p-b-15">
                                                <span class="property-info-icon icon-garage">
                                                
                                                </span>
                                                <span class="property-info-entry">
                                                    
                                                </span>
                                            </div>


                                        </div>
                                        
                                        </div>

                                        

                                    </div>
                                </div>
                            </div>


                            <div class="panel panel-default sidebar-menu wow fadeInRight animated">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Schedule Visit  </h3>
                                </div>
                                <div class="panel-body recent-property-widget">
                                    <div class="col-md-3">
                                    @if(Auth::check())
                                        <form action="{{url('/submitschedule')}}" method="POST">
                                        @csrf 
                                <input type="hidden" name="status" value="scheduled"/>
                                <input type="hidden" name="property_id" value='{{$property_id}}'/>
                                
                                <input type="hidden"name="user_id"  value="{{Auth::user()->id}}"/><br>
                             
                                <label for="name" >Intended Visit Date</label>
                                <input type="date" name="visit_date"class="panel-body form-control" required><br>

                                <label for="time">Intended Visit Time</label>
                                <input type="time" name="visit_time"class="panel-body form-control" required><br>
                                <label for="message">Enter Message</label>
                                <textarea name="message" class="panel-body form-control" ></textarea>
                                <input type="submit" value="submit" class="btn btn-outline">



                                </form>
                                @else
                                
                                <h3><a href="/register">Register</a> or <a href="/login">Login</a> to Make shedules</h3>

                                @endif
                                </div>
                            </div>
                            </div></div>
                            <div class="panel panel-default sidebar-menu wow fadeInRight animated" >
                                <div class="panel-heading">
                               
                            
                                </div>
                            </div>

                        </aside>
                    </div>

                </div>

            </div>
        </div>

   

        @include('layouts.footer')

@endsection


