<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/test', 'HomeController@test');
Route::post('/payment/add-funds/paypal', 'PaymentController@payWithpaypal');



Route::get('test2',function(){

    return view('test');
});

/////////Hmpage properties listing////////////////////
Route::get('/',  'HomeController@index');
Route::get('/homeproperties', 'HomeController@allpropertieshome');
Route::get('/showhomeproperty/{id}', 'HomeController@showproperty');
Route::get('/faq', 'HomeController@faq');
Route::post('/submitschedule', 'HomeController@submitschedule');
Route::post('/homeproperties', 'HomeController@allpropertieshome');

Route::get('/contactspage', 'HomeController@contactspage');


Route::post('/contact_us', 'HomeController@contactus');


/////////paystack route////////////////////
Route::post('/pay', 'PaymentController@redirectToGateway')->name('pay');
Route::get('/payment/callback', 'PaymentController@handleGatewayCallback'); 

Route::get('/transactions/{id}', 'TransactionController@show')->name('transactions.show');
/////////////tenant route/////////////////////////////
Route::get('/tenant','tenantController@index')->name('tenant');
 
Route::get('/tenant/profile/{id}', 'tenantController@profile');
Route::get('/tenant/edit/{id}', 'tenantController@edit');
Route::post('/tenant/update/{id}', 'Admin\UsersController@updateTenant');

Route::get('tenant/payment','tenantController@payments');
Route::get('tenant/invoices/{id}','tenantController@invoices');
Route::get('tenant/rentapplications','Tenant\TenantAppController@rentapps');
Route::get('tenant/lease','tenantController@lease');
Route::get('tenant/rentappform/{id}','tenantController@rentappform');
Route::get('tenant/rentappdetails/{id}','tenantController@rentappdetails');

Route::get('tenant/rentappeditform/{id}','Tenant\TenantAppController@rentappedit');
Route::post('tenant/appupdate/{id}','Tenant\TenantAppController@rentappupdate');
Route::get('/tenant/payments/report', 'tenantController@paymentreport');


Route::post('/tenant/requestermination', 'tenantControlle@reqterm');
////////////////////properties//////////////////////////////////////////////
Route::get('/allproperty', 'landlord\PropertyController@showproperty');
Route::get('/allproperty/{id}', 'landlord\PropertyController@showSelected');

////////////////////Landlord route/////////////////////////
 
  
Route::get('/landlord', 'landlordController@index')->name('landlord');
//Route::get('/landlord/profile', 'landlordController@profile');

Route::get('/landlord/profile/{id}', 'landlordController@profile');
Route::get('/landlord/edit/{id}', 'landlordController@edit');
Route::post('/landlord/update/{id}', 'Admin\UsersController@updateLandlord');


  
  Route::get('/landlord/allproperties', 'landlordController@allproperty');
  Route::get('/landlord/addroperties', 'landlord\PropertyController@create');
  Route::get('/landlord/propertydetails/{id}', 'landlordController@propertydetails');
  Route::get('/landlord/lease', 'landlordController@lease');
  Route::get('/landlord/myservice', 'landlordController@myservice');
  Route::get('/landlord/services', 'landlordController@services');
  Route::get('/landlord/invoice/{id}', 'landlordController@invoice');
  Route::get('/landlord/payments', 'landlordController@payments');
  Route::get('/landlord/invoices', 'landlordController@invoices');
  Route::get('/landlord/payments/report', 'landlordController@paymentreport');

  Route::post('/landlord/requestermination', 'landlordController@reqterm');

 // Route::get('/landlord/login', 'landlordController@invoices');



//////////////////////end of landlord route///////////////////////////////



///////////////////////end redirect/////////////
/////////////Admin////////////////////////////////////////
// Check role anywhere

    // Do admin stuff here
    Route::get('admin', 'Admin\AdminController@index');
    Route::get('adm', 'Admin\AdminController@adm');
 
    Route::get('admin/users', 'Admin\AdminController@users');
    Route::get('admin/viewuser/{id}', 'Admin\AdminController@showuser');
    Route::get('admin/edituser/{id}', 'Admin\AdminController@edituser');
    Route::post('admin/updateuser/{id}', 'Admin\AdminController@updateuser');
    Route::get('admin/adduser', 'Admin\AdminController@adduser');
    Route::get('admin/profile/{id}', 'Admin\AdminController@profile');
    Route::post('admin/storeuser', 'Admin\AdminController@storeuser');
    Route::get('admin/properties', 'Admin\AdminController@properties');
    Route::get('admin/showproperties/{id}', 'Admin\AdminController@showproperties');
    Route::get('admin/properties/edit/{id}', 'Admin\AdminController@editproperty');
    Route::post('admin/properties/update/{id}', 'Admin\AdminController@updateproperty');
    Route::get('admin/properties/manage/{id}', 'Admin\AdminController@manage');
     
    Route::get('admin/contactus', 'Admin\AdminController@contacts');
    Route::get('admin/leasetermination', 'Admin\AdminController@leasetermination');

    Route::get('admin/payments', 'Admin\AdminController@payments');
    Route::get('admin/lease', 'Admin\AdminController@lease');
    Route::get('admin/rentapplications', 'Admin\AdminController@rentapplications');
    Route::get('admin/showrerntapps/{id}', 'Admin\AdminController@showrentapplication');
    Route::get('admin/apps/manage/{id}', 'Admin\AdminController@manageapps');
    Route::get('admin/booking', 'Admin\AdminController@booking');
    Route::get('admin/bookins/manage/{id}', 'Admin\AdminController@managebooking');
    //Route::get('admin/services', 'Admin\AdminController@services');
    
    

    Route::get('admin/subscription', 'Admin\AdminController@subscription');
  
    Route::resource('admin/manageproperty', 'Admin\ManagePropertyController');
    Route::resource('admin/roles', 'Admin\RolesController');
    Route::resource('admin/permissions', 'Admin\PermissionsController');
   // Route::resource('admin/users', 'Admin\UsersController');
    Route::resource('admin/pages', 'Admin\PagesController');
    Route::resource('admin/activitylogs', 'Admin\ActivityLogsController')->only([
        'index', 'show', 'destroy'
    ]);
    Route::resource('admin/settings', 'Admin\SettingsController');
  Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {
      Route::get('/', ['uses' => 'AdminController@index']);
      
       
       
        
 });
    
///////////////end admin/////////////////////////////////////////


//////////////////
 Route::get('admin/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
 Route::post('admin/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);

 ////////////////////////trainers/////////////////////////////////////////
Route::resource('landlord/property', 'Landlord\\PropertyController');
Route::resource('tenant-app', 'Tenant\\TenantAppController');
Route::resource('admin/services', 'Admin\\servicesController');
//Route::resource('admin/subscription', 'Admin\\subscriptionController');

Route::get('logout', 'Auth\LoginController@logout', function () {
    return abort(404);
});