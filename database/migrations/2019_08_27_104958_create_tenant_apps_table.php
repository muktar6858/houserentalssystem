<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTenantAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_apps', function (Blueprint $table) {
           
            $table->unsignedInteger('user_id')->nullable();
            $table->string('rentapp_id')->unique();
            $table->string('full_name')->nullable();
            $table->string('addr')->nullable();
            $table->string('tel')->nullable();
            $table->string('app_status')->nullable();
            $table->string('sex')->nullable();
            $table->string('dob')->nullable();
            $table->string('id_type')->nullable();
            $table->string('id_no')->nullable();
            $table->string('marital_status')->nullable();
            $table->string('name_spouse')->nullable();
            $table->string('no_children')->nullable();
            $table->string('no_dependant')->nullable();
            $table->string('how_longr')->nullable();
            $table->string('how_manyr')->nullable();
            $table->string('no_cars')->nullable();
            $table->string('type_pet')->nullable();
            $table->string('no_pet')->nullable();
            $table->string('nationality')->nullable();
            $table->string('state_origin')->nullable();
            $table->string('img')->nullable();
            $table->unsignedInteger('property_id')->nullable();
            $table->timestamps();


            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

                $table->primary(['rentapp_id']);
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tenant_apps');
    }
}
