<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrentTenancyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('current_tenancy', function (Blueprint $table) {
          
            $table->string('ctime_atcurrent')->nullable();
            $table->string('creason')->nullable();
            $table->string('cname_agent')->nullable();
            $table->string('cnotice')->nullable();
            $table->string('cprev_addr')->nullable();
            $table->string('ctime_atprevaddr')->nullable();
            $table->string('crent_appid')->nullable();
            $table->string('ctel')->nullable();


            $table->timestamps();


            $table->foreign('crent_appid')
                ->references('rentapp_id')
                ->on('tenant_apps')
                ->onDelete('cascade');

                $table->primary(['crent_appid']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('current_tenancy');
    }
}
