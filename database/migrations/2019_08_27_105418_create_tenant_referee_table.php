<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenantRefereeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_referee', function (Blueprint $table) {
           
            $table->string('rfull_name')->nullable();
            $table->string('rrel')->nullable();
            $table->string('rtel')->nullable();
            $table->string('rrent_appid')->nullable();

            $table->timestamps();


            $table->foreign('rrent_appid')
                ->references('rentapp_id')
                ->on('tenant_apps')
                ->onDelete('cascade');
                $table->primary(['rrent_appid']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenant_referee');
    }
}
