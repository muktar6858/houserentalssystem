<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrevTenantInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prev_tenant_info', function (Blueprint $table) {
          
            $table->string('pname_agent')->nullable();
            $table->string('ptel')->nullable();
            $table->string('psec_deposit')->nullable();
            $table->string('preason')->nullable();
            $table->string('prent_appid')->nullable();

            $table->timestamps();


            $table->foreign('prent_appid')
                ->references('rentapp_id')
                ->on('tenant_apps')
                ->onDelete('cascade');
                $table->primary(['prent_appid']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prev_tenant_info');
    }
}
