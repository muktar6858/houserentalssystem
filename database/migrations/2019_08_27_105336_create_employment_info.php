<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmploymentInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employment_info', function (Blueprint $table) {
          

            $table->string('empaddr')->nullable();
            $table->string('empoccup')->nullable();
            $table->string('emplenth')->nullable();
            $table->string('emptel')->nullable();
            $table->string('empbname')->nullable();
            $table->string('empbaddr')->nullable();
            $table->string('empdate')->nullable();
            $table->string('emppos')->nullable();
            $table->string('empnamepartner')->nullable();
            $table->string('empmeans')->nullable();
            $table->string('empbankername')->nullable();
            $table->string('empbankeraddr')->nullable();
            $table->string('emprent_appid')->nullable();



            $table->timestamps();

            $table->foreign('emprent_appid')
                ->references('rentapp_id')
                ->on('tenant_apps')
                ->onDelete('cascade');
                $table->primary(['emprent_appid']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employment_info');
    }
}
