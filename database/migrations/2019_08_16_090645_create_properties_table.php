<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
      
            $table->unsignedInteger('user_id')->nullable();
            $table->string('status')->nullable();
            $table->string('state')->nullable();
            $table->string('city')->nullable();
            $table->string('mode')->nullable();
            $table->string('type')->nullable();
            $table->string('title')->nullable();
            $table->string('addr')->nullable();
            $table->string('area')->nullable();
            $table->integer('no_bedroom')->nullable();
            $table->string('no_bathroom')->nullable();
            $table->string('no_parkingsp')->nullable();
            $table->string('price')->nullable();
            $table->string('desc')->nullable();
            $table->string('img')->nullable();
            $table->string('img1')->nullable();
            $table->string('img2')->nullable();
            $table->string('img3')->nullable();
            $table->string('video_url')->nullable();
            $table->string('is_furnished')->nullable();
          
            $table->string('is_barchelorsa')->nullable();
            $table->timestamps();
            
            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');
  
        
        
        
        
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('properties');
    }
}
