<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNextOfKin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('next_of_kin', function (Blueprint $table) {
        
            $table->string('nfull_name')->nullable();
            $table->string('nrel')->nullable();
            $table->string('naddr')->nullable();
            $table->string('ntel')->nullable();
            $table->string('nmail')->nullable();
            $table->string('nrent_appid')->nullable();

            $table->timestamps();

            $table->foreign('nrent_appid')
                ->references('rentapp_id')
                ->on('tenant_apps')
                ->onDelete('cascade')
                ->onUpdate('cascade');

                $table->primary(['nrent_appid']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('next_of_kin');
    }
}
