<?php

namespace App\Http\Controllers\Tenant;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\TenantApp;
use App\Refrees;
use App\Employmenthistory;
use App\Prevtenancyinfo;
use App\Curentenancyinfo;
use App\Nextofkin;
use App\lease;
use Illuminate\Http\Request;
use Storage;
use Illuminate\Support\Facades\Auth;

class TenantAppController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $tenantapp = TenantApp::where('user_id', 'LIKE', "%$keyword%")
                ->orWhere('full_name', 'LIKE', "%$keyword%")
                ->orWhere('addr', 'LIKE', "%$keyword%")
                ->orWhere('tel', 'LIKE', "%$keyword%")
                ->orWhere('sex', 'LIKE', "%$keyword%")
                ->orWhere('marital_status', 'LIKE', "%$keyword%")
                ->orWhere('name_spouse', 'LIKE', "%$keyword%")
                ->orWhere('no_children', 'LIKE', "%$keyword%")
                ->orWhere('no_dependant', 'LIKE', "%$keyword%")
                ->orWhere('how_longr', 'LIKE', "%$keyword%")
                ->orWhere('no_cars', 'LIKE', "%$keyword%")
                ->orWhere('type_pet', 'LIKE', "%$keyword%")
                ->orWhere('nationatlity', 'LIKE', "%$keyword%")
                ->orWhere('state_origin', 'LIKE', "%$keyword%")
                ->orWhere('img', 'LIKE', "%$keyword%")
                ->orWhere('house_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $tenantapp = TenantApp::latest()->paginate($perPage);
        }

        return view('tenant.tenant-app.index', compact('tenantapp'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('tenant.tenant-app.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
       //  alpha caracters
      // $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

       // generate a pin based on 2 * 7 digits + a random character
       $pin = mt_rand(1000000, 9999999);
       
       // shuffle the result
      // $char=str_shuffle($characters);
       $rentappid= str_shuffle($pin);
       
      

      
        /////////////////validations////////////////////////////
       
        $this->validate($request, [
			'user_id' => 'required',
			'property_id' => 'required',
        ]);

        $data = $request->except('img','app_status','rentapp_id','prent_appid','rent_appid','nrent_appid','crent_appid','emprent_appid','rrent_appid');
        $data['rentapp_id'] = $rentappid;
        $data['app_status'] = 'inprogress';
        $data['rent_appid'] = $rentappid;
        $data['nrent_appid'] = $rentappid;
        $data['prent_appid'] = $rentappid;
        $data['rrent_appid'] = $rentappid;
        $data['crent_appid'] = $rentappid;
        $data['emprent_appid'] = $rentappid;
        if ($request->has('img')) {
            $path=Storage::putfile('public',$request->file('img'));
            $url=Storage::url($path);
            $data['img'] = $url;
        }

        
/////////////////validations of passport////////////////////////////


       // $requestData = $request->all();
       // TenantApp::create($requestData);
       $requesdata=TenantApp::create($data);
       $requestnextOfkin=Nextofkin::create($data);
       $requestRefree=Refrees::create($data);
       $requestemphistory =Employmenthistory::create($data);
       $requestprevtenancyinfo =Prevtenancyinfo::create($data);
       $requestcurrentenancyinfo =Curentenancyinfo::create($data);



  /////////////////object intialization////////////////////////////
  /*   
  $requesdata=new TenantApp;
        $requestnextOfkin=new Nextofkin;
        $requestRefree=new Refrees;
        $requestemphistory =new  Employmenthistory;
        $requestprevtenancyinfo =new  Prevtenancyinfo;
        $requestcurrentenancyinfo =new  Curentenancyinfo;
 //////end of object intialization////////////////////////////
      
 //////////////passing  basic information data to object//////////////////////////////

 $requesdata->user_id=$request->user_id;
      $requesdata->full_name=$request->full_name;
      $requesdata->rentapp_id=$rentappid;
      $requesdata->addr=$request->addr;
      $requesdata->tel=$request->tel;
      $requesdata->sex=$request->sex;
      $requesdata->dob=$request->dob;
      $requesdata->id_type=$request->id_type;
      $requesdata->id_no=$request->id_no;
      $requesdata->marital_status=$request->marital_status;
      $requesdata->name_spouse=$request->name_spouse;
      $requesdata->no_children=$request->no_children;
      $requesdata->no_dependant=$request->no_dependant;
      $requesdata->how_longr=$request->how_longr;
      $requesdata->how_manyr=$request->how_manyr;

      $requesdata->no_cars=$request->no_cars;
      $requesdata->type_pet=$request->type_pet;
      $requesdata->no_pet=$request->no_pet;
      $requesdata->nationatlity=$request->nationatlity;
      $requesdata->state_origin=$request->state_origin;
      $requesdata->img=$url;
      $requesdata->house_id=$request->house_id;

//////////////////////////passing of next of kin data///////////////
      $requestnextOfkin->full_name=$request->nfull_name;
  $requestnextOfkin->rel=$request->nrel;
  $requestnextOfkin->addr=$request->naddr;
  $requestnextOfkin->mail=$request->nmail;
  $requestnextOfkin->rent_appid=$rentappid;

//////////////////////////passing of Refrees data///////////////
  $requestRefree->full_name=$request->rfull_name;
  $requestRefree->rel=$request->rrel;
  $requestRefree->tel=$request->rtel;
  $requestRefree->rent_appid=$rentappid;


  //////////////////////////passing of employment history data///////////////
  $requestemphistory->c_empaddr =$request->c_empaddr;
  $requestemphistory->c_occup =$request->c_occup;
  $requestemphistory->c_lenth =$request->c_lenth;
  $requestemphistory->c_tel =$request->c_tel;
  $requestemphistory->s_bname =$request->s_bname;
  $requestemphistory->s_baddr =$request->s_date;
  $requestemphistory->s_pos =$request->s_pos;
  $requestemphistory->s_means =$request->s_means;
  $requestemphistory->s_bankername =$request->s_bankername;
  $requestemphistory->addr =$request->saddr;
  $requestemphistory->rent_appid =$rentappid;
 

//////////////////////////passing of previous tenancy information data///////////////
  $requestprevtenancyinfo->name_agent =$request->pname_agent;
  $requestprevtenancyinfo->tel =$request->ptel;
  $requestprevtenancyinfo->sec_deposit =$request->sec_deposit;
  $requestprevtenancyinfo->reason =$request->preason;
  $requestprevtenancyinfo->rent_appid =$rentappid;


//////////////////////////passing of current tenancy information data///////////////
  $requestcurrentenancyinfo->time_atcurrent =$request->time_atcurrent;
  $requestcurrentenancyinfo->reason =$request->creason;
  $requestcurrentenancyinfo->name_agent =$request->cname_agent;
  $requestcurrentenancyinfo->notice =$request->notice;
  $requestcurrentenancyinfo->prev_addr =$request->prev_addr;
  $requestcurrentenancyinfo->time_atprevaddr =$request->time_atprevaddr;
  $requestcurrentenancyinfo->rent_appid =$rentappid;
  $requestcurrentenancyinfo->tel =$request->ctel;



//////////////////////////saving of  data///////////////
  $requestemphistory->save();
  $requestprevtenancyinfo->save();
  $requestcurrentenancyinfo ->save();

  $requestRefree->save();
  $requestnextOfkin->save();
  $requesdata->save();

*/


     
     //////////////////////////redirecting users///////////////  
        return redirect('tenant/rentapplications')->with('flash_message', 'TenantApp added!');
    }

    public function rentappedit($id){
        
        
       $rentapp=TenantApp::findorFail($id);

      $nextOfkin=NextofKin::findorFail($id);
      $refree=Refrees::findorFail($id);
      $emphistory =Employmenthistory::findorFail($id);
       $prevtenancyinfo =Prevtenancyinfo::findorFail($id);
       $currentenancyinfo =Curentenancyinfo::findorFail($id);
       return view('tenant.rentappeditform',compact('rentapp','nextOfkin','refree','emphistory','prevtenancyinfo','currentenancyinfo'));
  
  
     
    }

    public function rentapps(){


        $userid=Auth::user()->id;
        $rentalapp=TenantApp::where('user_id',$userid)->get();
        $uid=0;

        $lease=lease::where('users_id',$userid)->where('rent_status','Active')->count();
        
  $bool=false;

 if($rentalapp){
    $bool=true;
  }

    return view('tenant.rentapplications',compact('rentalapp','bool','lease'));
}

    public function rentappupdate(Request $request,$id){



        $this->validate($request, [
			'user_id' => 'required',
		
		]);
      
        
        $Data = $request->except('img');  


/////////////////validations of passport////////////////////////////
if($request->has('img'))  {

    $path=Storage::putFile('public',$request->file('img'));
    
    $url=Storage::url($path);

    $Data['img']=$url;
  }

  


// TenantApp::create($requestData);

/////////////////object intialization////////////////////////////
$tenantapp =TenantApp::findOrFail($id);
$requestnextOfkin=Nextofkin::findOrFail($id);
$requestRefree=Refrees::findOrFail($id);
$requestemphistory =Employmenthistory::findOrFail($id);
$requestprevtenancyinfo =Prevtenancyinfo::findOrFail($id);
$requestcurrentenancyinfo =Curentenancyinfo::findOrFail($id);
//////end of object intialization////////////////////////////

//////////////passing  basic information data to object//////////////////////////////


//////////////////////////saving of  data///////////////
$requestemphistory->update($Data);
$requestprevtenancyinfo->update($Data);
$requestcurrentenancyinfo ->update($Data);

$requestRefree->update($Data);
$requestnextOfkin->update($Data);
$tenantapp->update($Data);








      

        return redirect('tenant/rentapplications')->with('flash_message', 'TenantApp updated!');

    }
  


  

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $tenantapp = TenantApp::findOrFail($id);

        return view('tenant.tenant-app.show', compact('tenantapp'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $tenantapp = TenantApp::findOrFail($id);

        

        return view('tenant.tenant-app.edit', compact('tenantapp'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'user_id' => 'required',
		
		]);
        $requestData = $request->all();
        
        $tenantapp = TenantApp::findOrFail($id);
        $tenantapp->update($requestData);

        return redirect('tenant-app')->with('flash_message', 'TenantApp updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        TenantApp::destroy($id);

        return redirect('tenant-app')->with('flash_message', 'TenantApp deleted!');
    }
}
