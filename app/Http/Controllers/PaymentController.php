<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Paystack;
use App\payment;
use App\subscription;
use App\lease;


class PaymentController extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
    /**
     * Redirect the User to Paystack Payment Page
     * @return Url
     */
    public function redirectToGateway()
    {
        return Paystack::getAuthorizationUrl()->redirectNow();
    }

    /**
     * Obtain Paystack payment information
     * @return void
     */
    public function handleGatewayCallback()
    {
        $paymentDetails = Paystack::getPaymentData();
        $payment=  new payment();
        $sub= new subscription();
        $lease=new lease();

       // dd($paymentDetails);
        // Now you have the payment details,
        // you can store the authorization_code in your db to allow for recurrent subscriptions
        // you can then redirect or do whatever you want


       
        ///////////handl  tenants rent payments////////////////

        if ($paymentDetails['data']['metadata']['type_ofservice']=='RentPayment'){

            if($paymentDetails['data']['status'] != 'success'){
         
                //redirect here 
                return redirect('/tenant/payments/report')->with([ 'status' => 'was not succcessfull' ]);
               
            }


             if($paymentDetails['data']['status'] == 'success'){
           
  
            $payment->transaction_ref=$paymentDetails['data']['reference'];
    
            $payment->amount=$paymentDetails['data']['amount'];
            $payment->user_id=$paymentDetails['data']['metadata']['user_id'];
            $payment->status=$paymentDetails['data']['status'];
            $payment->paid_at=$paymentDetails['data']['paid_at'];
            $payment->invoice_no=$paymentDetails['data']['metadata']['invoice_no'];
            $payment->type_ofservice=$paymentDetails['data']['metadata']['type_ofservice'];
            ////////pakage id handles property id////////
            $payment->package_id =$paymentDetails['data']['metadata']['property_id'];

       
           $lease->start_date="12/12/19";

           $lease->end_date="12/12/2020";
            $lease->users_id=$paymentDetails['data']['metadata']['user_id'];
            $lease->rent_status="Active";
            $lease->property_id=$paymentDetails['data']['metadata']['property_id'];

            $lease->save();
            $payment->save();

          $bool=true;
         return redirect('/tenant/payments/report')->with([ 'status' => 'succcessfull' ]);
           
        }

        }




        ///////////handl landlord subscriptions////////////////
        if ($paymentDetails['data']['metadata']['type_ofservice']=='subscription'){

            if($paymentDetails['data']['status'] != 'success'){
         
                //redirect here 
                return redirect('/landlord/payments/report')->with([ 'status' => 'was not succcessfull' ]);
               
            }
            
        if($paymentDetails['data']['status'] == 'success'){
           
  
            $payment->transaction_ref=$paymentDetails['data']['reference'];
  
            $payment->amount=$paymentDetails['data']['amount'];
           $payment->user_id=$paymentDetails['data']['metadata']['user_id'];
           $payment->status=$paymentDetails['data']['status'];

           $payment->invoice_no=$paymentDetails['data']['metadata']['invoice_no'];
           $payment->type_ofservice=$paymentDetails['data']['metadata']['type_ofservice'];
               ////////pakage id handles service id////////
               $payment->package_id =$paymentDetails['data']['metadata']['service_id'];
           $payment->paid_at=$paymentDetails['data']['paid_at'];


             $sub->service_id=$paymentDetails['data']['metadata']['service_id'];
             $sub->user_id=$paymentDetails['data']['metadata']['user_id'];
          $sub->date_subscribed=$paymentDetails['data']['paid_at'];
          $sub->status="Active";
          $sub->save();
          $payment->save();

          $bool=true;
         return redirect('/landlord/payments/report')->with([ 'status' => 'succcessfull' ]);
           
        }

        }
       



        
  
    }
    
}
