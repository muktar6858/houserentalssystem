<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Storage;
use App\TenantApp;
use App\Nextofkin;
use App\lease;
use App\payment;
use App\booking;
use App\Property;

use Illuminate\Support\Facades\DB;


class tenantController extends Controller
{
    //


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
    
    


    function index(){
        if(Auth::check()){
        $userid=Auth::user()->id;
        $lease=lease::where('users_id',$userid)->first();
        $tenantapp=TenantApp::where('user_id',$userid)->where('app_status','Rejected')->count();
        $tenantcheck=TenantApp::where('user_id',$userid)->where('app_status','inprogress')->count();
        $booking=booking::where('user_id',$userid)->where('status','Visited')->first();
        $checkschedul=booking::where('user_id',$userid)->where('status','sheduled')->first();
        $user=User::where('id',$userid)->first();
        
$bool=false;
$testbooking=false;
$boolschedule=false;
        if($lease){
            $bool=true;
        }
        if($checkschedul){
$boolschedule=true;

        }

        if($booking){
            $testbooking=true;
        }
        return view('tenant.dashboard',compact('lease','bool','tenantapp','booking','testbooking','tenantcheck','user','boolschedule','checkschedul'));
    }
    }


    public function profile($id){

        $user=User::findorFail($id);


    return view('userProfile',compact('user'));

    }

    public function edit($id){

        $user=User::findorFail($id);


    return view('userProfileUpdate',compact('user'));

    }


    public function payments(){

    $userid=Auth::user()->id;
    $payment=payment::where('user_id',$userid)->get();
   $bool=false;
    if($payment){
        $bool=true;
    }
        return view('tenant.payments', compact('payment','bool'));
    }



 //////////////invoices form/////////
    public function invoices($id){

 //$tapp=TenantApp::where('rentappid',$id)->get();

 

      //  alpha caracters
 $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

// generate a pin based on 2 * 7 digits + a random character
 $pin = mt_rand(1000000, 9999999);

// shuffle the result
$char=str_shuffle($characters);
$ints = str_shuffle($pin);

$invoiceID='HRS'.$ints;


$rentapp=DB::table('tenant_apps')->join('properties','properties.id', '=', 'tenant_apps.property_id')
->select('tenant_apps.*','properties.*')
->where('tenant_apps.rentapp_id', '=', $id)
->first();

$percent=(10*$rentapp->price)/100;
$totaldue=$percent+$rentapp->price;


        return view('tenant.tenantinvoice',compact('invoiceID','rentapp','percent','totaldue'));
    }

   


   
    
 //////////////lease form/////////
    public function lease(){

        $userid=Auth::user()->id;
        $lease=lease::where('users_id',$userid)->first();
        $count=lease::where('users_id',$userid)->count();


        return view('tenant.lease',compact('lease','count'));
    }

    
    //////////////rental application form/////////
    public function rentappform($id){

            $property=Property::find($id);
        return view('tenant.rentappform',compact('id'));
    }

    public function rentappdetails($id){

        $rentappdetails=TenantApp::findorFail($id);
        $userid=Auth::user()->id;
        $lease=lease::where('users_id',$userid)->where('rent_status','Active')->count();
        return view('tenant.rentappdetails',compact('rentappdetails','lease'));
    }

    public function paymentreport(){

        return view('tenant.paymentreport');
    }
   

    public function reqterm(Request $request){


        $data=$request->toArray();
    $term=termination::create($data);
    $term->save();
        return redirect('/landlord/lease');
    }
    


}
