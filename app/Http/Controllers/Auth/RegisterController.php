<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Storage;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
  


    public function redirectTo(){
      
            if(Auth::check() && Auth::user()->acc_type=='landlord'){
                return '/landlord';
            }
    
            if(Auth::check() && (Auth::user()->acc_type=='admin' || Auth::user()->acc_type=='manager')){
                return '/admin';
            }
            if(Auth::check() && Auth::user()->acc_type='tenant'){
                return '/tenant';
            }
            
       
            
        }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *'first_name' => ['required', 'string', 'max:20'],
      *      'middle_name' => ['required', 'string', 'max:20'],
       *     'last_name' => ['required', 'string', 'max:20'],
       *     'gender' => ['required', 'string', 'max:10'],
        *    'dob' => ['required', 'string', 'max:20'],
        *    'addr' => ['required', 'string', 'max:255'],
         *   'country' => ['required', 'string', 'max:10'],,
         *   'state' => ['required', 'string', 'max:10'],
          *  'locality' => ['required', 'string', 'max:10'],
          *  'phone' => ['required', 'integer',,'min:11', 'max:15'],
          *  'img' => ['required', 'string', 'max:255'],
          *  'acc_type' => ['required', 'string', 'max:255'],
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:10'],
            
            
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        

             
        return User::create([
            'name' => $data['name'],
            
            'first_name'=> $data['fname'],
            'middle_name'=> $data['mname'],
            'last_name'=> $data['lname'],
            'gender'=> $data['gender'],
            'dob'=> $data['dob'],
            'addr'=> $data['addr'],
            'country'=> $data['country'],
            'state'=> $data['state'],
            'locality'=> $data['local'],
            'phone'=> $data['tel'],
            'acc_type'=> $data['acctype'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
