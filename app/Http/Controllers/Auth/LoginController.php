<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\User;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
  
        
        public function redirectTo()
        {
          
            if(Auth::check() && Auth::user()->acc_type=='landlord'){
                return '/landlord';
            }elseif(Auth::check() && (Auth::user()->acc_type=='admin' || Auth::user()->acc_type=='manager')){
                return '/admin';
            }elseif(Auth::check() && Auth::user()->acc_type=='tenant'){
                return '/tenant';
            }
          
            
        }
    

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    
}
