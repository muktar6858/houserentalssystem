<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Storage;
use App\User;
use App\lease;
use App\property;
use App\payment;
use App\termination;
use App\subscription;
use App\TenantApp;
use App\booking;
use App\contact;
class AdminController extends Controller
{


    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {

      $propertycount =property::all()->count();
      $propertyrejected =property::where('mode','rejected')->count();
      $propertyundereview =property::where('mode','inactive')->count();
      $propertyrejected =property::where('mode','Decline')->count();
      $propertyrented =property::where('status','occupied')->count();
      
      $paymentcount =payment::all()->count();
      $activesub=subscription::where('status','Active')->count();
      $contacts=contact::all()->count();
      $termcount=termination::where('status','resolved')->count();


        return view('admin.dashboard',compact('propertycount','contacts','termcount','propertyrented','propertyrejected','propertyundereview','paymentcount','activesub'));
    }








    public function users()
    {
        $user=User::all();

        return view('admin.users',compact('user'));
    }


    
 public function rentapplications()
 {
     $rentalapp=TenantApp::all();

     return view('admin.applications',compact('rentalapp'));
 }

 public function showrentapplication($id){

    $rentappdetails=TenantApp::find($id);

    return view('admin.showappplication',compact('rentappdetails'));

 }

 public function manageapps(Request $request,$id){


    
    $apps=TenantApp::find($id);

// Make sure you've got the Page model
if($apps) {
$val=$request->get('mode');

 $apps->app_status = $val;
  $apps->save();

  return redirect('admin/rentapplications');
}else{

    return " transaction failed";

}
 }


 public function properties()
 {
     $property=property::all();

     return view('admin.properties',compact('property'));
 }

 public function editproperty($id)
 {

     $property=property::find($id);

     return view('admin.editproperty',compact('property'));

 }


 public function updateproperty(Request $request, $id)
 {
    
    $requestData = $request->all();
    
    $property = Property::findOrFail($id);
    $property->update($requestData);

    return redirect('admin/properties')->with('flash_message', 'Property updated!');

 }



public function manage(Request $request, $id){

    $property = property::find($id);

// Make sure you've got the Page model
if($property) {
$val=$request->get('mode');

 $property->mode = $val;
  $property->save();

  return redirect('admin/properties')->with('flash_message', 'Property updated!');
}else{

    return " transaction failed";

}


}



 public function showproperties($id)
 {
     $property=Property::find($id);

     return view('admin.showproperty',compact('property'));
 }


 public function payments()
 {
     $payment=payment::all();

     return view('admin.payment',compact('payment'));
 }


 public function lease()
 {
     $lease=lease::all();

     return view('admin.lease',compact('lease'));
 }

 public function booking()
 {
     $booking=booking::orderBy('status', 'ASC')->get();

     return view('admin.booking',compact('booking'));
 }


 public function managebooking(Request $request, $id)
 {
    $booking =booking::find($id);

    // Make sure you've got the Page model
    if($booking) {
    $val=$request->get('mode');
    
     $booking->status = $val;
      $booking->save();
    
      return redirect('admin/booking');
    }else{
    
        return " transaction failed";
    
    }
 }


 public function subscription()
 {
     $subscription=subscription::all();

     return view('admin.subscription',compact('subscription'));
 }


 public function showuser($id)
 {
     $user = User::findOrFail($id);

     return view('admin.showusers', compact('user'));
 }

 public function edituser($id){

    $user=User::findorFail($id);


return view('admin.updateuser',compact('user'));

}


public function updateuser(Request $request, $id)
    {
        

        $data = $request->except('password','img');
        if ($request->has('password')) {
            $data['password'] = bcrypt($request->password);
        }
        if ($request->has('img')) {
            $path=Storage::putfile('public',$request->file('img'));
            $url=Storage::url($path);
            $data['img'] = $url;
        }

        $user = User::findOrFail($id);
        $user->update($data);

       
        return redirect('admin/users')->with('flash_message', 'User updated!');
    }


    public function storeuser(Request $request)
    {

        $this->validate(
            $request,
            [
                'name' => 'required',
                'email' => 'required|string|max:255|email|unique:users',
                'password' => 'required',
                
            ]
        );
        

        $data = $request->except('password','img');
        $data['password'] = bcrypt($request->password);
        if ($request->has('img')) {
            $path=Storage::putfile('public',$request->file('img'));
            $url=Storage::url($path);
            $data['img'] = $url;
        }

        $user = User::create($data);

     

        return redirect('admin/users')->with('flash_message', 'User added!');
    }

   

    public function  adduser()
    {
      

        return view('admin.adduser');
    }

    public function  profile($id)
    {
      
        $user=User::find($id);
        return view('admin.profile',compact('user'));
    }

    
    public function  contacts()
    {
      
        $contact=Contact::all();
        return view('admin.contactus',compact('contact'));
    }


    public function  leasetermination()
    {
      
        $terminate=termination::all();
        return view('admin.termination',compact('terminate'));
    }
}
