<?php

namespace App\Http\Controllers\Admin;



use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Property;
use Storage;

class ManagePropertyController extends Controller
{
    //

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $property = Property::where('title', 'LIKE', "%$keyword%")
                ->orWhere('addr', 'LIKE', "%$keyword%")
                ->orWhere('type', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $property = Property::latest()->paginate($perPage);
        }

        return view('admin.manageproperty.index', compact('property'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.manageproperty.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->except('img','img1','img2','img3');
        if ($request->has('img')) {
            $path=Storage::putfile('public',$request->file('img'));
            $url=Storage::url($path);
            $requestData['img'] = $url;
        }
        if ($request->has('img1')) {
            $path=Storage::putfile('public',$request->file('img1'));
            $url=Storage::url($path);
            $requestData['img1'] = $url;
        }
        if ($request->has('img2')) {
            $path=Storage::putfile('public',$request->file('img2'));
            $url=Storage::url($path);
            $requestData['img2'] = $url;
        }
        if ($request->has('img3')) {
            $path=Storage::putfile('public',$request->file('img3'));
            $url=Storage::url($path);
            $requestData['img3'] = $url;
        }
        
        Property::create($requestData);

        return redirect('admin/manageproperty')->with('flash_message', 'property added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $property = Property::findOrFail($id);

        return view('admin.manageproperty.show', compact('property'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $property = Property::findOrFail($id);

        return view('admin.manageproperty.edit', compact('property'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->except('img','img1','img2','img3');
        if ($request->has('img')) {
            $path=Storage::putfile('public',$request->file('img'));
            $url=Storage::url($path);
            $requestData['img'] = $url;
        }
        if ($request->has('img1')) {
            $path=Storage::putfile('public',$request->file('img1'));
            $url=Storage::url($path);
            $requestData['img1'] = $url;
        }
        if ($request->has('img2')) {
            $path=Storage::putfile('public',$request->file('img2'));
            $url=Storage::url($path);
            $requestData['img2'] = $url;
        }
        if ($request->has('img3')) {
            $path=Storage::putfile('public',$request->file('img3'));
            $url=Storage::url($path);
            $requestData['img3'] = $url;
        }
        $property = Property::findOrFail($id);
        $property->update($requestData);

        return redirect('admin/manageproperty')->with('flash_message', 'property updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Property::destroy($id);

        return redirect('admin/manageproperty')->with('flash_message', 'property deleted!');
    }
}
