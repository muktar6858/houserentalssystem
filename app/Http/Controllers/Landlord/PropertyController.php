<?php

namespace App\Http\Controllers\Landlord;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Storage;
use App\Property;
use App\User;
use App\subscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */

//////////////////////////all users property display////////////////////
public function showproperty(){

$property=Property::all();


    return view('allProperty',compact('property'));
}

public function showSelected($id)
{
    $property = Property::findOrFail($id);

    return view('showProperty', compact('property'));
   
}

//////////////////////////end all users property display////////////////////





///////////////////specific for lanlord////////////////////////////////

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $property = Property::where('ID', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->orWhere('type', 'LIKE', "%$keyword%")
                ->orWhere('no_bedroom', 'LIKE', "%$keyword%")
                ->orWhere('no_bathroom', 'LIKE', "%$keyword%")
                ->orWhere('no_parkingsp', 'LIKE', "%$keyword%")
                ->orWhere('price', 'LIKE', "%$keyword%")
                ->orWhere('desc', 'LIKE', "%$keyword%")
                ->orWhere('img', 'LIKE', "%$keyword%")
                ->orWhere('img1', 'LIKE', "%$keyword%")
                ->orWhere('img2', 'LIKE', "%$keyword%")
                ->orWhere('img3', 'LIKE', "%$keyword%")
                ->orWhere('video_url', 'LIKE', "%$keyword%")
                ->orWhere('is_furnished', 'LIKE', "%$keyword%")
                ->orWhere('title', 'LIKE', "%$keyword%")
                ->orWhere('addr', 'LIKE', "%$keyword%")
                ->orWhere('area', 'LIKE', "%$keyword%")
                ->orWhere('is_barchelorsa', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $property = Property::latest()->paginate($perPage);
        }

        return view('landlord.property.index', compact('property'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()

    {
        $userid=Auth::user()->id;
        $Sub = subscription::find($userid);
        $bool=false;
        if($Sub !=null){
            $bool=true;
        }

        else 
        {
            $bool=false;
        }

        return view('landlord.property.create', compact('Sub','bool'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'user_id' => 'required'
        ]);
        
        $requestData= new Property;
        $url=""; $url1="";  $url2="";  $url3="";
      if($request->file('img') !=null)  {

        $path=Storage::putFile('public',$request->file('img'));
        
        $url=Storage::url($path);
      }
        

      if($request->file('img1') !=null){

        $path1=Storage::putFile('public',$request->file('img1'));
        
        $url1=Storage::url($path1);
      }
       

        if($request->file('img2') !=null){

            $path2=Storage::putFile('public',$request->file('img2'));
        
            $url2=Storage::url($path2);
        }
        
        if($request->file('img3') !=null){

            $path3=Storage::putFile('public',$request->file('img3'));
        
            $url3=Storage::url($path3);
        }
        

        $requestData->img=$url;
        $requestData->user_id= $request->user_id;
        $requestData ->status= $request->status;
        $requestData->type= $request->type;
        $requestData->no_bedroom= $request->no_bedroom;
        $requestData->no_bathroom= $request->no_bathroom;
        $requestData->no_parkingsp= $request->no_parkingsp;
        $requestData->price= $request->price;
        $requestData->desc= $request->desc;
        $requestData->mode= $request->mode;
        $requestData->state= $request->state;
        $requestData->city= $request->city;
        $requestData->img1= $url1;
        $requestData->img2= $url2;
        $requestData->img3= $url3;
        $requestData->video_url= $request->video_url;
        $requestData->is_furnished= $request->is_furnished;
        $requestData->title= $request->title;
        $requestData->addr= $request->addr;
        $requestData->area= $request->area;
        $requestData->is_barchelorsa= $request->is_barchelorsa;

        $requestData->save();

       // $requestData = $request->all();
        
       // Property::create($requestData);

        return redirect('/landlord/allproperties')->with('flash_message', 'Property added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $property = Property::findOrFail($id);

        return view('landlord.property.show', compact('property'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $property = Property::findOrFail($id);

        return view('landlord.property.edit', compact('property'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'ID' => 'required'
		]);
        $requestData = $request->all();
        
        $property = Property::findOrFail($id);
        $property->update($requestData);

        return redirect('/landlord/allproperties')->with('flash_message', 'Property updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Property::destroy($id);

        return redirect('/landlord/allproperties')->with('flash_message', 'Property deleted!');
    }
}
