<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Property;
use App\User;
use App\booking;
use App\contact;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
    
    
    public function index(){
        
        
        $property=Property::where('mode','Approved')->take(7)->get();

        return view('index',compact('property'));
    }
    
   
    public function  faq()
    {
        return view('faq');
    }
    public function propertieshome()
    {
        return view('home');
    }

    public function allpropertieshome(Request $request)
    {
        $keyword = $request->get('title');
        $keyword2=$request->get('bachelor');
        $keyword3=$request->get('city');
        $perPage = 15;

        if (!empty($keyword)) {
            $property= property::where('mode','Approved')->where('title', 'LIKE', "%$keyword%")->get();
              
        }elseif(!empty($keyword2)) {

            $property= property::where('mode','Approved')->where('title', 'LIKE', "%$keyword2%")->orWhere('is_barchelorsa', 'LIKE', "%$keyword2%")->get();
        }elseif(!empty($keyword3)) {

            $property= property::where('mode','Approved')->where('city', 'LIKE', "%$keyword3%")->orWhere('is_barchelorsa', 'LIKE', "%$keyword2%")->get();
        }

         else{
            $property=property::where('mode','Approved')->get();
        }

 
        
        return view('allproperty',compact('property'));
    }

    public function showproperty($id)
    {
        $property=property::where('id',$id)->first();
        $property2=property::where('mode','Approved')->take(3);
        $property_id=$id;

        return view('showProperty',compact('property','property_id','property2'));
    }
    
     
    
    public function submitschedule(Request $request){

        $data=$request->toArray();
        $booking=booking::create($data);

        $booking->save();

        return redirect('/tenant');

    }
    
    
    public function test(){

        return view('test');
    }


    

    public function contactspage(){

        return view('contact');
    }

    public function contactus(Request $request){


        $data=$request->toArray();
        $contact=contact::create($data);
        $contact->save();


        return redirect('/contactspage')->with('msg',"Message Send Succsee");
    }
   
}
