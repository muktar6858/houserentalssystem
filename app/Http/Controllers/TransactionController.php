<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TransactionController extends Controller
{
    //

    public function show($id)
    {
        $transaction = $this->transactionRepository->findWithoutFail($id);
        if (empty($transaction)) {
            Flash::error('Transaction not found');
            return redirect(route('transactions.index'));
        }
        return view('transactions.show')->with('transaction', $transaction);
    }
}
