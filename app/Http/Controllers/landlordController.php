<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\service;
use App\Property;
use App\Payment;
use Illuminate\Support\Facades\DB;
use App\subscription;
use App\termination;
use Illuminate\Support\Facades\Auth;


class landlordController extends Controller
{
    //

    

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
    public function index(){
        if(Auth::check()){
$userid=Auth::user()->id;
    $countp=Property::where('user_id',$userid)->count();
    $countpinactive=Property::where('user_id',$userid)->where('mode','inactive')->count(); 
    $countprented=Property::where('user_id',$userid)->where('status','rented')->count(); 
    $countprejected=Property::where('user_id',$userid)->where('status','rejected')->count(); 
    $paymentcount=Payment::where('user_id',$userid)->count();
    $paymentcountfailed=Payment::where('status','failed')->count();
    $paymentcountlatest=Payment::where('user_id',$userid)->latest()->count();
    $subscount=subscription::where('user_id',$userid)->where('status','Active')->count();

        return view('landlord.dashboard',compact('countp','paymentcountfailed','paymentcount','countprejected','subscount','countpinactive','countprented','paymentcountlatest'));
    }else{

        return redirect('/login');
    }
    }
    public function profile($id){

        $user=User::findorFail($id);


    return view('userProfile',compact('user'));

    }

    public function edit($id){

        $user=User::findorFail($id);


    return view('userProfileUpdate',compact('user'));

    }


 

public function allproperty(){
   $userid=Auth::user()->id;
   $property =Property::where('user_id',$userid)->get();

   $bool=false;

   if($property){
       $bool=true;
   }

    return view('landlord.myproperty',compact('property','bool'));
}

public function propertydetails($id){

    $property=Property::find($id);

    return view('landlord.propertydetails',compact('property'));
}

public function lease(){


    return view('landlord.lease');
}

public function myservice(){
   $userid=Auth::user()->id;
   $sub=DB::table('subscriptions')->join('users','users.id', '=', 'subscriptions.user_id')
   ->join('services','services.id','=','subscriptions.service_id')->select('subscriptions.*','services.*')
   ->where('user_id', '=', $userid)->where('status','=','Active')
   ->get();

  
    
    $bool=false;
if($sub){
    $bool=true;
}
    return view('landlord.myservices',compact('sub','bool'));
}

public function services(){

    $service=service::all();

    return view('landlord.services',compact('service'));
}

public function invoice($id){

$service=service::findorFail($id);


      //  alpha caracters
$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

// generate a pin based on 2 * 7 digits + a random character
$pin = mt_rand(1000000, 9999999);

// shuffle the result
$char=str_shuffle($characters);
$ints = str_shuffle($pin);

$invoiceID='HRS'.$ints;
$amt=$service->service_price.'00';

     return view('landlord.landlordinvoice',compact('service','invoiceID','amt'));
}

public function invoices(){

    return view('landlord.invoices');
}
public function payments(){

    $userid=Auth::user()->id;
    $payment=payment::where('user_id',$userid)->latest()->get();
    $bool=false;
if($payment){
    $bool=true;
}

    return view('landlord.payments',compact('payment','bool'));
}

public function register(){

    return view('landlord.payments');
}
public function paymentreport(){

    return view('landlord.paymentreport');
}

public function reqterm(Request $request){


    $data=$request->toArray();
$term=termination::create($data);
$term->save();
    return redirect('/landlord/allproperties');
}





}
