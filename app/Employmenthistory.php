<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employmenthistory extends Model
{
    //

        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'employment_info';
    
        /**
        * The database primary key value.
        *
        * @var string
        */
        protected $primaryKey = 'emprent_appid';
    
        /**
         * Attributes that should be mass-assignable.
         *
         * @var array
         */
        protected $fillable = ['empaddr', 'empoccup','emplenth','emptel','empbname','empbaddr','empdate','emppos','empnamepartner','empmeans','empbankername','empaddr','emprent_appid'];
    
        
    
        /**
         * Change activity log event description
         *
         * @param string $eventName
         *
         * @return string
         */
        public function getDescriptionForEvent($eventName)
        {
            return __CLASS__ . " model has been {$eventName}";
        }

        public function tenantApp()
        {
            return $this->belongsTo(TenantApp::class);
        }
}
