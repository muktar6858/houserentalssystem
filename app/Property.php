<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Property extends Model
{
    use LogsActivity;
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'properties';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'status', 'type', 'no_bedroom', 'no_bathroom', 'no_parkingsp', 'price', 'desc','mode','state','city', 'img', 'img1', 'img2', 'img3', 'video_url', 'is_furnished', 'title', 'addr', 'area', 'is_barchelorsa'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    

    /**
     * Change activity log event description
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getDescriptionForEvent($eventName)
    {
        return __CLASS__ . " model has been {$eventName}";
    }
}
