<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curentenancyinfo extends Model
{
    //

        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'current_tenancy';
    
        /**
        * The database primary key value.
        *
        * @var string
        */
        protected $primaryKey = 'crent_appid';
    
        /**
         * Attributes that should be mass-assignable.
         *
         * @var array
         */
        protected $fillable = ['ctime_atcurrent', 'creason',  'cname_agent','cnotice','cprev_addr','ctime_atprevaddr', 'crent_appid','ctel'];
    
        
    
        /**
         * Change activity log event description
         *
         * @param string $eventName
         *
         * @return string
         */
        public function getDescriptionForEvent($eventName)
        {
            return __CLASS__ . " model has been {$eventName}";
        }

        public function tenantApp()
        {
            return $this->belongsTo(TenantApp::class);
        }
}
