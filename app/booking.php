<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class booking extends Model
{
    //

    protected $fillable = ['user_id','property_id','visit_date','visit_time','message','status'];

}
