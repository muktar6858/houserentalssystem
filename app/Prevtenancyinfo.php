<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prevtenancyinfo extends Model
{
    //

    
        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'prev_tenant_info';
    
        /**
        * The database primary key value.
        *
        * @var string
        */
        protected $primaryKey = 'prent_appid';
    
        /**
         * Attributes that should be mass-assignable.
         *
         * @var array
         */
        protected $fillable = ['pname_agent', 'ptel',  'psec_deposit','preason', 'prent_appid'];
    
        
    
        /**
         * Change activity log event description
         *
         * @param string $eventName
         *
         * @return string
         */
        public function getDescriptionForEvent($eventName)
        {
            return __CLASS__ . " model has been {$eventName}";
        }

        public function tenantApp()
        {
            return $this->belongsTo(TenantApp::class);
        }
}
