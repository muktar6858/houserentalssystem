<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class TenantApp extends Model
{
    use LogsActivity;
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tenant_apps';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'rentapp_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'rentapp_id','full_name', 'addr', 'tel','app_status', 'sex','dob','id_type','id_no', 'marital_status', 'name_spouse', 'no_children', 'no_dependant', 'how_longr','how_manyr', 'no_cars', 'type_pet','no_pet', 'nationatlity', 'state_origin', 'img', 'property_id'];

  

    /**
     * Change activity log event description
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getDescriptionForEvent($eventName)
    {
        return __CLASS__ . " model has been {$eventName}";
    }



    public function refrees(){
    
        return $this->hasOne(Refrees::class);
    }

    public function prevtenancyinfo(){
    
        return $this->hasOne(Prevtenancyinfo::class);
    }
    public function nextofkin(){
    
        return $this->hasOne(Nextofkin::class);
    }
    public function employmenthistory(){
    
        return $this->hasOne(Employmenthistory::class);
    }
    public function curentenancyinfo(){
    
        return $this->hasOne(Curentenancyinfo::class);
    }
}
