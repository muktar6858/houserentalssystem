<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Refrees extends Model
{
    //

    
        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'tenant_referee';
    
        /**
        * The database primary key value.
        *
        * @var string
        */
        protected $primaryKey = 'rrent_appid';
    
        /**
         * Attributes that should be mass-assignable.
         *
         * @var array
         */
        protected $fillable = ['rfull_name', 'rrel',  'rtel', 'rrent_appid'];
    
        
    
        /**
         * Change activity log event description
         *
         * @param string $eventName
         *
         * @return string
         */
        public function getDescriptionForEvent($eventName)
        {
            return __CLASS__ . " model has been {$eventName}";
        }

        public function tenantApp()
    {
        return $this->belongsTo(TenantApp::class);
    }
}
