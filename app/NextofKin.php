<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class NextofKin extends Model
{
    //
 
        use LogsActivity;
        
    
        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'next_of_kin';
    
        /**
        * The database primary key value.
        *
        * @var string
        */
        protected $primaryKey = 'nrent_appid';
    
        /**
         * Attributes that should be mass-assignable.
         *
         * @var array
         */
        protected $fillable = ['nfull_name', 'nrel', 'naddr', 'ntel', 'nmail', 'nrent_appid'];
    
        
    
        /**
         * Change activity log event description
         *
         * @param string $eventName
         *
         * @return string
         */
        public function getDescriptionForEvent($eventName)
        {
            return __CLASS__ . " model has been {$eventName}";
        }

        public function tenantApp()
        {
            return $this->belongsTo(TenantApp::class);
        }
    }
    





