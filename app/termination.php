<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class termination extends Model
{
    //

    protected $fillable = ['lease_id','user_id','reasons','status'];

}
