<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
class LoginTest extends TestCase
{

  use RefreshDatabase;
    /** @test */
    public function only_login_tenant_can_view_tenant_dashbord()
    {
      $response=$this->get('/tenant')->assertRedirect('/login');
    }
 /** @test */
    public function only_login_landlord_can_view_landlord_dashbord()
    {
      $response=$this->get('/landlord')->assertRedirect('/login');
    }
 /** @test */

    public function only_login_staff_can_view_staff_dashbord()
    {
      $response=$this->get('/admin')->assertRedirect('/login');
    }


    /** @test */

    public function authenticated_users_can_access_authenticated_pages()
    {
      $this->actingAs(factory(User::class)->create());
      $response=$this->get('/tenant')->asserOk();

    }

    public function property_can_be_added_via_form()
    {
      $this->actingAs(factory(User::class)->create());
      $response=$this->get('/tenant')->asserOk();

    }
}
